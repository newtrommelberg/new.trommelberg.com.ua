<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Special';
$_['heading_title_raw'] = 'NeoSeo Special';
$_['new_title'] = 'NeoSeo Special';

//Tab
$_['tab_general'] = 'General';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';
$_['tab_support'] = 'Support';

//Button
$_['button_clear_log'] = 'Clear log';
$_['button_recheck'] = 'Recheck';
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';

// Text
$_['text_module'] = 'Modules';
$_['text_success'] = 'Success: You have modified Special module!';
$_['text_edit'] = 'Edit Special Module';
$_['text_clear_log'] = 'Clear log';
$_['text_success_clear'] = 'Log file clean success!';
$_['text_default_title'] = 'Special';
$_['text_template'] = 'Template';

// Entry
$_['entry_name'] = 'Module Name';
$_['entry_title'] = 'Title';
$_['entry_limit'] = 'Limit';
$_['entry_description_limit'] = 'Description symbols limit';
$_['entry_width'] = 'Ancho de la imagen';
$_['entry_height'] = 'Altura de la imagen';
$_['entry_status'] = 'Status';
$_['entry_debug'] = 'Debug';
$_['entry_template'] = 'Template';

// Templates
$_['template_default'] = 'Default';
$_['template_slider'] = 'Slider';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Special module!';
$_['error_name'] = 'Module Name must be between 3 and 64 characters!';
$_['error_width'] = 'Width required!';
$_['error_height'] = 'Height required!';





$_['text_module_version'] = '';
