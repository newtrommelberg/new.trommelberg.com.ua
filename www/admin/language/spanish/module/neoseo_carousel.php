<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Carousel';
$_['heading_title_raw'] = 'NeoSeo Carousel';
$_['new_title'] = 'Carousel';

//Tab
$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';
$_['tab_support'] = 'Support';

//Button
$_['button_clear_log'] = 'Clear Log';
$_['button_recheck'] = 'Check again';
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';

// Entry
$_['entry_status'] = 'Status';
$_['entry_debug'] = 'Debugging';
$_['entry_name'] = 'Name';
$_['entry_icon'] = 'Icon';
$_['entry_icon_desc'] = 'Icon class name';
$_['entry_title'] = 'Header';
$_['entry_banner'] = 'Banner';
$_['entry_width'] = 'Ancho de la imagen';
$_['entry_height'] = 'Altura de la imagen';


// Text
$_['text_module'] = 'Modules';
$_['text_clear_log'] = 'Clear Log';
$_['text_module_version'] = '';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_success'] = 'Module settings updated!';
$_['text_default_title'] = 'Carousel';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_name'] = 'The module name must be between 3 and 64 characters!';
$_['error_width'] = 'You must specify a width!';
$_['error_height'] = 'You must specify a height!';





$_['text_module_version'] = '';
