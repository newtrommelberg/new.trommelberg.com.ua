<?php

// Heading
$_['heading_title'] = 'Адреса';

//Buttons
$_['button_refresh'] = 'Обновить базу адресов';
$_['button_delete_all'] = 'Удалить все записи';

// Text
$_['text_success'] = 'Список адресов обновлен!';
$_['text_address'] = 'Адреса';
$_['text_city'] = 'Города';
$_['text_list'] = 'Список Адресов';
$_['text_add'] = 'Добавление Адреса';
$_['text_edit'] = 'Редактирование Адреса';
$_['text_default'] = 'По умолчанию';
$_['text_confirm_all'] = 'Вы уверены что хотите удалить все записи?';

// Column
$_['column_name'] = 'Адрес';
$_['column_zone'] = 'Регион / Область';
$_['column_city'] = 'Город';
$_['column_shipping_method'] = 'Доставка';
$_['column_action'] = 'Действие';
$_['column_language_id'] = 'Локализация';



// Entry
$_['entry_language_id'] = 'Локализация';
$_['entry_name'] = 'Адрес';
$_['entry_zone'] = 'Регион / Область';
$_['entry_city'] = 'Город';
$_['entry_shipping_method'] = 'Доставка';


// Error
$_['error_name'] = 'Название адреса должно быть от 3 до 128 символов!';
$_['error_warning'] = 'Внимание: Внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] = 'Внимание: У вас нет прав для изменения адресов!';
$_['error_action'] = 'Внимание: Действие не может быть завершено!';
$_['error_filetype'] = 'Недопустимый тип файла!';
