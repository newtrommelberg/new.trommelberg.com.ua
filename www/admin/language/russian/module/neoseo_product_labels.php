<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Метки товаров</span>';
$_['heading_title_raw'] = 'NeoSeo Метки товаров';

//Tab
$_['tab_general'] = 'Общие';
$_['tab_label'] = 'Метки';
$_['tab_special_label'] = 'Метка для скидки';
$_['tab_support'] = 'Поддержка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';

//Button
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить логи';

// Text
$_['text_module_version'] = '';
$_['text_edit'] = 'Параметры';
$_['text_success'] = 'Настройки модуля успешно обновлены!';
$_['text_success_clear'] = 'Логи успешно удалены';
$_['text_module'] = 'Модули';
$_['text_success_delete'] = 'Метка успешно удалена!';
$_['text_new_label'] = 'Новая метка';
$_['text_del_label'] = 'Удалить метку';
$_['text_special'] = 'Скидка';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_store'] = 'Витрина:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_name'] = 'Название метки';
$_['entry_class'] = 'Класс метки';
$_['entry_style'] = 'Стиль метки';
$_['entry_color'] = 'Цвет';
$_['entry_type'] = 'Тип списка';
$_['entry_priority'] = 'Порядок';
$_['entry_products'] = 'Товары';
$_['entry_label_type'] = 'Тип метки';
$_['entry_position'] = 'Положение метки';
$_['entry_special_status'] = 'Статус:';
$_['entry_special_class'] = 'Класс метки';
$_['entry_special_style'] = 'Стиль метки';
$_['entry_special_color'] = 'Цвет';
$_['entry_special_priority'] = 'Порядок';
$_['entry_special_label_type'] = 'Тип метки';
$_['entry_special_position'] = 'Положение метки';
$_['entry_duration_days'] = 'Срок действия (Дней):';
$_['entry_view_counts'] = 'Количество просмотров для активации:';
$_['entry_sold'] = 'Количество покупок:';
$_['entry_limit'] = 'Лимит';
$_['entry_special_title'] = 'Заголовок скидки';

//Params
$_['params_type_new'] = 'Новинки';
$_['params_type_popular'] = 'Популярные';
$_['params_type_hit'] = 'Бестселлер';
$_['params_type_hands'] = 'Указать вручную';
$_['params_type_stock'] = 'Нет на складе';
$_['params_type_instock'] = 'В наличии';
$_['params_position_top_left'] = 'Вверху слева';
$_['params_position_top_right'] = 'Вверху справа';
$_['params_position_bottom_left'] = 'Снизу слева';
$_['params_position_bottom_right'] = 'Снизу справа';
$_['params_type_label_corner']      = 'Уголок';
$_['params_type_label_flag']        = 'Флажок';
$_['params_type_label_stripes']     = 'Ленточка';
$_['params_type_label_sticker']     = 'Наклейка';
$_['params_type_label_chip']        = 'Фишка';
$_['params_type_label_tally']       = 'Этикетка';

// Error
$_['error_permission'] = 'У Вас недостаточно прав для изменения "NeoSeo Резервные копии"!';




