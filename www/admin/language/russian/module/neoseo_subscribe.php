<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Подписка</p>';
$_['heading_title_raw'] = 'NeoSeo Подписка';

$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_fields'] = 'Поля';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';


// Text
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_title'] = 'Подпишитесь на нашу рассылку и получайте скидки!';
$_['text_module_version'] = '';

$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_debug_desc'] = 'В логи модуля будет писаться различная информация для разработчика модуля';
$_['entry_title'] = 'Заголовок';
$_['entry_message'] = 'Сообщение о новой подписке';
$_['entry_message_error'] = 'Сообщение об ошибке при подписке';
$_['entry_notify_list'] = 'Получатели сообщения о подписке, через запятую';
$_['entry_notify_subject'] = 'Тема письма о новой подписке';
$_['entry_notify_message'] = 'Сообщение письма о новой подписке';
$_['entry_show_name'] = 'Использовать поле имя в форме подписки';

$_['param_message'] = "Спасибо что подписались на наши новости.";
$_['param_message_error'] = "Вы уже подписаны на наши новости.";
$_['param_notify_subject'] = "Новая подписка";
$_['param_notify_message'] = "Посетитель с email-ом {email} подписался на наши новости";

// Error
$_['error_permission'] = '';





