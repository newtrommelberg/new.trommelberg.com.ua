<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">SEO-Фильтр от веб-студии NeoSeo</span>';
$_['heading_title_raw'] = 'SEO-Фильтр от веб-студии NeoSeo';

//Tab
$_['tab_general'] = 'Общие';
$_['tab_support'] = 'Поддержка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';

//Button
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';

// Text
$_['text_module_version'] = '';
$_['text_edit'] = 'Параметры';
$_['text_success'] = 'Настройки модуля успешно обновлены!';
$_['text_success_clear'] = 'Логи успешно удалены';
$_['text_success_cache_clear'] = 'Кеш успешно очищен';
$_['text_default'] = 'По умолчанию';
$_['text_module'] = 'Модули';
$_['text_default_title'] = 'Фильтр';
$_['text_default_manufacturer_title'] = 'Производитель';
$_['text_template'] = 'Шаблон';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_name'] = 'Название';
$_['entry_manufacturer'] = 'Фильтр по производителю';
$_['entry_manufacturer_title'] = 'Производитель заголовок';
$_['entry_title'] = 'Заголовок';
$_['entry_template'] = 'Шаблон';
$_['entry_use_price'] = 'Фильтр по цене';


// Error
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';
$_['text_module_version'] = '';
