<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Карусель</span>';
$_['heading_title_raw'] = 'NeoSeo Карусель';
$_['new_title'] = 'Карусель';

//Tab
$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';

//Button
$_['button_clear_log'] = 'Очистить лог';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';

// Entry
$_['entry_status'] = 'Статус';
$_['entry_debug'] = 'Отладка';
$_['entry_name'] = 'Название';
$_['entry_icon'] = 'Иконка';
$_['entry_icon_desc'] = 'Имя класса иконки';
$_['entry_title'] = 'Заголовок';
$_['entry_banner'] = 'Баннер';
$_['entry_width'] = 'Ширина изображения';
$_['entry_height'] = 'Высота изображения';


// Text
$_['text_module'] = 'Модули';
$_['text_clear_log'] = 'Очистить лог';
$_['text_module_version'] = '';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_default_title'] = 'Карусель';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_name'] = 'Имя модуля должно быть от 3 и до 64 знаков!';
$_['error_width'] = 'Необходимо указать ширину!';
$_['error_height'] = 'Необходимо указать высоту!';





$_['text_module_version'] = '';
