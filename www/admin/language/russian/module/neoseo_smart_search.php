<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Аякс Поиск</span>';
$_['heading_title_raw'] = 'NeoSeo Аякс Поиск';

// Tab
$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';

// Text
$_['text_clear_log'] = 'Очистить лог';
$_['text_description'] = '<p>Описание</p>';
$_['text_success_options'] = 'Настройки модуля обновлены!';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_module'] = 'Модули';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module_version'] = '';

// Button
$_['button_clear_log'] = 'Очистить лог';
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';


// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладка:';
$_['entry_selector'] = 'Селектор поля поиска:';
$_['entry_product_limit'] = 'Количество товаров на страницу:';
$_['entry_link_target'] = 'Открывать ссылку с товаром:';
$_['entry_target_self'] = 'в текущем окне браузера';
$_['entry_target_blank'] = 'в новом окне браузера';
$_['entry_search_label'] = 'Текст для количества найденого:';
$_['entry_name_status'] = 'Поиск по названию:';
$_['entry_model_status'] = 'Поиск по модели:';
$_['entry_sku_status'] = 'Поиск по артикулу:';
$_['entry_image_status'] = 'Показывать картинки:';
$_['entry_price_status'] = 'Показывать цену:';
$_['entry_rating_status'] = 'Показывать рейтинг товара:';
$_['entry_description_limit'] = 'Количество символов в описании:';
$_['entry_image_width'] = 'Ширина изображения товара:';
$_['entry_image_height'] = 'Высота изображения товара:';
$_['entry_show_categories'] = 'Показывать категории:';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';




