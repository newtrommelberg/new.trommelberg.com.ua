<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Источник Заказа</span>';
$_['heading_title_raw'] = 'NeoSeo Источник Заказа';

//Tab
$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

//Button
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить логи';

// Text
$_['text_module_version'] = '';
$_['text_module'] = 'Модули';
$_['text_edit'] = 'Параметры';
$_['text_success'] = 'Настройки модуля успешно обновлены!';
$_['text_success_clear'] = 'Логи успешно удалены';
$_['text_default'] = 'По умолчанию';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_list_short_url_status'] = 'Отображать только домен в списке заказов:';
$_['entry_detail_short_url_status'] = 'Отображать только домен в карточке заказа:';
$_['entry_first_referrer_days'] = 'Количество дней для хранения первого захода:';
$_['entry_last_referrer_hours'] = 'Количество часов для хранения последнего захода:';
$_['entry_bad_user_agents'] = 'Блокировать запись на юзер-агенты:';
$_['entry_bad_user_agents_desc'] = 'Поисковикам выдавать куки бесполезно, а вот базу это напряжет, поэтому добавляем юзер агенты поисковиков для блокировки выдачи куков:';

// Error
$_['error_permission']		= 'У Вас нет прав для управления этим модулем!';
$_['error_post_size']       = "Увеличьте значения параметров post_max_size и upload_max_filesize, чтобы они превышали размер архива";
$_['error_empty_archive']   = "Архив пустой или имеет неправильную структуру. Правильная структура предполагает наличие файлов сразу в корне архива";




