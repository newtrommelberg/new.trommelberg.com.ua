<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Рекомендуемые товары</span>';
$_['heading_title_raw'] = 'NeoSeo Рекомендуемые товары';
$_['new_title'] = 'Рекомендуемые товары';

//Tab
$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';

//Button
$_['button_clear_log'] = 'Очистить лог';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';

// Text
$_['text_module'] = 'Модули';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_edit'] = 'Редактирование модуля';
$_['text_clear_log'] = 'Очистить лог';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_default_title'] = 'Хиты продаж';
$_['text_template'] = 'Шаблон';

// Entry
$_['entry_name'] = 'Название';
$_['entry_title'] = 'Заголовок';
$_['entry_limit'] = 'Лимит';
$_['entry_description_limit'] = 'Количество символов описания';
$_['entry_width'] = 'Ширина изображения';
$_['entry_height'] = 'Высота изображения';
$_['entry_status'] = 'Статус';
$_['entry_debug'] = 'Отладка';
$_['entry_template'] = 'Шаблон';
$_['entry_product'] = 'Товары';
$_['entry_use_banner'] = "Использовать Баннер";
$_['entry_banner'] = "Изображение баннера";
$_['entry_banner_link'] = "Ссылка для баннера";
$_['entry_block_description'] = "Описание блока";
$_['entry_banner_width'] = "Ширина баннера";
$_['entry_banner_height'] = "Высота баннера";

// Templates
$_['template_default'] = 'По умолчанию';
$_['template_slider'] = 'Слайдер';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_name'] = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_width'] = 'Необходимо указать ширину!';
$_['error_height'] = 'Необходимо указать высоту!';





$_['text_module_version'] = '';
