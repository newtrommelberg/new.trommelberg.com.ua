<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Поиск';
$_['heading_title_raw'] = 'NeoSeo Блог Поиск';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Модуль Блог Поиск удачно изменен!';
$_['text_edit']        = 'Редактирование модуля Блог Поиск';
$_['text_module_version'] = '';

// Entry
$_['entry_name']       = 'Название модуля';
$_['entry_title']      = 'Заголовок модуля';
$_['entry_blog_category'] = 'Корневой каталог';
$_['entry_template']   = 'Шаблон';
$_['entry_root_category'] = 'Ограничить категорией:';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У вас нет прав изменять модуль Блог Поиск!';