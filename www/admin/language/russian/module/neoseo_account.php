<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Личный кабинет покупателя</span>';
$_['heading_title_raw'] = 'NeoSeo Личный кабинет покупателя';

$_['tab_general'] = 'Параметры';
$_['tab_profiler'] = 'Профайлер';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_title_default'] = 'Авторизоваться через соцсети:';
$_['text_module_version'] = '';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_facebook'] = 'Facebook';
$_['text_vkontakte'] = 'vk.com';
$_['text_odnoklassniki'] = 'Одноклассники';
$_['text_googleplus'] = 'Google+';
$_['text_twitter'] = 'Twitter.com';
$_['text_instagram'] = 'Instagram';
$_['text_yandex'] = 'Yandex.ru';
$_['text_mailru'] = 'Mail.ru';
$_['text_google'] = 'Google';
$_['text_livejournal'] = 'Livejournal.com';
$_['text_openid'] = 'OpenID';
$_['text_lastfm'] = 'lastFM';
$_['text_linkedin'] = 'Linkedin.com';
$_['text_liveid'] = 'LiveID';
$_['text_soundcloud'] = 'SoundCloud';
$_['text_steam'] = 'Steam';
$_['text_flickr'] = 'Flickr.com';
$_['text_uid'] = 'uid.me';
$_['text_youtube'] = 'YouTube';
$_['text_webmoney'] = 'Webmoney';
$_['text_foursquare'] = 'Foursquare';
$_['text_dudu'] = 'Dudu.com';
$_['text_tumblr'] = 'Tumblr';
$_['text_vimeo'] = 'Vimeo.com';
$_['text_wargaming'] = 'Wargaming.net';

// Button
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_social_status'] = 'Авторизация через социальные сети';
$_['entry_social_sort'] = 'Сортировка по предпочтениям';
$_['entry_social_sort_desc'] = 'Cоцсети будут отсортированы для каждого покупателя в соответствии с его личными предпочтениями.';
$_['entry_social_networks'] = 'Социальные сети';
$_['entry_social_title'] = 'Заголовок блока соц.сетей';
// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_save_settings'] = 'Не удалось сохранить конфигурационные файлы';




$_['text_module_version'] = '';
