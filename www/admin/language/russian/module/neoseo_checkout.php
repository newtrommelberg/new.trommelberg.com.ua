<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Оформление заказа';
$_['heading_title_raw'] = 'NeoSeo Оформление заказа';

// Tab
$_['tab_general'] = 'Основные';
$_['tab_customer'] = 'Покупатель';
$_['tab_payment'] = 'Оплата';
$_['tab_shipping'] = 'Доставка';
$_['tab_shipping_type'] = 'Типы доставки';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';



// Text
$_['text_module'] = 'Модули';
$_['text_success'] = 'Успех: Вы успешно изменили настройки модуля NeoSeo Оформление заказа!';
$_['text_title'] = 'Оформление заказа';
$_['text_radio_type'] = 'Переключатель';
$_['text_select_type'] = 'Список';
$_['text_text_type'] = 'Text Type';
$_['text_city_select_default'] = 'Вручную вводить страну и регион';
$_['text_city_select_cities'] = 'Автоматическая подставновка страны и региона по названию города';
$_['text_clear_log'] = 'Очистить логи';
$_['text_success_clear'] = 'Логи успешно очищены';
$_['text_type-0'] = 'Физическое лицо';
$_['text_type-1'] = 'Юридическое лицо';
$_['text_dependency_disabled'] = 'Оплата и доставка не зависят друг от друга';
$_['text_dependency_payment_for_shipping'] = 'Оплата зависит от доставки';
$_['text_dependency_shipping_for_payment'] = 'Доставка зависит от оплаты';
$_['text_types_of_customers'] = 'Типы покупателей';
$_['text_block_customers'] = 'Поля для блока "Покупатель":';
$_['text_block_payment'] = 'Поля для метода оплаты';
$_['text_block_shipping'] = 'Поля для метода доставки';
$_['text_shipping'] = 'Метод доставки';
$_['text_payment'] = 'Метод оплаты';
$_['text_type_shipping'] = 'Тип доставки';
$_['text_pick_your_own'] = 'Забрать самостоятельно';
$_['text_order_delivery'] = 'Заказать доставку';
$_['text_select_all'] = 'Выделить все';
$_['text_name'] = 'Название:';
$_['text_field'] = 'Поле:';
$_['text_type'] = 'Тип:';
$_['text_identifier'] = 'Идентификатор:';
$_['text_mask'] = 'Маска ввода:';
$_['text_initial_value'] = 'Начальное значение:';
$_['text_show'] = 'Показывать:';
$_['text_required'] = 'Обязательное:';
$_['text_only_register'] = 'Только для регистрации:';

// Entry
$_['entry_status'] = 'Состояние: <a class="tooltip-trigger" title="Central function to turn on/off this extension."><i class="fa fa-question-circle"></i></a>';
$_['entry_payment_logo'] = 'Показывать логотип метода оплаты: <a class="tooltip-trigger" title="Display the payment logo for payment methods. This only works when payment method is displayed in radio mode."><i class="fa fa-question-circle"></i></a>';
$_['entry_payment_control'] = 'Выбор метода оплаты: <a class="tooltip-trigger" title="Sets the mode payment methods are displayed in. Either radio buttons or select drop down."><i class="fa fa-question-circle"></i></a>';
$_['entry_shipping_control'] = 'Выбор метода доставки: <a class="tooltip-trigger" title="Sets the mode shipping methods are displayed in. Either radio buttons or select drop down."><i class="fa fa-question-circle"></i></a>';
$_['entry_shipping_city_select'] = 'Ввод города';
$_['entry_shipping_country_select'] = 'Ввод страны';
$_['entry_shipping_country_default'] = 'Страна по умолчанию';
$_['entry_shipping_zone_default'] = 'Регион по умолчанию';
$_['entry_shipping_city_default'] = 'Город по умолчанию';
$_['entry_novaposhta_city_name'] = 'Город по умолчанию для доставки "Новая почта"';
$_['entry_novaposhta_city_name_desc'] = 'В системе обнаружен установленный модуль "NeoSeo Новая почта" с функцией создания накладных. Для его корректной работы необходимо выбрать город по умолчанию для получателя, если Вы хотите использовать параметр "Город по умолчанию"';
$_['entry_shipping_title'] = 'Вывод названия группы доставок';
$_['entry_shipping_novaposhta'] = 'Метод доставки для новой почты';
$_['entry_warehouse_types'] = 'Тип отделения новой почты';
$_['entry_compact'] = 'Компактный вывод';
$_['entry_agreement_id'] = 'Требовать принятие условий соглашения';
$_['entry_agreement_default'] = 'Начальное значение для галки "Я принимаю условия соглашения"';
$_['entry_agreement_text'] = 'Выводить требования текстом';
$_['entry_stock_control'] = 'Проверять наличие товара из корзины';
$_['entry_min_amount'] = 'Минимальная сумма заказа';
$_['entry_use_shipping_type'] = 'Использовать типы доставки';
$_['entry_dependency_type'] = 'Тип зависимости';
$_['entry_dropped_cart_template'] = 'Шаблон сообщения брошенной корзины';
$_['entry_dropped_cart_email_subject'] = 'Тема сообщения брошенной корзины';
$_['entry_onestep'] = 'Оформление в 1 шаг';
$_['entry_cart_redirect'] = 'Перенаправлять с корзины на оплату';
$_['entry_hide_menu'] = 'Скрыть меню во время оформления заказа';
$_['entry_hide_footer'] = 'Скрыть футер во время оформления заказа';
$_['entry_api_key'] = 'API key Новой почты';
$_['entry_api_key_desc'] = 'Используется для обновления отделений новой почты. <a href="https://devcenter.novaposhta.ua/" target="_blank">Получить ключ</a>';
$_['entry_use_international_phone_mask'] = 'Использовать маску для телефона ';
$_['entry_use_international_phone_mask_desc'] = 'Использовать плагин International Telephone Input для ввода и проверки международных телефонных номеров. Отменяет введенную маску в настройках покупателя.';

// Technical
$_['entry_debug'] = 'Отладка: <a class="tooltip-trigger" title="Turn on debug mode for checkout. Only turn this on if you know what you are doing."><i class="fa fa-question-circle"></i></a>';
$_['entry_payment_reloads_cart'] = 'Обновлять корзину при смене метода оплаты: <a class="tooltip-trigger" title="Only enable if your payment methods have surcharges. Disable to reduce ajax requests."><i class="fa fa-question-circle"></i></a>';
$_['entry_shipping_reloads_cart'] = 'Обновлять корзину при смене метода доставки: <a class="tooltip-trigger" title="Only enable if your payment methods are dependent on your shipping methods. Disable to reduce ajax requests."><i class="fa fa-question-circle"></i></a>';


// Module
$_['entry_coupon'] = 'Display Coupon Module: <a class="tooltip-trigger" title="Turn on/off the coupon module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_voucher'] = 'Display Voucher Module: <a class="tooltip-trigger" title="Turn on/off the voucher module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_reward'] = 'Display Reward Module: <a class="tooltip-trigger" title="Turn on/off the reward module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_cart'] = 'Display Cart Module: <a class="tooltip-trigger" title="Turn on/off the cart module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_shipping_module'] = 'Display Shipping Method Module: <a class="tooltip-trigger" title="Turn on/off the shipping method module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_payment_module'] = 'Display Payment Method Module: <a class="tooltip-trigger" title="Turn on/off the payment method module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_login_module'] = 'Display Login Module: <a class="tooltip-trigger" title="Turn on/off the login module on the checkout page."><i class="fa fa-question-circle"></i></a>';

$_['entry_aways_show_delivery_block'] = 'Показывать доставку перед выбором города';
$_['entry_aways_show_delivery_block_desc'] = 'Внимание! Не все методы доставки могут быть отображены. Некоторые методы доставки требуют обязательного выбора города до того, как они будут показаны. При включении данной опции блок ввода города будет скрыт, и отображен только для служб доставки, выбранных ниже.';
$_['entry_shipping_require_city'] = "Список методов доставки, которым необходим выбор города";
$_['entry_shipping_require_city_desc'] = "При использовании опции 'Показывать доставку перед выбором города' блок выбора города будет отображен только для выбранных методов доставки";

// Button
$_['button_add'] = 'Добавить';
$_['button_continue'] = 'Применить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_clear_log'] = 'Очистить лог';

// Error
$_['error_permission'] = '';
$_['error_warehouse_types'] = 'Ошибка выполнения запроса получения типов отделений. Смотрите лог для подробностей.';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';

$_['mail_support'] = '';
$_['module_licence'] = '';
$_['text_module_version'] = '';
