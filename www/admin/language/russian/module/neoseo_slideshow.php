<?php
// Heading
$_['heading_title']    = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Слайдшоу';
$_['heading_title_raw']= 'NeoSeo Слайдшоу';
$_['new_title'] 	   = 'Слайдшоу';

//Tab
$_['tab_general'] 	   = 'Параметры';
$_['tab_logs'] 		   = 'Логи';
$_['tab_license'] 	   = 'Лицензия';
$_['tab_support'] 	   = 'Поддержка';

//Button
$_['button_clear_log'] = 'Очистить лог';
$_['button_recheck']   = 'Проверить еще раз';
$_['button_save'] 	   = 'Сохранить';
$_['button_save_and_close']= 'Сохранить и Закрыть';
$_['button_close'] 	   = 'Закрыть';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки модуля успешно обновлены!';
$_['text_edit']        = 'Редактирование модуля Слайдшоу';
$_['text_clear_log']   = 'Очистить лог';
$_['text_module_version']= '';
$_['text_success_clear']= 'Лог файл успешно очищен!';
$_['text_default_title']= 'Слайдшоу';

// Entry
$_['entry_name']       = 'Название';
$_['entry_banner']     = 'Основные слайды';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_banner2']    = 'Дополнительные слайды';
$_['entry_width2']     = 'Ширина';
$_['entry_height2']    = 'Высота';
$_['entry_status']     = 'Статус';
$_['entry_debug'] 	   = 'Отладка';

// Error
$_['error_permission'] = 'У Вас нет прав для управления модулем Слайдшоу!';
$_['error_name']       = 'Название модуля должно быть от 3 до 64 символов!';
$_['error_width']      = 'Необходимо указать Ширину!';
$_['error_height']     = 'Необходимо указать Высоту!';





$_['text_module_version']   = '';