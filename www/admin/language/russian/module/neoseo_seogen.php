<?php

$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Генератор ЧПУ и метаданных</span>';
$_['heading_title_raw'] = 'NeoSeo Генератор ЧПУ и метаданных';

// Column
$_['column_status_name'] = 'Название статуса';
$_['column_template_subject'] = 'Сообщение';

// Text
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_success_seogen'] = 'Генерация прошла успешно!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_select_template'] = 'Укажите шаблон';
$_["text_new_order"] = 'Получен новый заказ';
$_['text_customer_templates'] = 'Сообщения покупателям';
$_['text_admin_templates'] = 'Сообщения админам';
$_['text_generate'] = 'Генерировать';

// Button
$_['button_clear_log'] = 'Очистить лог';
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_clear_log'] = 'Очистить журнал';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_insert'] = 'Создать';
$_['button_delete'] = 'Удалить';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_rewrite'] = 'Перезапись:';
$_['entry_debug'] = 'Отладка:';
$_['entry_limit_title'] = 'Лимит длины title:';
$_['entry_limit_description'] = 'Лимит длины description:';
$_['entry_option_name_value_separator'] = 'Разделитель между именем и значением опции:';
$_['entry_option_values_separator'] = 'Разделитель между значениями опций:';
$_['entry_options_separator'] = 'Разделитель между опциями:';
$_['entry_attribute_name_value_separator'] = 'Разделитель между именем и значением атрибута:';
$_['entry_attribute_values_separator'] = 'Разделитель между значениями атрибута:';
$_['entry_attributes_separator'] = 'Разделитель между атрибутами:';
$_['entry_language'] = 'Язык для формирования ЧПУ:';
$_['entry_category'] = 'Категории:';

$_['entry_products_seo_url'] = 'SEO URL:<br><span class="help">Доступные теги: [product_id], [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name]</span>';
$_['entry_products_h1'] = 'HTML-тег h1:<br><span class="help">Доступные теги: [product_id], [product_h1], [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name]</span>';
$_['entry_products_title'] = 'HTML-тег title:<br><span class="help">Доступные теги: [product_id], [product_name], [product_title], [sku], [price], [model_name], [manufacturer_name], [category_name]</span>';
$_['entry_products_description'] = 'HTML-тег description:<br><span class="help">Доступные теги: [product_id], [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name], [product_options], [product_attributes], [product_description]</span>';
$_['entry_products_product_description'] = 'Описание продукта:<br><span class="help"> Доступные теги: [product_id], [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name], [product_options], [product_attributes], [product_description]</span>';
$_['entry_products_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступные теги: [product_id], [product_name], [product_keyword], [sku], [price], [model_name], [manufacturer_name], [category_name], [product_options], [product_attributes]</span>';

$_['entry_categories_seo_url'] = 'SEO URL:<br><span class="help">Доступные теги: [category_name],[parent_category_name]</span>';
$_['entry_categories_h1'] = 'HTML-тег h1:<br><span class="help">Доступные теги: [category_name], [category_h1],[parent_category_name]</span>';
$_['entry_categories_title'] = 'HTML-тег title:<br><span class="help">Доступные теги: [category_name], [category_title],[parent_category_name]</span>';
$_['entry_categories_description'] = 'HTML-тег description:<br><span class="help">Доступные теги: [category_name], [category_description],[parent_category_name]</span>';
$_['entry_categories_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступные теги: [category_name], [category_keyword],[parent_category_name]</span>';

$_['entry_manufacturers_seo_url'] = 'SEO URL:<br><span class="help">Доступные теги: [manufacturer_name]</span>';
$_['entry_manufacturers_h1'] = 'HTML-тег h1:<br><span class="help">Доступные теги: [manufacturer_name], [manufacturer_h1]</span>';
$_['entry_manufacturers_title'] = 'HTML-тег title:<br><span class="help">Доступные теги: [manufacturer_name], [manufacturer_title]</span>';
$_['entry_manufacturers_description'] = 'HTML-тег description:<br><span class="help">Доступные теги: [manufacturer_name], [manufacturer_description]</span>';
$_['entry_manufacturers_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступные теги: [manufacturer_name], [manufacturer_keyword]</span>';

$_['entry_articles_seo_url'] = 'SEO URL:<br><span class="help">Доступные теги: [information_name]</span>';
$_['entry_articles_h1'] = 'HTML-тег h1:<br><span class="help">Доступные теги: [information_title], [information_h1]</span>';
$_['entry_articles_title'] = 'HTML-тег title:<br><span class="help">Доступные теги: [information_title], [information_title]</span>';
$_['entry_articles_description'] = 'HTML-тег description:<br><span class="help">Доступные теги: [information_title], [information_description]</span>';
$_['entry_articles_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступные теги: [information_title], [information_keyword]</span>';

$_['entry_blogs_articles_seo_url'] = 'SEO URL:<br><span class="help">Доступные теги: [blog_article_name]</span>';
$_['entry_blogs_articles_h1'] = 'HTML-тег h1:<br><span class="help">Доступные теги: [blog_article_name], [blog_article_h1]</span>';
$_['entry_blogs_articles_title'] = 'HTML-тег title:<br><span class="help">Доступные теги: [blog_article_name], [blog_article_title]</span>';
$_['entry_blogs_articles_description'] = 'HTML-тег description:<br><span class="help">Доступные теги: [blog_article_name], [blog_article_description]</span>';
$_['entry_blogs_articles_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступные теги: [blog_article_name], [blog_article_keyword]</span>';

$_['entry_blogs_authors_seo_url'] = 'SEO URL:<br><span class="help">Доступные теги: [blog_author_name]</span>';
$_['entry_blogs_authors_h1'] = 'HTML-тег h1:<br><span class="help">Доступные теги: [blog_author_name], [blog_author_h1]</span>';
$_['entry_blogs_authors_title'] = 'HTML-тег title:<br><span class="help">Доступные теги: [blog_author_name], [blog_author_title]</span>';
$_['entry_blogs_authors_description'] = 'HTML-тег description:<br><span class="help">Доступные теги: [blog_author_name], [blog_author_description]</span>';
$_['entry_blogs_authors_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступные теги: [blog_author_name], [blog_author_keyword]</span>';

$_['entry_blogs_categories_seo_url'] = 'SEO URL:<br><span class="help">Доступные теги: [blog_category_name]</span>';
$_['entry_blogs_categories_h1'] = 'HTML-тег h1:<br><span class="help">Доступные теги: [blog_category_name], [blog_category_h1]</span>';
$_['entry_blogs_categories_title'] = 'HTML-тег title:<br><span class="help">Доступные теги: [blog_category_name], [blog_category_title]</span>';
$_['entry_blogs_categories_description'] = 'HTML-тег description:<br><span class="help">Доступные теги: [blog_category_name], [blog_category_description]</span>';
$_['entry_blogs_categories_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступные теги: [blog_category_name], [blog_category_keyword]</span>';

$_['entry_filter_page_manufacturer'] = 'Наименование для производителя:<br><span class="help">Укажите наименование для производителя, если он используется в посадочной</span>';
$_['entry_filter_page_h1'] = 'H1 :<br><span class="help">Доступные теги: [filter_page_name], [filter_page_h1], [filter_page_title], [filter_page_category], [filter_page_filters]</span>';
$_['entry_filter_page_title'] = 'Document Title :<br><span class="help">Доступные теги: [filter_page_name], [filter_page_h1], [filter_page_title], [filter_page_category], [filter_page_filters]</span>';
$_['entry_filter_page_description'] = 'Meta Description:<br><span class="help">Доступные теги: [filter_page_name], [filter_page_h1], [filter_page_description], [filter_page_category], [filter_page_filters]</span>';
$_['entry_filter_page_keywords'] = 'Meta Keywords :<br><span class="help">Доступные теги: [filter_page_name], [filter_page_h1], [filter_page_keyword], [filter_page_category], [filter_page_filters]</span>';
$_['entry_instruction'] = 'Инструкция к модулю:';
$_['entry_history'] = 'История изменений:';
$_['entry_faq'] = 'Часто задаваемые вопросы:';

$_['entry_cron'] = 'CRON';

// Tabs
$_['tab_general'] = 'Общее';
$_['tab_magazine'] = 'Магазин';
$_['tab_blogs'] = 'Блоги';
$_['tab_filters'] = 'Фильтры';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';
$_['tab_usefull'] = 'Полезные ссылки';

// Sub tabs
$_['subtab_magazine_categories'] = 'Категории';
$_['subtab_magazine_products'] = 'Товары';
$_['subtab_magazine_manufacturers'] = 'Производители';
$_['subtab_magazine_articles'] = 'Статьи';

$_['subtab_blogs_categories'] = 'Категории';
$_['subtab_blogs_authors'] = 'Авторы';
$_['subtab_blogs_articles'] = 'Статьи';

$_['subtab_filter_pages'] = 'NeoSeo Посадочные страницы';

// Params
$_['params_products_seo_url'] = '[product_name]';
$_['params_products_h1'] = '[product_h1]';
$_['params_products_title'] = '[product_title]';
$_['params_products_keywords'] = '[product_keyword]';
$_['params_products_description'] = '[product_description]';
$_['params_products_product_description'] = '[category_name] [product_name] [model_name] [manufacturer_name]';

$_['params_categories_seo_url'] = '[category_name]';
$_['params_categories_h1'] = '[category_h1]';
$_['params_categories_title'] = '[category_title]';
$_['params_categories_description'] = '[category_description]';
$_['params_categories_keywords'] = '[category_keyword]';
$_['params_categories_parent_name'] = '[parent_category_name]';

$_['params_manufacturers_seo_url'] = '[manufacturer_name]';
$_['params_manufacturers_h1'] = '[manufacturer_h1]';
$_['params_manufacturers_title'] = '[manufacturer_title]';
$_['params_manufacturers_description'] = '[manufacturer_description]';
$_['params_manufacturers_keywords'] = '[manufacturer_keyword]';

$_['params_articles_seo_url'] = '[information_name]';
$_['params_articles_h1'] = '[information_h1]';
$_['params_articles_title'] = '[information_title]';
$_['params_articles_description'] = '[information_description]';
$_['params_articles_keywords'] = '[information_keyword]';

$_['params_blogs_articles_seo_url'] = '[blog_article_name]';
$_['params_blogs_articles_h1'] = '[blog_article_h1]';
$_['params_blogs_articles_title'] = '[blog_article_title]';
$_['params_blogs_articles_description'] = '[blog_article_description]';
$_['params_blogs_articles_keywords'] = '[blog_article_keyword]';

$_['params_blogs_authors_seo_url'] = '[blog_author_name]';
$_['params_blogs_authors_h1'] = '[blog_author_h1]';
$_['params_blogs_authors_title'] = '[blog_author_title]';
$_['params_blogs_authors_description'] = '[blog_author_description]';
$_['params_blogs_authors_keywords'] = '[blog_author_keyword]';

$_['params_blogs_categories_seo_url'] = '[blog_category_name]';
$_['params_blogs_categories_h1'] = '[blog_category_h1]';
$_['params_blogs_categories_title'] = '[blog_category_title]';
$_['params_blogs_categories_description'] = '[blog_category_description]';
$_['params_blogs_categories_keywords'] = '[blog_category_keyword]';

$_['params_filter_page_manufacturer'] = 'Производитель';
$_['params_filter_page_title'] = '[filter_page_h1]';
$_['params_filter_page_title'] = '[filter_page_title]';
$_['params_filter_page_description'] = '[filter_page_description]';
$_['params_filter_page_keywords'] = '[filter_page_keyword]';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';




$_['text_module_version'] = '';

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-generator-chpu-i-metadannyh">https://neoseo.com.ua/nastroyka-modulya-generator-chpu-i-metadannyh</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/generator-chpu-i-metadannyh#module_history">https://neoseo.com.ua/generator-chpu-i-metadannyh#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/generator-chpu-i-metadannyh#faqBox">https://neoseo.com.ua/generator-chpu-i-metadannyh#faqBox</a>';