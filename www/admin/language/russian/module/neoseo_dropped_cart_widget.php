<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Виджет брошенная корзина на панели управления</span>';
$_['heading_title_raw'] = 'NeoSeo Виджет брошенная корзина';

//Tabs
$_['tab_general'] = 'Параметры';
$_['tab_header'] = 'Шапка';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_clear'] = 'Очистить';
$_['text_title'] = 'Брошенная корзина';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';

// Entry
$_['entry_debug'] = 'Отладочный режим<br /><span class="help">В логи модуля будет писаться различная информация для разработчика модуля.</span>';
$_['entry_status'] = 'Статус';
$_['entry_show_dropped_cart_number'] = 'Выводить номер записи';
$_['entry_show_email'] = 'Выводить email';
$_['entry_show_customer'] = 'Выводить покупателя';
$_['entry_show_telephone'] = 'Выводить телефон';
$_['entry_show_total'] = 'Выводить сумму заказа';
$_['entry_show_date_modified'] = 'Выводить дату изменения записи';
$_['entry_show_notification'] = 'Выводить количество оповещений';
$_['entry_limit'] = 'Количество выводимых записей';
$_['entry_title'] = 'Заголовок блока';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';



