<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Категория';
$_['heading_title_raw'] = 'NeoSeo Блог Категория';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Модуль Блог Категория удачно изменен!';
$_['text_edit']        = 'Редактирование модуля Категория Блога';
$_['text_module_version'] = '';

// Entry
$_['entry_name']       = 'Название модуля';
$_['entry_title']      = 'Заголовок модуля';
$_['entry_root_category']     = 'Корневая категория';
$_['entry_status']     = 'Статус';
$_['entry_template']   = 'Шаблон';

// Error
$_['error_permission'] = 'У вас нету прав изменять модуль Блог Категория!';