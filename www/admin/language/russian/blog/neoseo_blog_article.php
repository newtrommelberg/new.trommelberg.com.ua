<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Статьи</p>';
$_['heading_title_raw'] = 'NeoSeo Блог Статьи';

// Text
$_['text_success'] = 'Статья успешно изменена!';
$_['text_edit'] = 'Редактирование статьи';
$_['text_add'] = 'Создание статьи';
$_['text_module_version'] = '';

// Buttons
$_['button_add_articles'] = 'Добавить статью';

// Columns
$_['column_article_name'] = 'Название статьи';
$_['column_author_name'] = 'Имя автора';
$_['column_category'] = 'Категория';
$_['column_sort_order'] = 'Сортировка';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата добавления';
$_['column_action'] = 'Действие';


// Help
$_['help_title'] = 'Имя статьи должно быть уникальным.';
$_['help_teaser'] = 'Отображается в списке статей';
$_['help_seo_url'] = 'Должно быть уникальным, не используйте пробелы, заменяйте их на "-".';
$_['help_image'] = 'Изображение статьи.';
$_['help_related_article_name'] = 'Добавляется через автодополенение.';


// Entry
$_['entry_title'] = 'Название статьи:';
$_['entry_description'] = 'Описание:';
$_['entry_teaser'] = 'Краткое описание:';
$_['entry_author_name'] = 'Имя автора:';
$_['entry_meta_title'] = 'HTML-тег Title';
$_['entry_meta_h1'] = 'HTML-тег H1';
$_['entry_meta_description'] = 'Meta Description:';
$_['entry_meta_keyword'] = 'Meta keyword:';
$_['entry_allow_comment'] = 'Разрешить комментарии?:';
$_['entry_seo_url'] = 'SEO URL:';
$_['entry_image'] = 'Изображение:';
$_['entry_images'] = 'Изображения галереи:';
$_['entry_sort_order'] = 'Сортировка:';
$_['entry_status'] = 'Статус:';
$_['entry_category'] = 'Категория:';
$_['entry_main_category'] = 'Главная категория';
$_['entry_store'] = 'Магазин:';
$_['entry_layout'] = 'Шаблон:';
$_['entry_related_products'] = 'Связанные товары:';
$_['entry_related_products_product'] = 'Товар:';
$_['entry_related_products_category'] = 'Категория:';
$_['entry_related_products_manufacturer'] = 'Производитель:';
$_['entry_related_articles'] = 'Связанные статьи';
$_['entry_related_article_name'] = 'Название статьи:';
$_['entry_date_added'] = 'Дата создания:';
$_['entry_date_modified'] = 'Дата модификации:';

// Error
$_['error_warning'] = 'Проверьте форму на ошибки!';
$_['error_permission'] = 'У вас нет прав изменять данный материал!';
$_['error_title'] = 'Название статьи должно быть больше 3 и меньше 100 символов!';
$_['error_title_found'] = 'Название статьи существует, оно должно быть уникальным!';
$_['error_description'] = 'Описание должно быть больше 3 символов!';
$_['error_author_name'] = 'Имя автора не может быть пустым!';
$_['error_author_not_found'] = 'Автор не найден!';
$_['error_seo_url'] = 'Этот SEO URL уже используется!';
$_['error_article_related'] = 'Вы не можете удалить статью, потому что есть связанные с ней %s статьи!';
