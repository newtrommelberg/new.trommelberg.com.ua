<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Категории</p>';
$_['heading_title_raw'] = 'NeoSeo Блог Категории';

// Text
$_['text_success'] = 'Категория удачно изменена!';
$_['text_edit'] = 'Редактирование категории';
$_['text_add'] = 'Создание категории';


// Column
$_['column_name'] = 'Название категории';
$_['column_sort_order'] = 'Сортировка';
$_['column_status'] = 'Статус';
$_['column_action'] = 'Действие';
$_['text_module_version'] = '';

// Help
$_['help_seo_url'] = 'Должно быть уникальным, не используйте пробелы, заменяйте их на "-".';
$_['help_top'] = 'Отображается в меню, работает только для родительских категорий.';

// Entry
$_['entry_name'] = 'Название категории:';
$_['entry_meta_title'] = 'HTML-тег Title';
$_['entry_meta_h1'] = 'HTML-тег H1';
$_['entry_meta_keyword'] = 'Meta Tag Keywords:';
$_['entry_teaser'] = 'Краткое описание:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description'] = 'Описание:';
$_['entry_parent'] = 'Родительская категория:';
$_['entry_store'] = 'Магазин:';
$_['entry_seo_url'] = 'SEO URL:';
$_['entry_image'] = 'Изображение:';
$_['entry_sort_order'] = 'Сортировка:';
$_['entry_status'] = 'Статус:';
$_['entry_top'] = 'Добавить в меню:';
$_['entry_layout'] = 'Шаблон:';

// Error 
$_['error_warning'] = 'Проверьте форму на наличие ошибок!';
$_['error_permission'] = 'У вас нет прав изменять категории!';
$_['error_name'] = 'Название категории должно быть больше 2 и меньше 255 символов!';
$_['error_seo_url'] = 'Этот SEO URL уже используется!';
$_['error_article'] = 'Эта категория не может быть удалена, так как в ней есть статьи %s!';
