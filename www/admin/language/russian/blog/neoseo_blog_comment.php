<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Комментарии</p>';
$_['heading_title_raw'] = 'NeoSeo Блог Комментарии';

// Text
$_['text_success']        = 'Комментарий успешно обновлен!';
$_['text_edit']           = 'Изменить комментарий';
$_['text_add']            = 'Добавить комментарий';
$_['text_module_version'] = '';
$_['text_parent_comment'] = 'Родительский коментарий';
$_['text_child_comments'] = 'Ответы на коментарий';
$_['text_goto_comment']   = 'перейти';

// Columns
$_['column_article_name'] = 'Название статьи';
$_['column_author_name']  = 'Имя автора';
$_['column_status']       = 'Статус';
$_['column_date_added']   = 'Дата добавления';
$_['column_action']       = 'Действие';
$_['column_comment']      = 'Комментарий';

// Buttons
$_['button_add_reply']    = 'Добавить ответ';

// Help
$_['help_article']        = 'Поиск через автодополнение.';

// Tab
$_['tab_comment']         = 'Комментарий';

// Entry
$_['entry_author']        = 'Имя автора:';
$_['entry_article']       = 'Название статьи:';
$_['entry_rating']        = 'Рейтинг:';
$_['entry_status']        = 'Статус:';
$_['entry_comment']       = 'Комментарий:';
$_['entry_reply_comment'] = 'Ответ';

// Error
$_['error_warning']       = 'Проверьте форму на наличие ошибок!';
$_['error_permission']    = 'У вас нету прав изменять комментарии!';
$_['error_author']        = 'Имя автора должно быть от 3 до 63 символов!';
$_['error_comment']       = 'Текст комментария должен быть от  3 до 1000 символов!';
$_['error_article_name']  = 'Название статьи обязательное!';
$_['error_article_not_found'] = 'Статьи не найдена!';