<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Настройки блога</p>';
$_['heading_title_raw'] = 'NeoSeo Настройки блога';

// Entry
$_['entry_add_to_menu']               = 'Добавить ссылку на блог в меню:';
$_['entry_comment_auto_approval']     = 'Автопроверка комментариев:';
$_['entry_author_block_article']      = 'Добавить инфомацию об авторе в статью:';
$_['entry_product_limit']             = 'К-во товаров в категории:';
$_['entry_comment_limit']             = 'К-во комментариев:';
$_['entry_cache_results']             = 'Кешировать результаты:';
$_['entry_article_time_format']       = 'Формат времени для статьи:';
$_['entry_comment_time_format']       = 'Формат времени для комментариев:';

$_['entry_meta_date']                 = 'Отображать дату в статье:';
$_['entry_meta_author']               = 'Отображать автора в статье:';
$_['entry_meta_comments']             = 'Отображать к-во комментариев в статье:';
$_['entry_meta_category']             = 'Отображать категорию в статье:';

$_['entry_heading']                   = 'Заголовок блога:';
$_['entry_title']                     = 'Блог Title:';
$_['entry_meta_keyword']              = 'Meta Keywords:';
$_['entry_meta_description']          = 'Meta Description:';
$_['entry_articles_block_heading']    = 'Заголовок блока статей:';
$_['entry_products_block_heading']    = 'Заголовок блока товаров:';
$_['entry_comments_block_heading']    = 'Заголовок блока комментариев:';
$_['entry_author_block_heading']      = 'Заголовок блока автора:';
$_['entry_gallery_heading']           = 'Заголовок галереи:';
$_['entry_share_social_code']         = 'Код блока "Поделиться":';

$_['entry_image_article']             = 'Размер изображения статьи';
$_['entry_image_article_list']        = 'Размер изображения статьи в категории';
$_['entry_image_article_block']       = 'Размер изображения статьи в блоке';
$_['entry_image_product_block']       = 'Размер изображения товара в блоке';
$_['entry_image_category_block']      = 'Размер изображения категории';
$_['entry_image_author_block']        = 'Размер изображения автора';
$_['entry_image_gallery_thumb']       = 'Размер изображения галереи';
$_['entry_width']                     = 'Ширина';
$_['entry_height']                    = 'Высота';

// Text
$_['text_success']                    = 'Настройки изменены!';
$_['text_edit']                       = 'Изменить настройки';
$_['text_support']                    = 'Поддержка модуля';
$_['text_module_version'] = '';

// Help 
$_['help_time_format']                = '<a href="http://php.net/manual/en/function.strftime.php" target="_blank">форматы времени</a>';

// Error
$_['error_permission']                = 'У вас нету прав изменять настройки блога!';