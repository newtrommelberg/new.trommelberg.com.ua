<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Авторы</p>';
$_['heading_title_raw'] = 'NeoSeo Блог Авторы';

// Columns
$_['column_author_name'] = 'Имя автора';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата добавления';
$_['column_action'] = 'Действие';

// Text
$_['text_success'] = 'Автор удачно изменен!';
$_['text_edit'] = 'Редактирование автора';
$_['text_add'] = 'Создание автора';
$_['text_module_version'] = '';

// Help
$_['help_name'] = 'Имя автора должно быть уникальным.';
$_['help_seo_url'] = 'Должно быть уникальным, не используйте пробелы, заменяйте их на "-".';

// Entry
$_['entry_name'] = 'Имя автора:';
$_['entry_seo_url'] = 'SEO URL:';
$_['entry_image'] = 'Аватар:';
$_['entry_type'] = 'Тип:';
$_['entry_teaser'] = 'Краткое описание:';
$_['entry_description'] = 'Описание:';
$_['entry_meta_title'] = 'HTML-тег Title';
$_['entry_meta_h1'] = 'HTML-тег H1';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_meta_keyword'] = 'Meta Tag Keywords:';
$_['entry_status'] = 'Статус:';


// Errors
$_['error_warning'] = 'Проверьте форум на наличие ошибок!';
$_['error_permission'] = 'У вас нет прав изменять этот материал!';
$_['error_name'] = 'Имя автора должно быть больше 3 и меньше 255 Символов!';
$_['error_author_found'] = 'Имя автора уже существует, оно должно быть уникальным!';
$_['error_seo_url'] = 'Этот SEO URL уже используется!';
$_['error_article'] = 'Этот автор не может быть удален, так как у него есть %s статей!';
