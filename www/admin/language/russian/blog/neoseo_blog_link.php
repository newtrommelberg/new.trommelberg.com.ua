<?php
$_['text_blogs']          = 'Блог';
$_['text_blog_author']    = 'Авторы';
$_['text_blog_category']  = 'Категории';
$_['text_blog_article']   = 'Статьи';
$_['text_blog_comment']   = 'Комментарии';
$_['text_blog_setting']   = 'Настройки';
$_['text_blog_report']    = 'Статистика';
$_['text_module_version'] = '';
