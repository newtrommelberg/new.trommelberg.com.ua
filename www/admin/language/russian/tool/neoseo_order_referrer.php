<?php

// Heading
$_['heading_title'] = 'Источники заказа';

// Text
$_['text_neoseo_order_referrer'] = 'Источники заказа';
$_['text_success'] = 'Успех: Вы отредактировали настройки!';
$_['text_setting_success'] = 'Успех: Вы изменили настройки менеджера настройки!';
$_['text_module_version'] = '';
$_['text_add'] = 'Добавление источника заказа';
$_['text_edit'] = 'Редактирование источника заказа';
$_['text_list'] = 'Список  источников заказа';
$_['text_import_warning'] = 'ВНИМАНИЕ! Импорт новых данных может привести к перезаписи имеющихся';
$_['text_import_success'] = 'Импорт выполнен успешно!';
$_['text_export_success'] = 'Экспорт выполнен успешно!';

//Entry
$_['entry_name'] = 'Название';
$_['entry_url_param'] = 'Параметр на URL';
$_['entry_url_mask'] = 'Маска в URL';


// Button
$_['button_save'] = 'Сохранить';
$_['button_close'] = 'Закрыть';
$_['button_import'] = 'Импорт';
$_['button_export'] = 'Экспорт';

// Column
$_['column_id'] = '№';
$_['column_name'] = 'Название';
$_['column_url_mask'] = 'Маска на URL';
$_['column_url_param'] = 'Параметр в URL';
$_['column_action'] = 'Действия';

// Error
$_['error_permission'] = 'Ошибка: У вас нет прав на измененеи данных модуля!';
$_['error_file'] = 'Ошибка: Не указан файл импорта!';
$_['error_no_selection'] = 'Ошибка: Укажите элементы для удаления!';
