<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Менеджер ЧПУ</span>';
$_['heading_title_raw'] = 'NeoSeo Менеджер ЧПУ';
$_['text_neoseo_route_manager'] = 'Менеджер ЧПУ';

//Tab
$_['tab_general'] = 'Параметры';
$_['tab_support'] = 'Поддержка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';

// Button
$_['button_clear_cache'] = 'Сбросить кэш';
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_filter'] = 'Фильтр';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладка:';

// Text
$_['text_module_version'] = '';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_success_options'] = 'Настройки модуля обновлены!';
$_['text_success'] = 'SEO Keyword успешно обновлены!';
$_['text_success_clear'] = 'Кэш SEO успешно сброшен!';
$_['text_default'] = 'По умолчанию';
$_['text_module'] = 'Модули';
$_['text_add'] = 'Добавление ЧПУ';
$_['text_module'] = 'Модули';
$_['text_list'] = 'Список ЧПУ';
$_['text_clear_log'] = 'Очистить лог';
$_['text_description'] = '<p>Описание</p>';

// Column
$_['column_query'] = 'Query';
$_['column_keyword'] = 'SEO Keyword';
$_['column_action'] = 'Действие';