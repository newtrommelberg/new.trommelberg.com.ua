<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">Улучшения для Интернет-магазина</p>';
$_['heading_title_raw'] = 'NeoSeo Улучшения для ИМ';

// Columns
$_['column_module_name'] = 'Название дополнения';
$_['column_action'] = 'Действие';

//Text
$_['text_list'] = 'Список модулей';
$_['text_install'] = 'Установлено';

//Buttons
$_['button_more'] = 'Узнать больше';
$_['button_view'] = 'Перейти к дополнению';

//Params
$_['param_main_page'] = 'Главная страница';
$_['param_category_page'] = 'Страница категории';
$_['param_product_page'] = 'Карточка товара';
$_['param_checkout_page'] = 'Оформление заказа';
$_['param_account_page'] = 'Личный кабинет';
$_['param_admin_home_page'] = 'Административная панель';
$_['param_integration_services'] = 'Интеграция с внешними сервисами';
$_['param_adding_products'] = 'Наполнение магазина товарами';
$_['param_technical_modules'] = 'Технические модули';
$_['param_advancement'] = 'Для продвижения';
$_['param_improvement'] = 'Улучшения';
$_['param_seo_structure,seo_tuning,seo_blog,product_feed,manager_actions'] = 'Для продвижения';