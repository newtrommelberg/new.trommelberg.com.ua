<?php

// Heading
$_['heading_title'] = 'Брошенная корзина';
$_['heading_title_info'] = 'Просмотр корзины';
$_['text_dropped_cart'] = 'Брошенная корзина';

$_['column_email'] = 'Email';
$_['column_name'] = 'Имя';
$_['column_total'] = 'Итого';
$_['column_subtotal'] = 'Предварительный итог';
$_['column_modified'] = 'Изменено';
$_['column_phone'] = 'Телефон';
$_['column_action'] = 'Действие';
$_['column_quantity'] = 'Количество';
$_['column_options'] = 'Опции';
$_['column_price'] = 'Цена';
$_['column_notification_count'] = 'Отправлено сообщений';

$_['entry_email'] = 'Email';
$_['entry_name'] = 'Имя';
$_['entry_phone'] = 'Телефон';
$_['entry_total'] = 'Итого';
$_['entry_modified'] = 'Изменено';
$_['entry_notification_count'] = 'Отправлено сообщений';

$_['error_missing_cart_id'] = 'Отсутствует идентификатор корзины';
$_['error_cart_not_found'] = 'Корзина не найдена';
$_['error_template_not_found'] = 'Шаблон "%s" не найден';

$_['email_send_successfully'] = 'Сообщение отправлено';
$_['email_sending_error'] = 'Ошибка отправки сообщения';

$_['button_send_notification'] = 'Отправить сообщение';
$_['button_view'] = 'Посмотреть';
