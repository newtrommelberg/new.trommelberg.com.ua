<?php

$_['heading_title'] = 'Обратный звонок';

//Columns
$_['column_callback_number'] = '№';
$_['column_customer'] = 'Покупатель';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата';
$_['column_comment'] = 'Комментарий';
$_['column_manager'] = 'Менеджер';
$_['column_message'] = 'Сообщение';

//Entry
$_['entry_domain'] = 'Домен';
$_['entry_email'] = 'Email';

//Text
$_['text_status_enabled'] = 'Обработан';
$_['text_status_disabled'] = 'Новый';
$_['text_module_turnof'] = 'Модуль отключен';
$_['text_no_information'] = 'Модуль "NeoSeo Обратный звонок" отключен';
$_['text_module_uninstall'] = 'Нет данных. Модуль отсутствует';
$_['text_buy'] = 'Купить';
$_['text_error'] = 'Домен и почта обязательны для заполнения';
$_['text_success'] = 'Ваша заявка на info@neoseo.com.ua успешно отправлена.';
$_['text_link'] = 'Зачем это?';

//Buttons
$_['button_request'] = 'Оставить заявку';
$_['button_details'] = 'Подробнее';
$_['button_enabled'] = 'Настроить модуль';

//Params
$_['params_email'] = 'info@neoseo.com.ua';
$_['params_subject'] = 'Новая заявка на покупку модуля "NeoSeo Обратный звонок"';
$_['params_message'] = "Поступила новая заявка на покупку модуля 'NeoSeo Обратный звонок'.<br>Домен: %s. <br>Почта: %s.<br>";
$_['params_link'] = 'https://neoseo.com.ua/zayavka-na-obratnyj-zvonok';