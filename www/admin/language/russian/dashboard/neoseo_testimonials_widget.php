<?php

$_['heading_title'] = 'Отзывы о магазине';

//Entry
$_['entry_domain'] = 'Домен';
$_['entry_email'] = 'Email';

//Columns
$_['column_testimonial_number'] = '№';
$_['column_author'] = 'Автор';
$_['column_text'] = 'Отзыв';
$_['column_admin_text'] = 'Ответ администратора';
$_['column_rating'] = 'Рейтинг';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата создания';
$_['column_youtube'] = 'Ссылка на YouTube';
$_['column_image'] = 'Изображение';

//Text
$_['text_status_enabled'] = 'Опубликован';
$_['text_status_disabled'] = 'Скрыт';
$_['text_module_turnof'] = 'Модуль отключен';
$_['text_no_information'] = 'Модуль "NeoSeo Отзывы о магазине" отключен';
$_['text_module_uninstall'] = 'Нет данных. Модуль отсутствует';
$_['text_buy'] = 'Купить';
$_['text_error'] = 'Домен и почта обязательны для заполнения';
$_['text_success'] = 'Ваша заявка на info@neoseo.com.ua успешно отправлена.';
$_['text_link'] = 'Зачем это?';

//Buttons
$_['button_request'] = 'Оставить заявку';
$_['button_details'] = 'Подробнее';
$_['button_enabled'] = 'Настроить модуль';

//Params
$_['params_email'] = 'info@neoseo.com.ua';
$_['params_subject'] = 'Новая заявка на покупку модуля "NeoSeo Отзывы о магазине"';
$_['params_message'] = "Поступила новая заявка на покупку модуля 'NeoSeo Отзывы о магазине'.<br>Домен: %s. <br>Почта: %s.<br>";
$_['params_link'] = 'https://neoseo.com.ua/otzyvy-pokupatelej';
