<?php
$_['heading_title'] = 'Брошенная корзина';

//Columns
$_['column_dropped_cart_number'] = '№';
$_['column_email'] = 'Email';
$_['column_customer'] = 'Покупатель';
$_['column_telephone'] = 'Телефон';
$_['column_total'] = 'Сумма';
$_['column_date'] = 'Дата изменения:';
$_['column_action'] = 'Действие';
$_['column_notification'] = 'Оповещений:';


//Entry
$_['entry_domain'] = 'Домен';
$_['entry_email'] = 'Email';

//Text
$_['text_module_turnof'] = 'Модуль отключен';
$_['text_no_information'] = 'Модуль "NeoSeo Оформление заказа" отключен';
$_['text_module_uninstall'] = 'Нет данных. Модуль отсутствует"';
$_['text_buy'] = 'Купить';
$_['text_error'] = 'Домен и почта обязательны для заполнения';
$_['text_success'] = 'Ваша заявка на info@neoseo.com.ua успешно отправлена.';
$_['text_isset_redirect'] = 'Редирект добавлен';
$_['text_link'] = 'Зачем это?';

//Buttons
$_['button_request'] = 'Оставить заявку';
$_['button_notify'] = 'Отправить сообщение';

//Params
$_['params_email'] = 'info@neoseo.com.ua';
$_['params_subject'] = 'Новая заявка на покупку модуля "NeoSeo Оформление заказа"';
$_['params_message'] = "Поступила новая заявка на покупку модуля 'NeoSeo Оформление заказа'.<br>Домен: %s. <br>Почта: %s.<br>";
$_['params_link'] = 'https://neoseo.com.ua/oformlenie-zakaza';