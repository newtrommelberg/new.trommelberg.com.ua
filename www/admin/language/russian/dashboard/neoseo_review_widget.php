<?php

//Columns
$_['column_review_number'] = '№';
$_['column_author'] = 'Автор';
$_['column_text'] = 'Отзыв';
$_['column_rating'] = 'Рейтинг';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата создания';
$_['column_product'] = 'Товар';

//Text
$_['text_status_enabled'] = 'Опубликован';
$_['text_status_disabled'] = 'Скрыт';
$_['text_new_request'] = 'Новых отзывов о товарах:';
$_['params_link'] = 'https://neoseo.com.ua/otzyvy-o-produktah';