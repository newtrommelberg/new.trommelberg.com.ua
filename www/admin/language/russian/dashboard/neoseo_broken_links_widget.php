<?php
$_['heading_title'] = '404-страницы';

//Columns
$_['column_notfound_number'] = '№';
$_['column_ip'] = 'IP';
$_['column_browser'] = 'Browser';
$_['column_request_uri'] = 'Запрашиваемая страница';
$_['column_referer'] = 'Referer';
$_['column_date_record'] = 'Дата создания';
$_['column_action'] = 'Действие';

//Entry
$_['entry_domain'] = 'Домен';
$_['entry_email'] = 'Email';

//Text
$_['text_new_request'] = 'Новых 404-страниц:';
$_['text_module_turnof'] = 'Модуль отключен';
$_['text_no_information'] = 'Модуль "NeoSeo Отслеживание битых ссылок" отключен';
$_['text_module_uninstall'] = 'Нет данных. Модуль отсутствует';
$_['text_buy'] = 'Купить';
$_['text_error'] = 'Домен и почта обязательны для заполнения';
$_['text_success'] = 'Ваша заявка на info@neoseo.com.ua успешно отправлена.';
$_['text_isset_redirect'] = 'Редирект добавлен';
$_['text_link'] = 'Зачем это?';

//Buttons
$_['button_request'] = 'Оставить заявку';
$_['button_add_redirect'] = 'Редирект';
$_['button_details'] = 'Подробнее';
$_['button_enabled'] = 'Настроить модуль';

//Params
$_['params_email'] = 'info@neoseo.com.ua';
$_['params_subject'] = 'Новая заявка на покупку модуля "NeoSeo Отслеживание битых ссылок"';
$_['params_message'] = "Поступила новая заявка на покупку модуля 'NeoSeo Отслеживание битых ссылок'.<br>Домен: %s. <br>Почта: %s.<br>";
$_['params_link'] = 'https://neoseo.com.ua/otslezhivanie-bityh-ssylok';