<?php

//Columns
$_['column_order_number'] = '№';
$_['column_customer'] = 'Покупатель';
$_['column_order_status'] = 'Статус';
$_['column_date_added'] = 'Дата создания';
$_['column_order_total'] = 'Сумма';
$_['column_payment'] = 'Метод оплаты';
$_['column_shipping'] = 'Метод доставки';
$_['column_comment'] = 'Комментарий';
$_['column_order_referrer'] = 'Источник заказа';
$_['column_product_name'] = 'Товар';
$_['column_product_quantity'] = 'Кол-во товара';
$_['column_product_price'] = 'Цена за ед.';
$_['column_product_total'] = 'Всего';

//Text
$_['text_customer_email'] = '';
$_['text_customer_telephone'] = '';
$_['text_order_number'] = 'Заказ №';
$_['text_order_status'] = 'Статус:';
$_['text_order_detail'] = 'Информация о заказе';
$_['text_customer_detail'] = 'Информация о покупателе';
$_['text_product_model'] = 'Модель:';