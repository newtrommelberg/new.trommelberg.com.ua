<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Shipping</p>';
$_['heading_title_raw']    = 'NeoSeo Shipping';

// Tabs
$_['tab_general'] = 'Settings';
$_['tab_methods'] = 'Methods';
$_['tab_logs'] = 'Logs';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';

// Text
$_['text_shipping']      = 'Shipping';
$_['text_success']       = 'Success changed!';
$_['text_edit']          = 'Module settings edit';
$_['text_edit_shipping'] = 'Edit shipping+ method';
$_['text_add']           = 'Add shipping+ method';
$_['text_all_zones']     = 'All Zones';
$_['text_list']          = 'Shipping+ methods list';

// Buttons
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Recheck';
$_['button_clear_log'] = 'Clear log';
$_['button_remove'] = 'Remove';
$_['button_insert'] = 'Add';

// Entry
$_['entry_name']        = 'Title:';
$_['entry_description'] = 'Description:';
$_['entry_geo_zone']    = 'GEO zones:';
$_['entry_geo_zone_desc']    = 'If left without a choice, all zones are used';
$_['entry_status']      = 'Status:';
$_['entry_sort_order']  = 'Sort order:';
$_['entry_price_min']   = 'Minimum shipping price:';
$_['entry_price_max']   = 'Maximum shipping price:';
$_['entry_fix_payment'] = 'Fixed shipping cost:';
$_['entry_cities']      = 'Cities list:';
$_['entry_debug'] = 'Debug mode';
$_['entry_debug_desc'] = 'Some information will be written to module\'s logs';
$_['entry_stores']		= 'Stores';

$_['entry_weight_price_zone']        = 'GEO zones:';
$_['entry_weight_price_types']        = 'Price per weight, (Template: <i> Weight = Cost; Weight = Cost</i>):';
$_['entry_weight_price_types_desc']        = '<b> Template: </b> Weight = Cost; Weight = Cost';

$_['column_name']       = 'Shipping method title';
$_['column_status']     = 'Status';
$_['column_action']     = 'Actions';
$_['column_sort_order'] = 'Sort order';

// Help
$_['help_max']    = 'Leave blank for cancel maximum shipping price limits';
$_['help_min']    = 'Leave blank for cancel minimun shipping price limits';
$_['help_fix_payment']    = 'Leave blank for cancel fixed shipping cost';
$_['help_cities'] = 'Leave blank for cancel city limits.';

// Error
$_['error_permission'] = 'You have no rights to edit this module!';
$_['error_name'] = 'To short or to long or empty title';






