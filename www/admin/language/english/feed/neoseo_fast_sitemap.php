<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Site Map</span>';
$_['heading_title_raw'] = 'NeoSeo Site Map';

// Tab
$_['tab_general'] = 'Options';
$_['tab_store'] = 'Store';
$_['tab_store_information'] = 'Information';
$_['tab_store_category'] = 'Category';
$_['tab_store_manufacturer'] = 'Manufacturer';
$_['tab_store_product'] = 'Product';
$_['tab_blog'] = 'Blog';
$_['tab_blog_module'] = 'Modules';
$_['tab_blog_category'] = 'Categories';
$_['tab_blog_author'] = 'Authors';
$_['tab_blog_article'] = 'Articles';

$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';
$_['tab_support'] = 'Support';

// Button
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Logs';

// Text
$_['text_edit'] = 'Options';
$_['text_success'] = 'Module settings have been successfully updated!';
$_['text_success_clear'] = 'Logs successfully deleted';
$_['text_default'] = 'Default';
$_['text_feed'] = 'Promotion channels';
$_['text_gzip_0'] = 'Disabled';
$_['text_gzip_1'] = 'Level 1';
$_['text_gzip_2'] = 'Level 2';
$_['text_gzip_3'] = 'Level 3';
$_['text_gzip_4'] = 'Level 4';
$_['text_gzip_5'] = 'Level 5';
$_['text_gzip_6'] = 'Level 6';
$_['text_gzip_7'] = 'Level 7';
$_['text_gzip_8'] = 'Level 8';
$_['text_gzip_9'] = 'Level 9';

$_['text_seo_0'] = 'Disabled';
$_['text_seo_1'] = 'SEO Pro';
$_['text_seo_2'] = 'SEO Url';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debug Mode:';
$_['entry_image_status'] = 'Display images:';
$_['entry_image_status_desc'] = 'Not recommended, not all robots understand';
$_['entry_seo_status'] = 'Human-Readable URLs forming system:';
$_['entry_seo_url_include_path'] = 'Hide the full path in the URLs categories and products:';
$_['entry_seo_lang_status'] = 'Multi-language Human-Readable URLs:';
$_['entry_filterpro_seo_status'] = 'SEO FilterPro:';
$_['entry_filterpro_seo_status_desc'] = '';
$_['entry_ocfilter_seo_status'] = 'SEO OcFilter:';
$_['entry_ocfilter_seo_status_desc'] = '';
$_['entry_mfilter_seo_status'] = 'SEO MegaFilter:';
$_['entry_mfilter_seo_status_desc'] = '';
$_['entry_category_brand_status'] = 'Categories with brands:';
$_['entry_category_brand_status_desc'] = 'Links of the category category / brand will be generated';
$_['entry_status_addresses'] = 'Address information:';
$_['entry_partition_status'] = 'Split the map into parts:';
$_['entry_partition_volume'] = 'The size of the map:';
$_['entry_multistore_status'] = 'MultiStore:';
$_['entry_gzip_status'] = 'Compression:';
$_['entry_url'] = 'Link to sitemap.xml:';

$_['entry_category_status'] = 'Display in the site map';
$_['entry_category_url_date'] = 'Date of change';
$_['entry_category_url_frequency'] = 'Update frequency';
$_['entry_category_url_priority'] = 'Upgrade Priority';

$_['entry_manufacturer_status'] = 'Display in the site map';
$_['entry_manufacturer_line_by_tima'] = 'Manufacturers - Lines by T1ma (additional module)';
$_['entry_manufacturer_url_date'] = 'Date of change';
$_['entry_manufacturer_url_frequency'] = 'Update frequency';
$_['entry_manufacturer_url_priority'] = 'Upgrade Priority';

$_['entry_product_status'] = 'Display in the site map';
$_['entry_product_url_date'] = 'Date of change';
$_['entry_product_url_frequency'] = 'Update frequency';
$_['entry_product_url_priority'] = 'Upgrade Priority';

$_['entry_information_status'] = 'Display in the site map';
$_['entry_information_url_date'] = 'Date of change';
$_['entry_information_url_frequency'] = 'Update frequency';
$_['entry_information_url_priority'] = 'Upgrade Priority';


$_['entry_blog_freecart_status'] = 'Output NeoSeo Blog';
$_['entry_blog_pavo_status'] = 'Output Pavo Blog';
$_['entry_blog_seocms_status'] = 'Output SEO CMS Blog';
$_['entry_blog_blogmanager_status'] = 'Output Blog Manager';


$_['entry_blog_category_status'] = 'Display in the site map';
$_['entry_blog_category_url_date'] = 'Date of change';
$_['entry_blog_category_url_frequency'] = 'Update frequency';
$_['entry_blog_category_url_priority'] = 'Upgrade Priority';

$_['entry_blog_author_status'] = 'Display in the site map';
$_['entry_blog_author_url_date'] = 'Date of change';
$_['entry_blog_author_url_frequency'] = 'Update frequency';
$_['entry_blog_author_url_priority'] = 'Upgrade Priority';

$_['entry_blog_article_status'] = 'Display in the site map';
$_['entry_blog_article_url_date'] = 'Date of change';
$_['entry_blog_article_url_frequency'] = 'Update frequency';
$_['entry_blog_article_url_priority'] = 'Upgrade Priority';

// Error
$_['error_permission'] = 'You do not have sufficient permissions to modify "NeoSeo Sitemap"!';




$_['text_module_version'] = '';
