<?php
// Heading
$_['heading_title']     = 'Latest Orders';

// Column
$_['column_order_id']   = 'Order ID';
$_['column_customer']   = 'Customer';
$_['column_status']     = 'Status';
$_['column_total']      = 'Total';

                /* NeoSeo Order Referrer - begin */
                $_['column_referrer']                             = 'Order referrer';
                /* NeoSeo Order Referrer - end */
                
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';