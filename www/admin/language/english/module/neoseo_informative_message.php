<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Informative message</p>';
$_['heading_title_raw'] = 'NeoSeo Informative message';

// Tab
$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_fields'] = 'Fields';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';

// Text
$_['text_success'] = 'Module settings have been successfully updated!';
$_['text_module'] = 'Modules';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear Log';

// Button
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Log';
$_['button_download_log'] = 'Download log file';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debug mode:';
$_['entry_debug_desc'] = 'The module logs will write various information for the module developer';
$_['entry_text'] = 'Message';
$_['entry_message_desc'] = "Enter the message that will be displayed in the site header";
$_['entry_show_close_button'] = 'Display the close button';
$_['entry_close_button_text'] = 'Close button text';
$_['entry_color_background'] = 'Background color';
$_['entry_color_close_button'] = 'Close button color';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_supplier_info'] = 'This field is required!';
$_['error_download_logs'] = 'The log file is empty or missing!';



// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';



$_['param_message'] = "Your application №{id} for calculating the cost is recorded!\nOur manager will contact you as soon as possible";
$_['param_notify_subject'] = "Received application for cost calculation №{id}";
$_['param_notify_message'] = "The visitor {name} left application №{id} for cost calculation.";

//Fields
$_['field_desc_order_payment_id'] = 'Application number';
$_['field_desc_order_payment_name'] = 'Customer\'s name';
$_['field_desc_order_payment_email'] = 'Customer\'s E-mail';
$_['field_desc_order_payment_phone'] = 'Customer\'s phone number';
$_['field_desc_order_payment_message'] = 'Customer Message';



$_['text_module_version'] = '';
