<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Filter</span>';
$_['heading_title_raw'] = 'NeoSeo Filter';

// Tab
$_['tab_general'] = 'General';
$_['tab_support'] = 'Support';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';
$_['tab_support'] = 'Support';

// Button
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check Again';
$_['button_clear_log'] = 'Clear log';
$_['button_download_log'] = 'Download log file';

// Text
$_['text_module_version'] = '';
$_['text_edit'] = 'Options';
$_['text_success'] = 'Module settings successfully updated!';
$_['text_success_clear'] = 'Logs successfully deleted';
$_['text_success_cache_clear'] = 'The cache has been successfully cleaned';
$_['text_default'] = 'By default';
$_['text_module'] = 'Modules';
$_['text_default_title'] = 'Filter';
$_['text_default_manufacturer_title'] = 'Manufacturer';
$_['text_template'] = 'Template';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debug mode:';
$_['entry_name'] = 'Name';
$_['entry_manufacturer'] = 'Filter manufacturer';
$_['entry_manufacturer_title'] = 'Manufacturer Title';
$_['entry_title'] = 'Title';
$_['entry_template'] = 'Template';
$_['entry_use_price'] = 'Filter price';

// Error
$_['error_download_logs'] = 'The log file is empty or missing!';
$_['error_permission'] = 'You do not have permission to manage this module!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';
$_['text_module_version'] = '';
