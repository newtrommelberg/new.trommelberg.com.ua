<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Category Tree</p>';
$_['heading_title_raw'] = 'NeoSeo Category Tree';

$_['tab_general'] = 'Options';
$_['tab_header'] = 'Cap';
$_['tab_menus'] = 'Menu';
$_['tab_header_left'] = 'Cap on the left';
$_['tab_header_right'] = 'Cap to the right';
$_['tab_product'] = 'Goods';
$_['tab_logs'] = 'Logs';
$_['tab_fields'] = 'Fields';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';
$_['tab_summ'] = 'Suma in cuirsive';
$_['tab_category_category'] = 'Categories';
$_['tab_category_information'] = 'Information';
$_['tab_category_manufacturer'] = 'Brands';
$_['tab_category_blog'] = 'Blog';
$_['tab_category_landing'] = 'Landing pages';
$_['tab_category_system'] = 'System Pages';
$_['tab_category_custom'] = 'Other';
// Text
$_['text_module_version']= '';
$_['text_payment'] = 'Payment';
$_['text_success'] = 'Module settings updated!';
$_['text_module'] = 'Modules';
$_['text_order_date_created'] = 'Date of creation';
$_['text_order_date_modified'] = 'Date of change';
$_['text_order_date_current'] = 'The current date';
$_['text_separate_column'] = 'In a separate column';
$_['text_product_column'] = 'In the product column';
$_['text_product_name'] = 'In the product name';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear Log';

$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Log';
$_['button_download_log'] = 'Download log';

// Entry
$_['entry_title'] = 'Menu name';
$_['entry_label'] = 'Name: ';
$_['entry_url'] = 'Link: ';
$_['entry_params'] = 'Parameter: ';
$_['entry_class'] = 'Additional class: ';
$_['entry_style'] = 'Additional style: ';
$_['entry_icon'] = 'Additional icon: ';
$_['entry_max_width'] = 'Maximum width: ';
$_['entry_bg_color'] = 'Background color:';
$_['entry_hover_bg_color'] = 'Hover background color:';
$_['entry_font_color'] = 'Font Color:';
$_['entry_hover_font_color'] = 'Font color for hover';
$_['entry_unit_option_null'] = 'Units:';
$_['entry_unit_option_null_desc'] = 'Only 0 (zero)';
$_['entry_unit_option_one_nine'] = 'Units:';
$_['entry_unit_option_one_nine_desc'] = 'Only 1-9';
$_['entry_unit_option_ten_nineteen'] = 'Units:';
$_['entry_unit_option_ten_nineteen_desc'] = 'Only 10-19';
$_['entry_tens_option'] = 'Decade:';
$_['entry_tens_option_desc'] = 'Only 20,30,40,50,60,70,80,90';
$_['entry_hundreds_option'] = 'Hundreds:';
$_['entry_hundreds_option_desc'] = 'Only 100,200,300,400,500,600,700,800,900';
$_['entry_money_option_coins'] = 'Monetary units:';
$_['entry_money_option_coins_desc'] = 'Only the name of coins';
$_['entry_money_option_currency'] = 'Monetary units:';
$_['entry_money_option_currency_desc'] = 'Currency name only';
$_['entry_unit_option_one_nine_thousand'] = 'Units of Thousand:';
$_['entry_unit_option_one_nine_thousand_desc'] = 'Only 1-9';
$_['entry_count_money_option_thousand'] = 'Number of currency units:';
$_['entry_count_money_option_thousand_desc'] = 'Only a thousand';
$_['entry_count_money_option_millon'] = 'Number of currency units:';
$_['entry_unit_option_one_nine_millon'] = 'Units of a million:';
$_['entry_unit_option_one_nine_millon_desc'] = 'Only 1-9';
$_['entry_count_money_option_millon_desc'] = 'Only one million';
$_['entry_count_money_option_billion'] = 'Number of currency units:';
$_['entry_unit_option_one_nine_billon'] = 'Units of billion:';
$_['entry_unit_option_one_nine_billon_desc'] = 'Only 1-9';
$_['entry_count_money_option_billion_desc'] = 'Only billion';
$_['entry_print_img'] = 'Printing:';
$_['entry_print_img_desc'] = 'Recommended image sizes: 100x100';
$_['entry_print_img_store'] = 'Shop logo:';
$_['entry_print_logo_img_desc'] = 'If not filled, the name of the store will be displayed';
$_['entry_print_img_width'] = 'Printing width:';
$_['entry_print_img_width_desc'] = '';
$_['entry_print_img_height'] = 'Print height:';
$_['entry_print_img_height_desc'] = '';
$_['entry_status_sale'] = 'Status:';
$_['entry_menu_sort_order'] = 'Sorting order:';
$_['entry_cat_name'] = 'Name:';
$_['entry_menu_status'] = 'Status:';
$_['entry_menu_action'] = 'Action:';

$_['entry_supplier_info'] = 'Provider:';
$_['entry_text'] = 'Additional text:';
$_['entry_debug'] = 'Debugging mode: <br/> <span class = "help"> The module logs will write various information for the module developer. </span>';
$_['entry_status'] = 'Status:';
$_['entry_currency_unit'] = 'Currency:';
$_['entry_replace_status'] = 'Remove the old button:';
$_['entry_customer_info_format'] = 'Customer information:';
$_['entry_customer_info_format_desc'] = 'Tags are supported: {country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}';
$_['entry_payment_info_format'] = 'Payment Information:';
$_['entry_payment_info_format_desc'] = 'Tags are supported: {method}, {country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}';
$_['entry_shipping_info_format'] = 'Information about delivery:';
$_['entry_shipping_info_format_desc'] = 'Tags are supported: {method}, {country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}';
$_['entry_store_name'] = 'Name of shop:';
$_['entry_store_name_desc'] = 'If not filled, it will be taken from the store settings';
$_['entry_store_url'] = 'Store URL:';
$_['entry_store_url_desc'] = 'If it is not filled, it will not be displayed';
$_['entry_store_phone'] = 'Store Phone:';
$_['entry_store_phone_desc'] = 'If not filled, it will be taken from the store settings';
$_['entry_store_email'] = 'Store Mail:';
$_['entry_store_email_desc'] = 'If not filled, it will be taken from the store settings';
$_['entry_store_logo'] = 'Store mail:';
$_['entry_store_logo_desc'] = 'If not selected, it will be taken from the store settings';
$_['entry_store_logo_width'] = 'Width';
$_['entry_store_logo_height'] = 'Height';
$_['entry_sort_order'] = 'Sorting order:';
$_['entry_order_date'] = 'Order date:';
$_['entry_order_date_desc'] = 'The date that will be displayed in the check';
$_['entry_column_sku_status'] = 'Show article:';
$_['entry_column_sku_status_desc'] = 'Show or not article in the list of goods';
$_['entry_column_image_status'] = 'Show image:';
$_['entry_column_image_status_desc'] = 'Show or not picture of goods';
$_['entry_column_image_width'] = 'Image width:';
$_['entry_column_image_height'] = 'Image height:';
$_['entry_column_model_status'] = 'Show model:';
$_['entry_column_model_status_desc'] = 'Show or not model in the list of goods';
$_['entry_column_option_status'] = 'Show Options';
$_['entry_column_option_status_desc'] = 'Show or not options in the list of goods';
$_['entry_field_list_name'] = 'Template';
$_['entry_field_list_desc'] = 'Description';
$_['entry_show_comment'] = 'Show note to the order';
$_['entry_sort_product'] = 'Sort products';
$_['entry_column_unit_status'] = 'Show units of measure';
$_['entry_column_quantity_field'] = 'The quantity field';
$_['entry_column_quantity_field_desc'] = 'The default is the quantity, but it can also be wtboxtotal if the corresponding module is installed';
$_['text_name_asc'] = 'By name (A - Z)';
$_['text_name_desc'] = 'By name (Z - A)';
$_['text_model_asc'] = 'By article (A - Z)';
$_['text_model_desc'] = 'By article (Z - A)';


$_['field_desc_order_id'] = 'Order number';
$_['field_desc_invoice_no'] = 'Account number';
$_['field_desc_date_added'] = 'Order creation date';
$_['field_desc_date_modified'] = 'Order change date';
$_['field_desc_date_current'] = 'The current date';
$_['field_desc_store_name'] = 'Name of shop';
$_['field_desc_store_url'] = 'Link to the store';
$_['field_desc_store_address'] = 'Store Address';
$_['field_desc_store_email'] = 'Store Email';
$_['field_desc_store_phone'] = 'Store phone number';
$_['field_desc_store_fax'] = 'Store Fax';
$_['field_desc_store_owner'] = 'Shopkeeper';
$_['field_desc_text'] = 'Additional text from module settings';
$_['field_desc_email'] = 'Buyer Email';
$_['field_desc_customer_info'] = 'Information about the buyer according to the settings of the module';
$_['field_desc_firstname'] = 'Buyer\'s name';
$_['field_desc_lastname'] = 'Buyer\'s surname';
$_['field_desc_telephone'] = 'Buyer phone number';
$_['field_shipping_firstname'] = 'Receiver name';
$_['field_shipping_lastname'] = 'Receiver surname';
$_['field_desc_shipping_company'] = 'Company of the recipient';
$_['field_desc_shipping_address_1'] = 'Address of 1 recipient';
$_['field_desc_shipping_address_2'] = 'Address of 2 recipient';
$_['field_desc_shipping_city'] = 'City of the recipient';
$_['field_desc_shipping_postcode'] = 'Postal code of the recipient';
$_['field_desc_shipping_zone'] = 'Recipient Region';
$_['field_desc_shipping_zone_code'] = 'Region code of the recipient';
$_['field_desc_shipping_country'] = 'Recipien\'s country';
$_['field_desc_shipping_info'] = 'Delivery information according to module settings';
$_['field_desc_shipping_method'] = 'Shipping method name';
$_['field_desc_payment_firstname'] = 'Payor name';
$_['field_desc_payment_lastname'] = 'Payor surname';
$_['field_desc_payment_company'] = 'Company payer';
$_['field_desc_payment_address_1'] = 'Address of 1 payer';
$_['field_desc_payment_address_2'] = 'Address of 2 payer';
$_['field_desc_payment_city'] = 'City of the payer';
$_['field_desc_payment_postcode'] = 'Postal code of the payer';
$_['field_desc_payment_zone'] = 'Region of the payer';
$_['field_desc_payment_zone_code'] = 'Region code of the payer';
$_['field_desc_payment_country'] = 'Country of payer';
$_['field_desc_payment_info'] = 'Delivery information according to module settings';
$_['field_desc_payment_method'] = 'Shipping method name';
$_['field_desc_product'] = 'List of products ( name, model, option, quantity, price, total)';
$_['field_desc_voucher'] = 'List of vouchers ( description, amount )';
$_['field_desc_total'] = 'Order summary ( code, title, text, value )';
$_['field_desc_total_str'] = 'Price in words';
$_['field_desc_comment'] = 'Order note';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_supplier_info'] = 'This field is required!';
$_['error_ioncube_missing'] = '<h3 style = "color: red"> IonCube Loader is missing! </h3> <p> To use our module, you need to install the IonCube Loader. Here are the instructions for installing the IonCube Loader for different cases: </p> <ul> <li> If you have shared hosting - <a href = "http://neoseo.com.ua/articles/ioncube-loader-shared "> http://neoseo.com.ua/articles/ioncube-loader-shared </a> </li> <li> If you have VPS on ubuntu - <a href =" http://neoseo.com. ua / articles / ioncube-loader-ubuntu "> http://neoseo.com.ua/articles/ioncube-loader-ubuntu </a> </li> <li> If you have a VPS on centos - <a href = "http://neoseo.com.ua/articles/ioncube-loader-centros">http://neoseo.com.ua/articles/ioncube-loader-centos </a> </li> </ul> < p> If you do not get to install IonCube Loader yourself, you can also ask for help from our specialists at <a href="mailto:alex.sorokin@neoseo.com.ua"> alex.sorokin@neoseo.com.ua < / a>, indicating exactly where you bought fashion l, your nickname on this resource and the order number. </p>';
$_['error_license_missing'] = '<h3 style = "color: red"> There is no license file! </h3> <p> To obtain the license file, contact the module developer at <a href="mailto:alex.sorokin@neoseo.com.ua"> alex.sorokin @ neoseo.com.ua </a>, specifying exactly where you purchased the module, your nickname on this resource and the order number. </p> <p> Put the license file in the root of the site, i.e. next to the robots.txt file and click "Check again." </p> <p> You can not worry that someone will steal your license file! Your license file is made for you personally and will not work on another domain </p>';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_ioncube_missing'] = '<h3 style = "color: red"> IonCube Loader is missing! </h3> <p> To use our module, you need to install the IonCube Loader. Here are the instructions for installing the IonCube Loader for different cases: </p> <ul> <li> If you have shared hosting - <a href = "http://neoseo.com.ua/articles/ioncube-loader-shared "> http://neoseo.com.ua/articles/ioncube-loader-shared </a> </li> <li> If you have VPS on ubuntu - <a href =" http://neoseo.com. ua / articles / ioncube-loader-ubuntu "> http://neoseo.com.ua/articles/ioncube-loader-ubuntu </a> </li> <li> If you have a VPS on centos - <a href = "http://neoseo.com.ua/articles/ioncube-loader-centros">http://neoseo.com.ua/articles/ioncube-loader-centos </a> </li> </ul> < p> If you do not get to install IonCube Loader yourself, you can also ask for help from our specialists at <a href="mailto:alex.sorokin@neoseo.com.ua"> alex.sorokin@neoseo.com.ua < / a>, indicating exactly where you bought fashion l, your nickname on this resource and the order number. </p>';
$_['error_license_missing'] = '<h3 style = "color: red"> There is no license file! </h3> <p> To obtain the license file, contact the module developer at <a href="mailto:alex.sorokin@neoseo.com.ua"> alex.sorokin @ neoseo.com.ua </a>, specifying exactly where you purchased the module, your nickname on this resource and the order number. </p> <p> Put the license file in the root of the site, i.e. next to the robots.txt file and click "Check again." </p> <p> You can not worry that someone will steal your license file! Your license file is made for you personally and will not work on another domain </p>';


$_['mail_support'] = '
<h3 style="color:red">Thank you for choosing our product!</h3>
    <p><a href="http://neoseo.com.ua">Web Studio NeoSeo </a> makes every effort to ensure that its products are installed as quickly and easily as possible, without creating conflicts with other modules and themes, and delivering customers only the joy of using products. We will be glad if you <a href="http://seomag.com.ua/moduli/moduli-obrabotki-zakazov/soforp-cash-memo" target="_blank"> buy NeoSeo module Yandex Metrica </a> more once or any other module in the store seomag.com.ua. <b> Cumulative discounts apply! </b></p>
    <p>However, this is not always possible, considering that opencart has very weak technical capabilities for this, so please take these nuances with understanding.</p>
    <p><b> What we guarantee </b>, and we provide for free:</p>
    <ul>
        <li>the work of our modules on the standard issue of opencart</li>
        <li>the work of our modules on the standard admin opencart</li>
    </ul>
    <p>If you have a problem with the module in this context, you can always request free technical support at <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a>.</p>
    <p><b>What we try to provide, but do not guarantee</b>:</p>
    <ul>
        <li>the work of our modules on a NON-standard theme opencart</li>
        <li>the work of our modules on the NON-standard admin opencart</li>
    </ul>
    <p>As already mentioned, it is impossible to envisage in advance all the nuances of foreign themes, so in case of problems in this vein, we provide paid support for symbolic value. You can request it, as well as comprehensive maintenance of your store, at <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></p>
    <p><b> ATTENTION !!! </b> If you are having trouble installing the modules, then you do not need to spend your precious time on this routine process. Let our technical experts do this instead of you for a nominal fee, and you can spend the saved time on the development of your business, family and hobbies. You can order installation and maintenance at <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></p>';

$_['module_licence'] = '
    <h3 style="color:red">Thank you for choosing our product!</h3>
    <p>All rights to the software product, then the module, belong to <a href="http://neoseo.com.ua"> NeoSeo Web Studio </a>. You can <a href="http://seomag.com.ua/moduli/moduli-obrabotki-zakazov/soforp-cash-memo" target="_blank"> buy NeoSeo Yandex Metrica </a> once again in the store seomag.com.ua. <b> Cumulative discounts apply! </b></p>
    <p><b>The license for this module entitles you to:</b>
        <ul>
            <li>activation on <b> ONE domain </b>. Not on the site, not at the person, not at the studio. At you some domains are connected to one site - means you need several licenses.</li>
            <li>to use at your store or customer store.</li>
            <li>free updates to store owners within a year of purchase, regardless of who was the installer of the module</li>
        </ul>
    </p>
    <p><b>Strictly forbidden:</b>
        <ul>
            <li>Publish the module on other sites without notifying the author</li>
            <li>Transfer the module to third parties</li>
            <li>Sell on its own behalf without prior agreement with the author</li>
            <li>Use unlicensed versions of modules (warez). In case of violation, all purchases on the domain without refunds will be canceled.</li>
        </ul>
    </p>
    <p><b>Denial of responsibility:</b>
        <ul>
            <li>The author of the module does not bear any responsibility for material and non-material damage caused by the module. You use the module at your own risk.</li>
            <li>In order to significantly minimize the risks, you can <a href="http://seomag.com.ua/moduli/moduli-prochie/soforp-backup" target="_blank"> buy the NeoSeo backup module </a>, which is reliable protect your store from data loss, or order a comprehensive service for your store from the author <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></li>
            <li>The author reserves the right at any time to change the terms of the license agreement, without agreement with the end users of his products.</li>
        </ul>
    </p>';

$_['params_unit_option_null'] = 'zero';
$_['params_unit_option_one_nine'] = 'one, two, three, four, five, six, seven, eight, nine';
$_['params_unit_option_ten_nineteen'] = 'ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen, seventeen, eighteen, nineteen';
$_['params_tens_option'] = 'twenty, thirty, forty, fifty, sixty, seventy, eighty, ninety';
$_['params_hundreds_option'] = 'one hundred, two hundred, three hundred, four hundred, five hundred, six hundred, seven hundred, eight hundred, nine hundred';
$_['params_money_option_coins'] = 'cent, cent, cent';
$_['params_money_option_currency'] = 'euro, euro, euro';
$_['params_unit_option_one_nine_thousand'] = 'one, two, three, four, five, six, seven, eight, nine';
$_['params_count_money_option_thousand'] = 'thousand, thousand, thousand';
$_['params_unit_option_one_nine_millon'] = 'one, two, three, four, five, six, seven, eight, nine';
$_['params_count_money_option_millon'] = 'million, million, millions';
$_['params_unit_option_one_nine_billon'] = 'one, two, three, four, five, six, seven, eight, nine';
$_['params_count_money_option_billon'] = 'billion, billion, billion';