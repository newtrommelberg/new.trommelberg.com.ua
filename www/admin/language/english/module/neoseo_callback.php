<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Callback</p>';
$_['heading_title_raw'] = 'NeoSeo Callback';

$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_fields'] = 'Fields';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';


// Text
$_['text_success'] = 'Module settings updated!';
$_['text_module'] = 'Modules';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear Log';

$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Log';
$_['button_download_log'] = 'Download log';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debug mode:';
$_['entry_debug_desc'] = 'The module logs will write various information for the module developer';
$_['entry_message'] = 'Notification of the accepted application';
$_['entry_notify'] = 'Recipients of message about the application, separated by commas';
$_['entry_notify_subject'] = 'The subject of the letter about the new application';
$_['entry_notify_message'] = 'Message about new application';
$_['entry_notify_message_desc'] = 'You can use {callback_id},{name},{email},{message},{time_from},{time_to}';
$_['entry_phone_mask'] = 'Input mask for the phone';
$_['entry_phone_mask_desc'] = 'Specify an input mask to minimize errors';
$_['entry_use_email'] = 'Display email on the order form';
$_['entry_title'] = 'Title form';

//Params
$_['param_message'] = "Your application # {callback_id} for the callback is recorded!\nOur manager will call you back within 15 minutes during business hours.";
$_['param_notify_subject'] = "Received callback request # {callback_id}";
$_['param_notify_message'] = "The visitor {name} left application # {callback_id} for a callback. \n\nHe(She) asks for a call back to {phone} on the {message}.";
$_['param_title'] = "Enter your details";

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_download_logs'] = 'The log file is empty or missing!';




$_['text_module_version'] = '';
