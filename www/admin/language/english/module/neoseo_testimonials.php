<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Customer Reviews';
$_['heading_title_raw'] = 'NeoSeo Customer Reviews';
$_['new_title'] = 'Customer Reviews';

//Tab
$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';
$_['tab_support'] = 'Support';

//Button
$_['button_clear_log'] = 'Clear Log';
$_['button_recheck'] = 'Check again';
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';

// Entry
$_['entry_status'] = 'Status';
$_['entry_debug'] = 'Debugging';
$_['entry_name'] = 'Name';
$_['entry_youtube'] = 'YouTube Video Reviews';
$_['entry_title'] = 'Header';
$_['entry_limit'] = 'Amount';
$_['entry_description_limit'] = 'How many characters to show (empty - show all)';
$_['entry_sort_order'] = 'Order:';
$_['entry_template'] = 'Template:';
$_['entry_user_image_status'] = 'Custom Image:';
$_['entry_user_image_width'] = 'Image Width:';
$_['entry_user_image_height'] = 'Image Height:';

// Text
$_['text_module'] = 'Modules';
$_['text_clear_log'] = 'Clear Log';
$_['text_module_version'] = '';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_success'] = 'Module settings updated!';
$_['text_default_title'] = 'Customer Reviews';
$_['text_template'] = 'Template';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_image'] = 'Image width &amp; height dimensions required!';



$_['mail_support'] = '
    <h3 style="color:red">Thank you for choosing our product!</h3>
    <p><a href="http://neoseo.com.ua">Web Studio NeoSeo </a> makes every effort to ensure that its products are installed as quickly and easily as possible, without creating conflicts with other modules and themes, and delivering customers only the joy of using products. We will be glad if you <a href="http://seomag.com.ua/moduli/moduli-obrabotki-zakazov/soforp-cash-memo" target="_blank"> buy NeoSeo module Yandex Metrica </a> more once or any other module in the store seomag.com.ua. <b> Cumulative discounts apply! </b></p>
    <p>However, this is not always possible, considering that opencart has very weak technical capabilities for this, so please take these nuances with understanding.</p>
    <p><b> What we guarantee </b>, and we provide for free:</p>
    <ul>
        <li>the work of our modules on the standard issue of opencart</li>
        <li>the work of our modules on the standard admin opencart</li>
    </ul>
    <p>If you have a problem with the module in this context, you can always request free technical support at <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a>.</p>
    <p><b>What we try to provide, but do not guarantee</b>:</p>
    <ul>
        <li>the work of our modules on a NON-standard theme opencart</li>
        <li>the work of our modules on the NON-standard admin opencart</li>
    </ul>
    <p>As already mentioned, it is impossible to envisage in advance all the nuances of foreign themes, so in case of problems in this vein, we provide paid support for symbolic value. You can request it, as well as comprehensive maintenance of your store, at <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></p>
    <p><b> ATTENTION !!! </b> If you are having trouble installing the modules, then you do not need to spend your precious time on this routine process. Let our technical experts do this instead of you for a nominal fee, and you can spend the saved time on the development of your business, family and hobbies. You can order installation and maintenance at <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></p>';

$_['module_licence'] = '
    <h3 style="color:red">Thank you for choosing our product!</h3>
    <p>All rights to the software product, then the module, belong to <a href="http://neoseo.com.ua"> NeoSeo Web Studio </a>. You can <a href="http://seomag.com.ua/moduli/moduli-obrabotki-zakazov/soforp-cash-memo" target="_blank"> buy NeoSeo Yandex Metrica </a> once again in the store seomag.com.ua. <b> Cumulative discounts apply! </b></p>
    <p><b>The license for this module entitles you to:</b>
        <ul>
            <li>activation on <b> ONE domain </b>. Not on the site, not at the person, not at the studio. At you some domains are connected to one site - means you need several licenses.</li>
            <li>to use at your store or customer store.</li>
            <li>free updates to store owners within a year of purchase, regardless of who was the installer of the module</li>
        </ul>
    </p>
    <p><b>Strictly forbidden:</b>
        <ul>
            <li>Publish the module on other sites without notifying the author</li>
            <li>Transfer the module to third parties</li>
            <li>Sell on its own behalf without prior agreement with the author</li>
            <li>Use unlicensed versions of modules (warez). In case of violation, all purchases on the domain without refunds will be canceled.</li>
        </ul>
    </p>
    <p><b>Denial of responsibility:</b>
        <ul>
            <li>The author of the module does not bear any responsibility for material and non-material damage caused by the module. You use the module at your own risk.</li>
            <li>In order to significantly minimize the risks, you can <a href="http://seomag.com.ua/moduli/moduli-prochie/soforp-backup" target="_blank"> buy the NeoSeo backup module </a>, which is reliable protect your store from data loss, or order a comprehensive service for your store from the author <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></li>
            <li>The author reserves the right at any time to change the terms of the license agreement, without agreement with the end users of his products.</li>
        </ul>
    </p>';