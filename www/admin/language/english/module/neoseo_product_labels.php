<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Product Tags</span>';
$_['heading_title_raw'] = 'NeoSeo Product Tags';

//Tab
$_['tab_general'] = 'General';
$_['tab_label'] = 'Labels';
$_['tab_special_label'] = 'Label for discounts';
$_['tab_support'] = 'Support';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';

//Button
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Logs';

// Text
$_['text_module_version'] = '';
$_['text_edit'] = 'Options';
$_['text_success'] = 'Module settings have been successfully updated!';
$_['text_success_clear'] = 'Logs successfully deleted';
$_['text_module'] = 'Modules';
$_['text_success_delete'] = 'Label successfully deleted!';
$_['text_new_label'] = 'New label';
$_['text_del_label'] = 'Remove Tag';
$_['text_special'] = 'Discount';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_store'] = 'Stores';
$_['entry_debug'] = 'Debug mode:';
$_['entry_name'] = 'Label name';
$_['entry_class'] = 'Label class';
$_['entry_style'] = 'Label style';
$_['entry_color'] = 'Colour';
$_['entry_type'] = 'List type';
$_['entry_priority'] = 'Order';
$_['entry_products'] = 'Goods';
$_['entry_label_type'] = 'Label type';
$_['entry_position'] = 'Label position';
$_['entry_special_status'] = 'Status:';
$_['entry_special_class'] = 'Label class';
$_['entry_special_style'] = 'Label style';
$_['entry_special_color'] = 'Colour';
$_['entry_special_priority'] = 'Order';
$_['entry_special_label_type'] = 'Label type';
$_['entry_special_position'] = 'Label position';
$_['entry_duration_days'] = 'Duration (Days):';
$_['entry_view_counts'] = 'Viewed for active:';
$_['entry_sold'] = 'Sold products count:';
$_['entry_limit'] = 'Limit';
$_['entry_special_title'] = 'Discount title';

//Params
$_['params_type_new'] = 'Novelties';
$_['params_type_popular'] = 'Popular';
$_['params_type_hit'] = 'Best-seller';
$_['params_type_hands'] = 'Specify manually';
$_['params_type_stock'] = 'Not in stock';
$_['params_type_instock'] = 'In stock';
$_['params_position_top_left'] = 'Top left';
$_['params_position_top_right'] = 'Top right';
$_['params_position_bottom_left'] = 'Bottom left';
$_['params_position_bottom_right'] = 'Bottom right';
$_['params_type_label_corner'] = 'Corner';
$_['params_type_label_flag'] = 'Checkbox';
$_['params_type_label_stripes'] = 'Ribbon';
$_['params_type_label_sticker'] = 'Stiker';

// Error
$_['error_permission'] = 'You do not have permission to edit "NeoSeo Backups"!';




