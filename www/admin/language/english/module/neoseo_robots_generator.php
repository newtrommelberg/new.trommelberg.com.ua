<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Robots Generator</p>';
$_['heading_title_raw'] = 'NeoSeo Robots Generator';

//Tab
$_['tab_general'] = 'General';
$_['tab_support'] = 'Support';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';
$_['tab_support'] = 'Support';

//Button
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save & Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Re-Check';
$_['button_clear_log'] = 'Clear log';

// Text
$_['text_module_version']='';
$_['text_edit'] = 'Edit setting';
$_['text_success'] = 'Settings are updated successfuly!';
$_['text_success_clear'] = 'Logs are cleared';
$_['text_default'] = 'By default';
$_['text_feed'] = 'Feeds';
$_['text_gzip_0'] = 'Disabled';
$_['text_gzip_1'] = 'Level 1';
$_['text_gzip_2'] = 'Level 2';
$_['text_gzip_3'] = 'Level 3';
$_['text_gzip_4'] = 'Level 4';
$_['text_gzip_5'] = 'Level 5';
$_['text_gzip_6'] = 'Level 6';
$_['text_gzip_7'] = 'Level 7';
$_['text_gzip_8'] = 'Level 8';
$_['text_gzip_9'] = 'Level 9';
$_['text_seo_0'] = 'Disabled';
$_['text_seo_1'] = 'SEO Pro';
$_['text_seo_2'] = 'SEO Url';
$_['text_blog_0'] = 'Disabled';
$_['text_blog_1'] = 'ocCMS';
$_['text_blog_2'] = 'PAV Blog';
$_['text_blog_3'] = 'BlogManager';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_status_desc'] = '';
$_['entry_debug'] = 'Debug mode:';
$_['entry_debug_desc'] = '';
$_['entry_image_status'] = 'Include images:';
$_['entry_image_status_desc'] = 'Not recommended. Not all robots understand it';
$_['entry_seo_status'] = 'Product SEO URL:';
$_['entry_seo_status_desc'] = '';
$_['entry_filterpro_seo_status'] = 'SEO FilterPro:';
$_['entry_filterpro_seo_status_desc'] = '';
$_['entry_category_brand_status'] = 'Categories with brands';
$_['entry_category_brand_status_desc'] = 'Generates additional urls like {category}/{manufacturer}';
$_['entry_status_addresses'] = 'Addresses:';
$_['entry_status_addresses_desc'] = '';
$_['entry_partition_status'] = 'Use partitions:';
$_['entry_partition_status_desc'] = 'Split sitemap with partitions:';
$_['entry_partition_volume'] = 'Partition size:';
$_['entry_partition_volume_desc'] = '50,000 is recommended';
$_['entry_multistore_status'] = 'Multistore:';
$_['entry_multistore_status_desc'] = '';
$_['entry_gzip_status'] = 'Compression:';
$_['entry_gzip_status_desc'] = '';
$_['entry_blog_status'] = 'Blog entries:';
$_['entry_blog_status_desc'] = '';
$_['entry_url'] = 'Url for sitemap.xml:';
$_['entry_url_desc'] = '';
$_['entry_use_url_date'] = 'Generate modification date:';
$_['entry_use_url_date_desc'] = '';
$_['entry_use_url_frequency'] = 'Generate frequency';
$_['entry_use_url_frequency_desc'] = '';
$_['entry_use_url_priority'] = 'Generate priority';
$_['entry_use_url_priority_desc'] = '';

// Error




