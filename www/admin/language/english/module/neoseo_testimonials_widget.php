<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Виджет отзывы о магазине на панели управления</span>';
$_['heading_title_raw'] = 'NeoSeo Виджет отзывы о магазине';

//Tabs
$_['tab_general'] = 'Параметры';
$_['tab_header'] = 'Шапка';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_clear'] = 'Очистить';
$_['text_title'] = 'Отзывы о магазине';
$_['text_status_disabled'] = 'Не опубликованные';
$_['text_no_admin_answer'] = 'Без ответа администратора';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';

// Entry
$_['entry_debug'] = 'Отладочный режим<br /><span class="help">В логи модуля будет писаться различная информация для разработчика модуля.</span>';
$_['entry_status'] = 'Статус';
$_['entry_show_testimonial_number'] = 'Выводить номер отзыва';
$_['entry_show_author'] = 'Выводить автора';
$_['entry_show_text'] = 'Выводить текст отзыва';
$_['entry_show_admin_text'] = 'Выводить ответ админа';
$_['entry_show_rating'] = 'Выводить рейтинг отзыва';
$_['entry_show_status'] = 'Выводить статус отзыва';
$_['entry_show_date_added'] = 'Выводить дату создания отзыва';
$_['entry_show_youtube'] = 'Выводить ссылку на видео отзыв YouTube';
$_['entry_show_image'] = 'Выводить изображение из отзыва';
$_['entry_limit'] = 'Количество выводимых записей';
$_['entry_title'] = 'Заголовок блока';
$_['entry_method'] = 'Способ отбора отзывов';
$_['entry_height_image'] = 'Высота изображения';
$_['entry_width_image'] = 'Ширина изображения';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';



