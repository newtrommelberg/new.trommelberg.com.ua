<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Blog Search';
$_['heading_title_raw'] = 'NeoSeo Blog Search';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Blog Search module!';
$_['text_edit']        = 'Edit Blog Search';
$_['text_module_version'] = '';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_title']      = 'Module Title';
$_['entry_template']   = 'Template';
$_['entry_root_category'] = 'Restricted by category:';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Blog Search module!';