<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Tree Categories - Block</p>';
$_['heading_title_raw'] = 'NeoSeo Tree Categories - Block';

$_['tab_general'] = 'Options';
$_['tab_header'] = 'Cap';
$_['tab_menus'] = 'Menu';
$_['tab_header_left'] = 'Cap on the left';
$_['tab_header_right'] = 'Cap to the right';
$_['tab_product'] = 'Goods';
$_['tab_logs'] = 'Logs';
$_['tab_fields'] = 'Fields';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';
$_['tab_summ'] = 'Suma in cuirsive';
$_['tab_menu_category'] = 'Categories';
$_['tab_menu_information'] = 'Information';
$_['tab_menu_manufacturer'] = 'Brands';
$_['tab_menu_blog'] = 'Blog';
$_['tab_menu_landing'] = 'Landing pages';
$_['tab_menu_system'] = 'System Pages';
$_['tab_menu_custom'] = 'Other';
$_['text_template'] = 'Template';

// Text
$_['text_module_version']= '';
$_['text_default_title']= 'Блок категорий';
$_['text_default_description']= 'Описание блока категорий';
$_['text_payment'] = 'Payment';
$_['text_success'] = 'Module settings updated!';
$_['text_module'] = 'Modules';
$_['text_order_date_created'] = 'Date of creation';
$_['text_order_date_modified'] = 'Date of change';
$_['text_order_date_current'] = 'The current date';
$_['text_separate_column'] = 'In a separate column';
$_['text_product_column'] = 'In the product column';
$_['text_product_name'] = 'In the product name';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear Log';
$_['text_manual'] = 'Manual';
$_['text_auto'] = 'Automatic';
$_['text_auto_child'] = 'Auto-selection of subcategories';
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Log';
$_['button_download_log'] = 'Download log';

// Entry
$_['entry_name'] = 'Module name';
$_['entry_description'] = 'Описание';
$_['entry_categories_id'] = 'Set of categories';
$_['entry_template'] = 'Select a display template';
$_['entry_icon_width'] = 'Image Width';
$_['entry_icon_height'] = 'Image Height';
$_['entry_title'] = 'Menu name';
$_['entry_label'] = 'Name: ';
$_['entry_url'] = 'Link: ';
$_['entry_params'] = 'Parameter: ';
$_['entry_class'] = 'Additional class: ';
$_['entry_style'] = 'Additional style: ';
$_['entry_icon'] = 'Additional icon: ';
$_['entry_max_width'] = 'Maximum width: ';
$_['entry_bg_color'] = 'Background color:';
$_['entry_hover_bg_color'] = 'Hover background color:';
$_['entry_font_color'] = 'Font Color:';
$_['entry_hover_font_color'] = 'Font color for hover';
$_['entry_unit_option_null'] = 'Units:';
$_['entry_unit_option_null_desc'] = 'Only 0 (zero)';
$_['entry_unit_option_one_nine'] = 'Units:';
$_['entry_unit_option_one_nine_desc'] = 'Only 1-9';
$_['entry_unit_option_ten_nineteen'] = 'Units:';
$_['entry_unit_option_ten_nineteen_desc'] = 'Only 10-19';
$_['entry_tens_option'] = 'Decade:';
$_['entry_tens_option_desc'] = 'Only 20,30,40,50,60,70,80,90';
$_['entry_hundreds_option'] = 'Hundreds:';
$_['entry_hundreds_option_desc'] = 'Only 100,200,300,400,500,600,700,800,900';
$_['entry_money_option_coins'] = 'Monetary units:';
$_['entry_money_option_coins_desc'] = 'Only the name of coins';
$_['entry_money_option_currency'] = 'Monetary units:';
$_['entry_money_option_currency_desc'] = 'Currency name only';
$_['entry_unit_option_one_nine_thousand'] = 'Units of Thousand:';
$_['entry_unit_option_one_nine_thousand_desc'] = 'Only 1-9';
$_['entry_count_money_option_thousand'] = 'Number of currency units:';
$_['entry_count_money_option_thousand_desc'] = 'Only a thousand';
$_['entry_count_money_option_millon'] = 'Number of currency units:';
$_['entry_unit_option_one_nine_millon'] = 'Units of a million:';
$_['entry_unit_option_one_nine_millon_desc'] = 'Only 1-9';
$_['entry_count_money_option_millon_desc'] = 'Only one million';
$_['entry_count_money_option_billion'] = 'Number of currency units:';
$_['entry_unit_option_one_nine_billon'] = 'Units of billion:';
$_['entry_unit_option_one_nine_billon_desc'] = 'Only 1-9';
$_['entry_count_money_option_billion_desc'] = 'Only billion';
$_['entry_print_img'] = 'Printing:';
$_['entry_print_img_desc'] = 'Recommended image sizes: 100x100';
$_['entry_print_img_store'] = 'Shop logo:';
$_['entry_print_logo_img_desc'] = 'If not filled, the name of the store will be displayed';
$_['entry_print_img_width'] = 'Printing width:';
$_['entry_print_img_width_desc'] = '';
$_['entry_print_img_height'] = 'Print height:';
$_['entry_print_img_height_desc'] = '';
$_['entry_status_sale'] = 'Status:';
$_['entry_menu_sort_order'] = 'Sorting order:';
$_['entry_cat_name'] = 'Name:';
$_['entry_menu_status'] = 'Status:';
$_['entry_menu_action'] = 'Action:';

$_['entry_supplier_info'] = 'Поставщик:';
$_['entry_text'] = 'Дополнительный текст:';
$_['entry_debug'] = 'Отладочный режим:<br /><span class="help">В логи модуля будет писаться различная информация для разработчика модуля.</span>';
$_['entry_status'] = 'Статус:';
$_['entry_type'] = 'Тип построения меню:';
$_['entry_currency_unit'] = 'Валюта:';
$_['entry_replace_status'] = 'Убрать старую кнопку:';
$_['entry_customer_info_format'] = 'Информация о клиенте:';
$_['entry_customer_info_format_desc'] = 'Поддерживаются теги: {country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}';
$_['entry_payment_info_format'] = 'Информация об оплате:';
$_['entry_payment_info_format_desc'] = 'Поддерживаются теги: {method}, {country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}';
$_['entry_shipping_info_format'] = 'Информация о доставке:';
$_['entry_shipping_info_format_desc'] = 'Поддерживаются теги: {method}, {country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}';
$_['entry_store_name'] = 'Название магазина:';
$_['entry_store_name_desc'] = 'Если не заполнено, будет взято из настроек магазина';
$_['entry_store_url'] = 'URL магазина:';
$_['entry_store_url_desc'] = 'Если не заполнено, то выводиться не будет';
$_['entry_store_phone'] = 'Телефон магазина:';
$_['entry_store_phone_desc'] = 'Если не заполнен, будет взят из настроек магазина';
$_['entry_store_email'] = 'Почта магазина:';
$_['entry_store_email_desc'] = 'Если не заполнена, будет взята из настроек магазина';
$_['entry_store_logo'] = 'Логотип магазина:';
$_['entry_store_logo_desc'] = 'Если не выбран, будет взят из настроек магазина';
$_['entry_store_logo_width'] = 'Ширина';
$_['entry_store_logo_height'] = 'Высота';
$_['entry_sort_order'] = 'Порядок сортировки:';
$_['entry_order_date'] = 'Дата заказа:';
$_['entry_order_date_desc'] = 'Дата, которая будет выводиться в чеке';
$_['entry_column_sku_status'] = 'Показывать артикул:';
$_['entry_column_sku_status_desc'] = 'Показывать или нет артикулу в списке товаров';
$_['entry_column_image_status'] = 'Показывать картинку:';
$_['entry_column_image_status_desc'] = 'Показывать или нет картинку товара';
$_['entry_column_image_width'] = 'Ширина картинки:';
$_['entry_column_image_height'] = 'Высота картинки:';
$_['entry_column_model_status'] = 'Показывать модель:';
$_['entry_column_model_status_desc'] = 'Показывать или нет модель в списке товаров';
$_['entry_column_option_status'] = 'Показывать опции';
$_['entry_column_option_status_desc'] = 'Показывать или нет опции в списке товаров';
$_['entry_field_list_name'] = 'Шаблон';
$_['entry_field_list_desc'] = 'Описание';
$_['entry_show_comment'] = 'Показывать примечание к заказу';
$_['entry_sort_product'] = 'Сортировать товары';
$_['entry_column_unit_status'] = 'Показывать единицы измерения';
$_['entry_column_quantity_field'] = 'Поле для количества';
$_['entry_column_quantity_field_desc'] = 'По умолчанию quantity, но может быть и wtboxtotal если установлен соответствующий модуль';
$_['text_name_asc'] = 'По наименованию (А - Я)';
$_['text_name_desc'] = 'По наименованию (Я - А)';
$_['text_model_asc'] = 'По артикулу (А - Я)';
$_['text_model_desc'] = 'По артикулу (Я - А)';

$_['field_desc_order_id'] = 'Номер заказа';
$_['field_desc_invoice_no'] = 'Номер счета';
$_['field_desc_date_added'] = 'Дата создания заказа';
$_['field_desc_date_modified'] = 'Дата изменения заказа';
$_['field_desc_date_current'] = 'Текущая дата';
$_['field_desc_store_name'] = 'Название магазина';
$_['field_desc_store_url'] = 'Ссылка на магазин';
$_['field_desc_store_address'] = 'Адрес магазина';
$_['field_desc_store_email'] = 'Email магазина';
$_['field_desc_store_phone'] = 'Телефон магазина';
$_['field_desc_store_fax'] = 'Факс магазина';
$_['field_desc_store_owner'] = 'Владелец магазина';
$_['field_desc_text'] = 'Дополнительный текст из настроек модуля';
$_['field_desc_email'] = 'Email покупателя';
$_['field_desc_customer_info'] = 'Информация о покупателя согласно настройкам модуля';
$_['field_desc_firstname'] = 'Имя покупателя';
$_['field_desc_lastname'] = 'Фамилия покупателя';
$_['field_desc_telephone'] = 'Телефон покупателя';
$_['field_shipping_firstname'] = 'Имя получателя ';
$_['field_shipping_lastname'] = 'Фамилия получателя';
$_['field_desc_shipping_company'] = 'Компания получателя';
$_['field_desc_shipping_address_1'] = 'Адрес 1 получателя';
$_['field_desc_shipping_address_2'] = 'Адрес 2 получателя';
$_['field_desc_shipping_city'] = 'Город получателя';
$_['field_desc_shipping_postcode'] = 'Почтовый код получателя';
$_['field_desc_shipping_zone'] = 'Регион получателя';
$_['field_desc_shipping_zone_code'] = 'Код региона получателя';
$_['field_desc_shipping_country'] = 'Страна получателя';
$_['field_desc_shipping_info'] = 'Информация о доставке согласно настройкам модуля';
$_['field_desc_shipping_method'] = 'Название метода доставки';
$_['field_desc_payment_firstname'] = 'Имя плательщика';
$_['field_desc_payment_lastname'] = 'Фамилия плательщика';
$_['field_desc_payment_company'] = 'Компания плательщика';
$_['field_desc_payment_address_1'] = 'Адрес 1 плательщика';
$_['field_desc_payment_address_2'] = 'Адрес 2 плательщика';
$_['field_desc_payment_city'] = 'Город плательщика';
$_['field_desc_payment_postcode'] = 'Почтовый код плательщика';
$_['field_desc_payment_zone'] = 'Регион плательщика';
$_['field_desc_payment_zone_code'] = 'Код региона плательщика';
$_['field_desc_payment_country'] = 'Страна плательщика';
$_['field_desc_payment_info'] = 'Информация об оплате согласно настройкам модуля';
$_['field_desc_payment_method'] = 'Название метода оплаты';
$_['field_desc_product'] = 'Список товаров ( name, model, option, quantity, price, total)';
$_['field_desc_voucher'] = 'Список ваучеров ( description, amount )';
$_['field_desc_total'] = 'Итоги заказа ( code, title, text, value )';
$_['field_desc_total_str'] = 'Сумма прописью';
$_['field_desc_type'] = 'Если выбранно вручную, то выбирается меню категории из которой строить стену';
$_['field_desc_comment'] = 'Примечание к заказу';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_supplier_info'] = 'Это поле обязательно для заполнения!';
$_['error_ioncube_missing'] = '<h3 style="color:red">Отсутствует IonCube Loader!</h3><p>Чтобы пользоваться нашим модулем, вам нужно установить IonCube Loader. Ниже приводятся инструкции по установке IonCube Loader для разных случаев:</p><ul><li>Если у вас shared-хостинг - <a href="http://neoseo.com.ua/faq/ioncube-loader-shared">http://neoseo.com.ua/faq/ioncube-loader-shared</a></li><li>Если у вас VPS на ubuntu - <a href="http://neoseo.com.ua/faq/ioncube-loader-ubuntu">http://neoseo.com.ua/faq/ioncube-loader-ubuntu</a></li><li>Если у вас VPS на centos - <a href="http://neoseo.com.ua/faq/ioncube-loader-centos">http://neoseo.com.ua/faq/ioncube-loader-centos</a></li></ul><p>Если у вас не выходит установить IonCube Loader самостоятельно, вы также можете попросить помощи у наших специалистов по адресу <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a>, указав где именно вы приобрели модуль, ваш ник на этом ресурсе и номер заказа.</p>';
$_['error_license_missing'] = '<h3 style="color:red">Отсутствует файл лицензии!</h3><p>Для получения файла лицензии свяжитесь с разработчиком модуля по адресу <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a>, указав где именно вы приобрели модуль, ваш ник на этом ресурсе и номер заказа.</p><p>Полученный файл лицензии положите в корень сайта, т.е. рядом с файлом robots.txt и нажмите кнопку "Проверить еще раз".</p><p>Вы можете не переживать что ваш файл лицензии кто-то украдет! Ваш файл лицензии сделан персонально для вас и не будет работать на другом домене</p>';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_ioncube_missing'] = '<h3 style="color:red">Отсутствует IonCube Loader!</h3><p>Чтобы пользоваться нашим модулем, вам нужно установить IonCube Loader. Ниже приводятся инструкции по установке IonCube Loader для разных случаев:</p><ul><li>Если у вас shared-хостинг - <a href="http://neoseo.com.ua/articles/ioncube-loader-shared">http://neoseo.com.ua/articles/ioncube-loader-shared</a></li><li>Если у вас VPS на ubuntu - <a href="http://neoseo.com.ua/articles/ioncube-loader-ubuntu">http://neoseo.com.ua/articles/ioncube-loader-ubuntu</a></li><li>Если у вас VPS на centos - <a href="http://neoseo.com.ua/articles/ioncube-loader-centos">http://neoseo.com.ua/articles/ioncube-loader-centos</a></li></ul><p>Если у вас не выходит установить IonCube Loader самостоятельно, вы также можете попросить помощи у наших специалистов по адресу <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a>, указав где именно вы приобрели модуль, ваш ник на этом ресурсе и номер заказа.</p>';
$_['error_license_missing'] = '<h3 style="color:red">Отсутствует файл лицензии!</h3><p>Для получения файла лицензии свяжитесь с разработчиком модуля по адресу <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a>, указав где именно вы приобрели модуль, ваш ник на этом ресурсе и номер заказа.</p><p>Полученный файл лицензии положите в корень сайта, т.е. рядом с файлом robots.txt и нажмите кнопку "Проверить еще раз".</p><p>Вы можете не переживать что ваш файл лицензии кто-то украдет! Ваш файл лицензии сделан персонально для вас и не будет работать на другом домене</p>';


$_['mail_support'] = '
    <h3 style="color:red">Благодарим вас за выбор нашего продукта!</h3>
    <p><a href="http://neoseo.com.ua">Веб студия NeoSeo</a> прикладывает максимум усилий для того, чтобы ее продукты устанавливались как можно быстрее и проще, не создавая конфликтов с другими модулями и темами оформления, и доставляя клиентам только радость от использования продуктов. Мы будем рады если вы <a href="http://seomag.com.ua/moduli/moduli-obrabotki-zakazov/soforp-cash-memo" target="_blank">купите модуль NeoSeo Товарный чек</a> еще раз или любой другой модуль в магазине seomag.com.ua. <b>Действуют накопительные скидки!</b></p>
    <p>Однако это не всегда возможно, учитывая что opencart имеет очень слабые технические возможности для этого, поэтому просим отнестись к этим нюансам с пониманием.</p>
    <p><b>Что мы гарантируем</b>, и обеспечиваем бесплатно:</p>
    <ul>
        <li>работу наших модулей на стандартной теме оформления opencart</li>
        <li>работу наших модулей на стандартной админке opencart</li>
    </ul>
    <p>Если у вас возникла проблема с работой модуля в этом контексте, то вы всегда можете запросить бесплатную техническую поддержку по адресу <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a>.</p>
    <p><b>Что мы стараемся обеспечить, но не гарантируем</b>:</p>
    <ul>
        <li>работу наших модулей на НЕ стандартной теме оформления opencart</li>
        <li>работу наших модулей на НЕ стандартной админке opencart</li>
    </ul>
    <p>Как уже говорилось, заранее нельзя предусмотреть все нюансы чужих тем оформления, поэтому в случае проблем в этом ключе, мы обеспечиваем платную поддержку за символическую стоимость. Запросить ее, а также комплексное техническое обслуживание вашего магазина можно по адресу <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></p>
    <p><b>ВНИМАНИЕ!!!</b> Если вы испытываете трудности с установкой модулей, то вам не обязательно тратить свое драгоценное время на этот рутинный процесс. Позвольте нашим техническим специалистам выполнить это вместо вас за символическую плату, а сэкономленное время вы сможеет потратить на развитие своего бизнеса, семью и хобби. Заказать установку и техническое обслуживание можно по адресу <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></p>
    ';

$_['module_licence'] = '
    <h3 style="color:red">Благодарим вас за выбор нашего продукта!</h3>
    <p>Все права на программный продукт, далее модуль, принадлежат <a href="http://neoseo.com.ua">Веб студии NeoSeo</a>. Вы можете <a href="http://seomag.com.ua/moduli/moduli-obrabotki-zakazov/soforp-cash-memo" target="_blank">купить модуль NeoSeo Товарный чек</a> еще раз в магазине seomag.com.ua. <b>Действуют накопительные скидки!</b></p>
    <p><b>Лицензия на данный модуль дает право на:</b>
        <ul>
            <li>активацию на <b>ОДИН домен</b>. Не на сайт, не на человека, не на студию. У вас несколько доменов подключены к одному сайту - значит вам нужно несколько лицензий.</li>
            <li>на использование на своем магазине или магазине клиента.</li>
            <li>бесплатные апдейты владельцам магазина в течение года после покупки, вне зависимости от того кто был установщиком модуля</li>
        </ul>
    </p>
    <p><b>Категорически запрещается:</b>
        <ul>
            <li>Публиковать модуль на других сайтах без уведомления автора</li>
            <li>Передавать модуль третьим лицам</li>
            <li>Продавать от своего имени без предварительной договоренности с автором</li>
            <li>Использовать нелицензионные версии модулей ( варез ). В случае нарушения, анулируются все покупки по домену без возврата денег</li>
        </ul>
    </p>
    <p><b>Отказ от ответственности:</b>
        <ul>
            <li>Автор модуля не несет какой либо ответственности за материальный и нематериальный ущерб, причиненный модулем. Вы используете модуль на свой страх и риск.</li>
            <li>Чтобы значительно минимизировать риски, вы можете <a href="http://seomag.com.ua/moduli/moduli-prochie/soforp-backup" target="_blank">купить модуль NeoSeo Резервные копии</a>, который надежно защитит ваш магазин от потери данных, либо заказать комплексное обслуживание вашего магазина у автора <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></li>
            <li>Автор оставляет за собой право в любой момент изменить условия лицензионного соглашения, без согласования с конечными пользователями его продуктов.</li>
        </ul>
    </p>
    ';

$_['params_unit_option_null'] = 'ноль';
$_['params_unit_option_one_nine'] = 'одна, две, три, четыре, пять, шесть, семь, восемь, девять';
$_['params_unit_option_ten_nineteen'] = 'десять, одиннадцать, двенадцать, тринадцать, четырнадцать, пятнадцать, шестнадцать, семнадцать, восемнадцать, девятнадцать';
$_['params_tens_option'] = 'двадцать, тридцать, сорок, пятьдесят, шестьдесят, семьдесят, восемьдесят, девяносто';
$_['params_hundreds_option'] = 'сто, двести, триста, четыреста, пятьсот, шестьсот, семьсот, восемьсот, девятсот';
$_['params_money_option_coins'] = 'копейка, копейки, копеек';
$_['params_money_option_currency'] = 'гривна, гривны, гривен';
$_['params_unit_option_one_nine_thousand'] = 'одна, две, три, четыре, пять, шесть, семь, восемь, девять';
$_['params_count_money_option_thousand'] = 'тысяча, тысячи, тысяч';
$_['params_unit_option_one_nine_millon'] = 'один, два, три, четыре, пять, шесть, семь, восемь, девять';
$_['params_count_money_option_millon'] = 'миллион, миллиона, миллионов';
$_['params_unit_option_one_nine_billon'] = 'один, два, три, четыре, пять, шесть, семь, восемь, девять';
$_['params_count_money_option_billon'] = 'миллиард, миллиарда, миллиардов';