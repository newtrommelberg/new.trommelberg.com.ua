<?php
// Heading
$_['heading_title']	  = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Backups</span>';
$_['heading_title_raw']  = 'NeoSeo Backups';

// Tab
$_['tab_general']		= 'General';
$_['tab_logs']		   = 'Logs';
$_['tab_license']		= 'License';
$_['tab_support']		= 'Support';
$_['tab_usefull'] = 'Usefull links';

// Text
$_['text_success']	   = 'Success: You have modified account module!';
$_['text_module']		= 'modules';
$_['text_description']   = '<p>Backups are in the menu System \ Backups.But it will only be able to see the one who has the right to view \ delete for this module.Accordingly, immediately after installation, you must go to System \ Users \ User Group and add the rights to view the \ modifications necessary user groups</p>';
$_['text_destination_yandex.disk']	= 'Yandex.Disk';
$_['text_destination_dropbox']	= 'Dropbox';
$_['text_destination_ftp']	= 'FTP-Server';
$_['text_destination_drive']	= 'Google Drive';
$_['text_check_password']	= 'Check password';
$_['text_get_token']	= 'Get the response code from dropbox';
$_['text_check_token']	= 'Check or receive Token';
$_['text_get_access']	= 'Connect with Google Drive';
$_['text_success_clear']	= 'The logs are cleared';
$_['text_module_version']= '';

// Button
$_['button_save']		= 'Save';
$_['button_save_and_close'] = 'Save & Close';
$_['button_close']	   = 'Close';
$_['button_recheck']	 = 'Re-Check';
$_['button_clear_log']   = 'Clear logs';

// Entry
$_['entry_status']	   = 'Extension status:';
$_['entry_debug']		= 'Debug mode:';
$_['entry_replace_system_backup'] = 'Replace system backups';
$_['entry_destination']  = 'Destination:';
$_['entry_notify_list']  = 'Email notify list:';
$_['entry_notify_list_desc']  = 'Use ";" as delimiter';
$_['entry_max_copies']	  = 'Max copies to hold:';
$_['entry_max_copies_desc'] = 'Redundant backups will be deleted before creating a new backup';
$_['entry_server']	   = 'Server:';
$_['entry_token']	   = 'Token:';
$_['entry_api_key']	   = 'Api key:';
$_['entry_api_secret']	   = 'Api secret:';
$_['entry_google_api']	   = 'Api key:';
$_['entry_client_id']	   = 'Client id:';
$_['entry_client_secret']	   = 'Client secret:';
$_['entry_token_desc']   = 'A token is required to link your Dropbox account with our application';
$_['entry_folder']	   = 'Folder:';
$_['entry_username']	 = 'Login:';
$_['entry_password']	 = 'Password:';
$_['entry_confirm_password'] = 'Confirm password:';
$_['entry_cron']		 = "Cron job: ";
$_['entry_exclude_files']	 = 'Exclude this files:';
$_['entry_exclude_files_desc']	 = 'One line - one rule, wildcards are allowed<br><br>Recommended:<br>*cache/*<br>*.log';
$_['entry_exclude_tables']	= 'Exclude this tables';
$_['entry_exclude_tables_desc']	= 'One line - one rule, wildcards are allowed';
$_['entry_instruction'] = 'Read the module instruction:';
$_['entry_history'] = 'Changes history:';
$_['entry_faq'] = 'Frequency Asked Questions:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify account module!';
$_['error_empty_folder']   = 'Specify a directory.';
$_['error_zip_archive_missing'] = '<h3 style="color:red">The php-class <b>ZipArchive</b> is missing.</h3><p>The backup cannot be made without this class. Please contact your hosting provider.</p>';
$_['error_ioncube_missing'] = "";
$_['error_license_missing'] = "";

$_['mail_support'] = "";
$_['module_licence'] = "";

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/rezervnoe-kopirovanie-s-ispolzovaniem-google-drive">https://neoseo.com.ua/rezervnoe-kopirovanie-s-ispolzovaniem-google-drive</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/rezervnye-kopii#module_history">https://neoseo.com.ua/rezervnye-kopii#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/rezervnye-kopii#faqBox">https://neoseo.com.ua/rezervnye-kopii#faqBox</a>';