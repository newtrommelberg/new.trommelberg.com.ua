<?php
// Heading
$_['heading_title']    = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Slideshow';
$_['heading_title_raw']= 'NeoSeo Slideshow';
$_['new_title'] 	   = 'Slideshow';

//Tab
$_['tab_general'] 	   = 'General';
$_['tab_logs'] 		   = 'Logs';
$_['tab_license']      = 'License';
$_['tab_support']      = 'Support';

//Button
$_['button_clear_log'] = 'Clear log';
$_['button_recheck']   = 'Recheck';
$_['button_save'] 	   = 'Save';
$_['button_save_and_close']= 'Save and Close';
$_['button_close'] 	   = 'Close';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified slideshow module!';
$_['text_edit']        = 'Edit Slideshow Module';
$_['text_clear_log']   = 'Clear log';
$_['text_module_version']= '';
$_['text_success_clear']= 'Log file clean success!';
$_['text_default_title']= 'Slideshow';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_banner']     = 'Big Banner';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_banner2']    = 'Small Banners';
$_['entry_width2']     = 'Width';
$_['entry_height2']    = 'Height';
$_['entry_status']     = 'Status';
$_['entry_debug'] 	   = 'Debug';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify slideshow module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';





$_['text_module_version']   = '';