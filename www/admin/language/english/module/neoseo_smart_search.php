<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Ajax Search</span>';
$_['heading_title_raw'] = 'NeoSeo Ajax Search';

// Tab
$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';
$_['tab_support'] = 'Support';

// Button
$_['button_clear_log'] = 'Clear Log';
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';

// Text
$_['text_clear_log'] = 'Clear Log';
$_['text_description'] = '<p>Description</p>';
$_['text_success_options'] = 'Module settings updated!';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_module'] = 'Modules';
$_['text_success'] = 'Module settings updated!';
$_['text_module_version'] = '';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debugging:';
$_['entry_selector'] = 'Search field selector:';
$_['entry_product_limit'] = 'Number of products per page:';
$_['entry_link_target'] = 'Open link to product:';
$_['entry_target_self'] = 'in the current browser window';
$_['entry_target_blank'] = 'in a new browser window';
$_['entry_search_label'] = 'Text for the quantity found:';
$_['entry_name_status'] = 'Search by name:';
$_['entry_model_status'] = 'Search by model:';
$_['entry_sku_status'] = 'Search by article number:';
$_['entry_image_status'] = 'Show pictures:';
$_['entry_price_status'] = 'Show price:';
$_['entry_rating_status'] = 'Show item rating:';
$_['entry_description_limit'] = 'Number of characters in the description:';
$_['entry_image_width'] = 'Width of the product image:';
$_['entry_image_height'] = 'Heigth of the product image:';
$_['entry_show_categories'] = 'Show categories:';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';




