<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Human-Readable URLs Manager</span>';
$_['heading_title_raw'] = 'NeoSeo Human-Readable URLs Manager';
$_['text_neoseo_route_manager'] = 'NeoSeo Human-Readable URLs Manager';

// Tab
$_['tab_general'] = 'Options';
$_['tab_support'] = 'Support';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';

// Text
$_['button_recheck'] = 'Re-check license';
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_clear_log'] = 'Clear Log';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debugging:';

$_['text_description'] = '<p>Description</p>';

// Text
$_['text_module'] = 'Modules';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_success_options'] = 'Module settings updated!';
$_['text_success'] = 'SEO Keyword successfully updated!';
$_['text_success_clear'] = 'The SEO cache was successfully dropped!';
$_['text_default'] = 'Default';
$_['text_module'] = 'Modules';
$_['text_clear_log'] = 'Clear Log';
$_['button_clear_cache'] = 'Reset cache';

// Column
$_['column_query'] = 'Query';
$_['column_keyword'] = 'SEO Keyword';
$_['column_action'] = 'Action';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';
$_['text_module_version'] = '';