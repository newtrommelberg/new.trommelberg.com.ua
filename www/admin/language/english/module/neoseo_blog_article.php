<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Blog Article';
$_['heading_title_raw'] = 'NeoSeo Blog Article';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Blog Article module!';
$_['text_edit']        = 'Edit Blog Article Module';

$_['text_latest']      = 'Latest Articles';
$_['text_popular']     = 'Popular Articles';
$_['text_selected']    = 'Selected Articles';
$_['text_featured']    = 'Featured by category';
$_['text_module_version'] = '';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_title']      = 'Module Title';
$_['entry_category']   = 'Category';
$_['entry_root_category'] = 'Limit with category:';
$_['entry_type']       = 'Type';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';
$_['entry_template']   = 'Template';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Blog Article module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';