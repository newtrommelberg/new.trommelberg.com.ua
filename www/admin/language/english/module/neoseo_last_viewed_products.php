<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Viewed products</span>';
$_['heading_title_raw'] = 'NeoSeo Viewed products';
$_['new_title'] = 'Similar products';

$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';
$_['tab_support'] = 'Support';

// Text
$_['text_module'] = 'Modules';
$_['button_clear_log'] = 'Clear Log';
$_['button_recheck'] = 'Check again';
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';

// Entry
$_['entry_status'] = 'Status';
$_['entry_debug'] = 'Debugging';
$_['entry_name'] = 'Name';
$_['entry_title'] = 'Header';
$_['entry_limit'] = 'Number of goods';
$_['entry_description_limit'] = 'Number of description characters';
$_['entry_view'] = 'Scheme';
$_['entry_image_width'] = 'Image Width';
$_['entry_image_height'] = 'Image Height';
$_['entry_sort_order'] = 'Order:';
$_['entry_template'] = 'Template:';

// Text
$_['text_clear_log'] = 'Clear Log';
$_['text_module_version'] = '';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_success'] = 'Module settings updated!';
$_['text_default_title'] = 'Viewed products';
$_['text_template'] = 'Template';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_image'] = 'Image width &amp; height dimensions required!';




