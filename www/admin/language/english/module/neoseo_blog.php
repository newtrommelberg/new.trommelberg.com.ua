<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Blog</p>';
$_['heading_title_raw'] = 'NeoSeo Blog settings';

// Tab
$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_fields'] = 'Fields';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';

// Text
$_['text_success']                    = 'Settings changed!';
$_['text_edit']                       = 'Change settings';
$_['text_support']                    = 'Module Support';
$_['text_module'] = 'Modules';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear Log';
$_['text_module_version'] = '';

// Button
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Log';
$_['button_download_log'] = 'Download log file';

// Entry
$_['entry_add_to_menu']               = 'Add a link to a blog in the menu:';
$_['entry_debug'] = 'Debug mode:';
$_['entry_comment_auto_approval']     = 'AutoCheck comments:';
$_['entry_author_block_article']      = 'Add information about the author to the article:';
$_['entry_product_limit']             = 'Number of products in category:';
$_['entry_comment_limit']             = 'Number of comments:';
$_['entry_cache_results']             = 'Cache results:';
$_['entry_article_time_format']       = 'Time format for the article:';
$_['entry_comment_time_format']       = 'Time format for comments:';

$_['entry_meta_date']                 = 'Display the date in the article:';
$_['entry_meta_author']               = 'Display author in article:';
$_['entry_meta_comments']             = 'Display the number of comments in the article:';
$_['entry_meta_category']             = 'Display category in article:';

$_['entry_heading']                   = 'Headline blog:';
$_['entry_title']                     = 'Blog Title:';
$_['entry_meta_keyword']              = 'Meta Keywords:';
$_['entry_meta_description']          = 'Meta Description:';
$_['entry_articles_block_heading']    = 'Article block header:';
$_['entry_products_block_heading']    = 'Product block header:';
$_['entry_comments_block_heading']    = 'Comment block header:';
$_['entry_author_block_heading']      = 'Headline author\'s block:';
$_['entry_gallery_heading']           = 'The title of the gallery:';
$_['entry_share_social_code']         = 'Block code "Share":';

$_['entry_image_article']             = 'Article image size';
$_['entry_image_article_list']        = 'Article image size in category';
$_['entry_image_article_list_width']  = 'Article image width in category';
$_['entry_image_article_list_height'] = 'Article image height in category';

$_['entry_image_article_block']       = 'Image size of the article in the block';
$_['entry_image_article_block_width']       = 'Width of the image of the article in the block';
$_['entry_image_article_block_height']       = 'Height of the image of the article in the block';

$_['entry_image_product_block']       = 'The size of the product image in the block';
$_['entry_image_product_block_width']       = 'Width of the product image in the block';
$_['entry_image_product_block_height']       = 'Height of the product image in the block';

$_['entry_image_category_block']      = 'Category image size';
$_['entry_image_category_block_width']      = 'Image Category Width';
$_['entry_image_category_block_height']      = 'Image Category Height';

$_['entry_image_author_block']        = 'Image size of the author';
$_['entry_image_author_block_width']        = 'Image width of the author';
$_['entry_image_author_block_height']        = 'Image heigth of the author';

$_['entry_image_gallery_thumb']       = 'Image size of gallery';
$_['entry_width'] = 'Width';
$_['entry_height'] = 'Height';



// Help
$_['help_time_format'] = '<a href="http://php.net/manual/en/function.strftime.php" target="_blank">time formats</a>';

// Error
$_['error_permission'] = 'You do not have the right to change the blog settings!';
$_['error_download_logs'] = 'The log file is empty or missing!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';
$_['text_module_version'] = '';
