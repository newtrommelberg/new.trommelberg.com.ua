<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Account</span>';
$_['heading_title_raw'] = 'NeoSeo Account';

$_['tab_general'] = 'General';
$_['tab_logs'] = 'Logs';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';

// Text
$_['text_title_default'] = 'Authorize using social networks';
$_['text_module_version'] = '';
$_['text_success'] = 'Settings updated!';
$_['text_module'] = 'Module';
$_['text_success_clear'] = 'Log is success cleared !';
$_['text_facebook'] = 'Facebook';
$_['text_vkontakte'] = 'vk.com';
$_['text_odnoklassniki'] = 'Odnoklassniki.ru';
$_['text_googleplus'] = 'Google+';
$_['text_twitter'] = 'Twitter.com';
$_['text_instagram'] = 'Instagram';
$_['text_yandex'] = 'Yandex.ru';
$_['text_mailru'] = 'Mail.ru';
$_['text_google'] = 'Google';
$_['text_livejournal'] = 'Livejournal.com';
$_['text_openid'] = 'OpenID';
$_['text_lastfm'] = 'lastFM';
$_['text_linkedin'] = 'Linkedin.com';
$_['text_liveid'] = 'LiveID';
$_['text_soundcloud'] = 'SoundCloud';
$_['text_steam'] = 'Steam';
$_['text_flickr'] = 'Flickr.com';
$_['text_uid'] = 'uid.me';
$_['text_youtube'] = 'YouTube';
$_['text_webmoney'] = 'Webmoney';
$_['text_foursquare'] = 'Foursquare';
$_['text_dudu'] = 'Dudu.com';
$_['text_tumblr'] = 'Tumblr';
$_['text_vimeo'] = 'Vimeo.com';
$_['text_wargaming'] = 'Wargaming.net';

// Button
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check license';
$_['button_clear_log'] = 'Clear log';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debug mode:';
$_['entry_social_status'] = 'Authorize using social networks';
$_['entry_social_sort'] = 'Sort social networks for users';
$_['entry_social_sort_desc'] = 'Social networks will be sorted by user preferences';
$_['entry_social_networks'] = 'Social Networks';
$_['entry_social_title'] = 'Social auth Title';
// Error
$_['error_permission'] = 'You have not permissions to change module';
$_['error_save_settings'] = 'Error, settings not saved';




$_['text_module_version'] = '';
