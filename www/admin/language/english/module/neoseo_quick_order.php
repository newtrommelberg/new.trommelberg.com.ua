<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Quick order</p>';
$_['heading_title_raw'] = 'NeoSeo Quick order';

// Tab
$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_fields'] = 'Fields';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';

// Text
$_['text_template'] = 'Template';
$_['text_success'] = 'Module settings updated!';
$_['text_module'] = 'Modules';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear Log';
$_['text_order_complete'] = 'Your order № {id} is accepted for processing! <br> <br> Our manager will contact you in the nearest working hours';

// Button
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Log';
$_['button_download_log'] = 'Download log file';

// Entry
$_['entry_product_template'] = 'Product template';
$_['entry_popup_cart_template'] = 'Popup cart template';
$_['entry_cart_template'] = 'Cart template';
$_['entry_popup_form_template'] = 'Popup template "Product added to cart"';
$_['entry_status'] = 'Status:';
$_['entry_status_product'] = 'Use in the product card:';
$_['entry_status_popup_cart'] = 'Use in the drop-down box:';
$_['entry_status_cart'] = 'Use on the Recycle Bin page:';
$_['entry_status_popup_form'] = 'Use in the "Product added to cart" window:';
$_['entry_debug'] = 'Debug mode:';
$_['entry_debug_desc'] = 'The module logs will write various information for the module developer';
$_['entry_order_status_id'] = 'Order status:';
$_['entry_ecommerce'] = 'Send e-commerce statistics';
$_['entry_image_width'] = 'Image Width:';
$_['entry_image_height'] = 'Image Height:';
$_['entry_phone_mask'] = 'Input mask for the phone';
$_['entry_phone_mask_desc'] = 'Specify an input mask to minimize errors';
$_['entry_text_complete'] = 'The text of the message, in case of successful registration of the order:';
$_['entry_text_complete_desc'] = 'You can use {id}';
$_['entry_shipping_method'] = 'Shipping Method';
$_['entry_payment_method'] = 'Payment method';
$_['entry_country_id'] = 'Country';
$_['entry_city'] = 'City';
$_['entry_agreement_id'] = 'Require acceptance of the terms of the agreement';
$_['entry_agreement_default'] = 'Initial value for the daw "I accept the terms of the agreement"';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_download_logs'] = 'The log file is empty or missing!';




$_['text_module_version'] = '';
