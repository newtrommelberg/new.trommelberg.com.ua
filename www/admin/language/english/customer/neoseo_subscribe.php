<?php

// Heading
$_['heading_title'] = 'Subscribers';

// Text
$_['text_module_version'] = '';
$_['text_neoseo_subscribe'] = 'Subscribers';
$_['text_success'] = 'Module settings updated!';
$_['text_setting_success'] = 'Module settings updated!';
$_['text_store_default'] = 'Default Store';
$_['text_module'] = 'Modules';
$_['text_add'] = 'Adding a subscription';
$_['text_list'] = 'List of subscribers';
$_['text_edit'] = 'Editing a subscription';
$_['text_confirm_delete'] = 'Are you sure you want to delete the specified records?';
$_['text_edit_item'] = 'Edit';
$_['text_delete_item'] = 'Remove';

//Entry
$_['entry_date'] = 'Subscription date';
$_['entry_email'] = 'E-mail';
$_['entry_action'] = 'Action';
$_['entry_name'] = 'Name';
$_['entry_store'] = 'Store';
$_['entry_all_stores'] = 'All stores';

// Button
$_['button_save'] = 'Save';
$_['button_cancel'] = 'Cancel';
$_['button_import'] = 'Import';
$_['button_export'] = 'Export';
$_['button_close'] = 'Close';

// Error
$_['error_permission'] = 'Error: You do not have permission to modify the module data!';
$_['error_file'] = 'Error: Import file not specified!';
$_['error_no_selection'] = 'Error: Specify the items to delete!';
$_['error_url_group'] = 'Error: Required field!';
$_['error_delete_redirect'] = 'Error: You are trying to delete an existing redirect!';