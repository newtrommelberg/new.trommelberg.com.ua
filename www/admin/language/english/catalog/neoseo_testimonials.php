<?php

// Heading
$_['heading_title'] = '<img width="36" height="36" src="view/image/neoseo.png" style="float: left;">NeoSeo Customer Reviews';
$_['text_neoseo_testimonials'] = 'Customer Reviews';
$_['heading_title_raw'] = 'NeoSeo Customer Reviews';
$_['text_home'] = 'Main';

// Text
$_['text_success'] = 'Settings successfully changed!';
$_['text_list'] = 'Customer Reviews';
$_['text_add'] = 'Upload';
$_['text_edit'] = 'Editing';
$_['text_enabled'] = 'Publish';
$_['text_disabled'] = 'Hide';
$_['text_user_image'] = 'Custom Image:';

//Button
$_['button_filter'] = 'Filter';
$_['button_add'] = 'Add';
$_['button_edit'] = 'Edit';

// Column
$_['column_description'] = 'Message';
$_['column_admin_text'] = 'Answer';
$_['column_name'] = 'Author';
$_['column_rating'] = 'Rating';
$_['column_status'] = 'Status';
$_['column_date_added'] = 'Date';
$_['column_action'] = 'Action';
$_['column_store'] = 'Store';

// Entry
$_['entry_name'] = 'Author';
$_['entry_rating'] = 'Rating';
$_['entry_status'] = 'Status';
$_['entry_youtube'] = 'Link to YouTube video review';
$_['entry_description'] = 'Message';
$_['entry_admin_text'] = 'Administrator Response';
$_['entry_date_added'] = 'Date';
$_['entry_date_admin_added'] = 'Administrator response date';
$_['entry_user_image'] = 'Custom Image:';
$_['entry_store'] = 'Store';

// Error
$_['error_permission'] = 'You are not allowed to edit reviews!';
$_['error_text'] = 'The review text must contain at least 1 character!';
$_['error_rating'] = 'You need to set a rating!';

