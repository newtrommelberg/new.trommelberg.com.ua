<?php

// Text
$_['text_main_page'] = 'Main page';
$_['text_category_page'] = 'Category page';
$_['text_product_page'] = 'Card product';
$_['text_checkout_page'] = 'Checkout';
$_['text_account_page'] = 'Personal area';
$_['text_admin_home_page'] = 'Administrative panel';
$_['text_integration_services'] = 'Integration with external services';
$_['text_adding_products'] = 'Filling the store with goods';
$_['text_technical_modules'] = 'Technical modules';
$_['text_advancement'] = 'For advancement';
$_['text_improvement'] = 'Improvements';
