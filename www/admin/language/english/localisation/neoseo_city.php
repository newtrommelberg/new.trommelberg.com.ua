<?php

// Heading
$_['heading_title'] = 'NeoSeo Cities';

// Button
$_['button_refresh'] = 'Refill cities list';

// Text
$_['text_success'] = 'Success: You have modified cities!';
$_['text_list'] = 'Cities List';
$_['text_add'] = 'Add City';
$_['text_edit'] = 'Edit City';

// Column
$_['column_name'] = 'City Name';
$_['column_code'] = 'Zone Name';
$_['column_country'] = 'Country';
$_['column_action'] = 'Action';

// Entry
$_['entry_name'] = 'City Name';
$_['entry_zone'] = 'Zone Name';
$_['entry_country'] = 'Country';
$_['entry_status'] = 'Status';
$_['button_filter'] = 'Filter';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify cities!';
$_['error_name'] = 'City Name must be between 2 and 128 characters!';
$_['error_default'] = 'Warning: This city cannot be deleted as it is currently assigned as the default store zone!';
$_['error_store'] = 'Warning: This city cannot be deleted as it is currently assigned to %s stores!';
$_['error_address'] = 'Warning: This city cannot be deleted as it is currently assigned to %s address book entries!';
$_['error_affiliate'] = 'Warning: This city cannot be deleted as it is currently assigned to %s affiliates!';
