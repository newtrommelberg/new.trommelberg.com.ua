<?php

// Heading
$_['heading_title'] = 'Address';

//Buttons
$_['button_refresh'] = 'Update the address database';
$_['button_delete_all'] = 'Delete all entries';

// Text
$_['text_success'] = 'Address list updated!';
$_['text_address'] = 'Address';
$_['text_list'] = 'Address List';
$_['text_add'] = 'Adding an Address';
$_['text_edit'] = 'Editing Addresses';
$_['text_city'] = 'Cities';
$_['text_default'] = 'Default';
$_['text_confirm_all'] = 'Are you sure you want to delete all entries?';

// Column
$_['column_name'] = 'Address';
$_['column_zone'] = 'Region / Area';
$_['column_city'] = 'City';
$_['column_shipping_method'] = 'Delivery';
$_['column_action'] = 'Action';
$_['column_language_id'] = 'Localization';

// Entry
$_['entry_language_id'] = 'Localization';
$_['entry_name'] = 'Address';
$_['entry_zone'] = 'Region / Area';
$_['entry_city'] = 'City';
$_['entry_shipping_method'] = 'Delivery';

// Error
$_['error_name'] = 'The address must be between 3 and 128 characters!';
$_['error_warning'] = 'Attention: Check the form carefully for errors!';
$_['error_permission'] = 'Attention: You do not have permission to edit addresses!';
$_['error_action'] = 'Attention: The action can not be completed!';
$_['error_filetype'] = 'Invalid file type!';
