<?php

$_['text_alert_clean'] = 'This action will completely remove the image cache on your site. Would you like to continue?';

//Button
$_['button_clear_cache'] = 'Clear image cache';
