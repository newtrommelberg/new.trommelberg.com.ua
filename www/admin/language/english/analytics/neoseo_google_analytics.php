<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Google Analytics</span>';
$_['heading_title_raw'] = 'NeoSeo Google Analytics';
//Tab
$_['tab_general'] = 'General';
$_['tab_support'] = 'Support';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';
$_['tab_support'] = 'Support';

//Button
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Logs';
$_['button_download_log'] = 'Download log file';

// Text
$_['text_module_version'] = '';
$_['text_edit'] = 'Options';
$_['text_success'] = 'Module settings have been successfully updated!';
$_['text_success_clear'] = 'Logs successfully deleted';
$_['text_default'] = 'Default';
$_['text_module'] = 'Statistics';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debug mode:';
$_['entry_code'] = 'Google Analytics code:';
$_['entry_code_desc'] = 'For example UA-37668896-1';
$_['entry_remarketing'] = 'Use the remarketing tag:';
$_['entry_ecommerce'] = 'Send ecommerce data:';

// Error
$_['error_permission'] = 'You do not have permission to edit "NeoSeo Google Analytics"!';
$_['error_download_logs'] = 'Log file empty or not exist!';



