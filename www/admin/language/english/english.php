<?php
// Locale
$_['code']                          = 'en';
$_['lang']                          = 'en-GB';
$_['direction']                     = 'ltr';
$_['date_format_short']             = 'd/m/Y';
$_['date_format_long']              = 'l dS F Y';
$_['time_format']                   = 'h:i:s A';
$_['datetime_format']               = 'd/m/Y H:i:s';
$_['decimal_point']                 = '.';
$_['thousand_point']                = ' ';

// Text
$_['text_yes']                      = 'Yes';
$_['text_no']                       = 'No';
$_['text_enabled']                  = 'Enabled';
$_['text_disabled']                 = 'Disabled';
$_['text_none']                     = ' --- None --- ';
$_['text_select']                   = ' --- Please Select --- ';
$_['text_select_all']               = 'Select All';
$_['text_unselect_all']             = 'Unselect All';
$_['text_all_zones']                = 'All Zones';
$_['text_default']                  = ' <b>(Default)</b>';
$_['text_close']                    = 'Close';
$_['text_pagination']               = 'Showing %d to %d of %d (%d Pages)';
$_['text_loading']                  = 'Loading...';
$_['text_no_results']               = 'No results!';
$_['text_confirm']                  = 'Are you sure?';
$_['text_home']                     = 'Home';

// Button
$_['button_add']                    = 'Add New';
$_['button_delete']                 = 'Delete';
$_['button_save']                   = 'Save';
$_['button_cancel']                 = 'Cancel';
$_['button_cancel_recurring']       = 'Cancel Recurring Payments';
$_['button_continue']               = 'Continue';
$_['button_clear']                  = 'Clear';
$_['button_close']                  = 'Close';
$_['button_enable']                 = 'Enable';
$_['button_disable']                = 'Disable';
$_['button_filter']                 = 'Filter';
$_['button_send']                   = 'Send';
$_['button_edit']                   = 'Edit';
$_['button_copy']                   = 'Copy';
$_['button_back']                   = 'Back';
$_['button_remove']                 = 'Remove';
$_['button_refresh']                = 'Refresh';
$_['button_export']                 = 'Export';
$_['button_import']                 = 'Import';
$_['button_download']               = 'Download';
$_['button_rebuild']                = 'Rebuild';
$_['button_upload']                 = 'Upload';
$_['button_submit']                 = 'Submit';
$_['button_invoice_print']          = 'Print Invoice';
$_['button_shipping_print']         = 'Print Shipping List';
$_['button_address_add']            = 'Add Address';
$_['button_attribute_add']          = 'Add Attribute';
$_['button_banner_add']             = 'Add Banner';
$_['button_custom_field_value_add'] = 'Add Custom Field';
$_['button_product_add']            = 'Add Product';
$_['button_filter_add']             = 'Add Filter';
$_['button_option_add']             = 'Add Option';
$_['button_option_value_add']       = 'Add Option Value';
$_['button_recurring_add']          = 'Add Recurring';
$_['button_discount_add']           = 'Add Discount';
$_['button_special_add']            = 'Add Special';
$_['button_image_add']              = 'Add Image';
$_['button_geo_zone_add']           = 'Add Geo Zone';
$_['button_history_add']            = 'Add History';
$_['button_transaction_add']        = 'Add Transaction';
$_['button_route_add']              = 'Add Route';
$_['button_rule_add']               = 'Add Rule';
$_['button_module_add']             = 'Add Module';
$_['button_link_add']               = 'Add Link';
$_['button_approve']                = 'Approve';
$_['button_reset']                  = 'Reset';
$_['button_generate']               = 'Generate';
$_['button_voucher_add']            = 'Add Voucher';
$_['button_reward_add']             = 'Add Reward Points';
$_['button_reward_remove']          = 'Remove Reward Points';
$_['button_commission_add']         = 'Add Commission';
$_['button_commission_remove']      = 'Remove Commission';
$_['button_credit_add']             = 'Add Credit';
$_['button_credit_remove']          = 'Remove Credit';
$_['button_ip_add']                 = 'Add IP';
$_['button_parent']                 = 'Parent';
$_['button_folder']                 = 'New Folder';
$_['button_search']                 = 'Search';
$_['button_view']                   = 'View';
$_['button_install']                = 'Install';
$_['button_uninstall']              = 'Uninstall';
$_['button_login']                  = 'Login into Store';
$_['button_unlock']                 = 'Unlock Account';
$_['button_link']                   = 'Link';
$_['button_currency']               = 'Refresh Currency Values';
$_['button_apply']                  = 'Apply';
$_['button_category_add']           = 'Add Category';

// Tab
$_['tab_address']                   = 'Address';
$_['tab_additional']                = 'Additional';
$_['tab_admin']                     = 'Admin';
$_['tab_attribute']                 = 'Attribute';
$_['tab_customer']                  = 'Customer Details';
$_['tab_data']                      = 'Data';
$_['tab_design']                    = 'Design';
$_['tab_discount']                  = 'Discount';
$_['tab_general']                   = 'General';
$_['tab_history']                   = 'History';
$_['tab_ftp']                       = 'FTP';
$_['tab_ip']                        = 'IP Addresses';
$_['tab_links']                     = 'Links';
$_['tab_log']                       = 'Log';
$_['tab_image']                     = 'Image';
$_['tab_option']                    = 'Option';
$_['tab_server']                    = 'Server';
$_['tab_store']                     = 'Store';
$_['tab_special']                   = 'Special';
$_['tab_session']                   = 'Session';
$_['tab_local']                     = 'Local';
$_['tab_mail']                      = 'Mail';
$_['tab_module']                    = 'Module';
$_['tab_payment']                   = 'Payment Details';
$_['tab_product']                   = 'Products';
$_['tab_reward']                    = 'Reward Points';
$_['tab_shipping']                  = 'Shipping Details';
$_['tab_total']                     = 'Totals';
$_['tab_transaction']               = 'Transactions';
$_['tab_voucher']                   = 'Vouchers';
$_['tab_sale']                      = 'Sales';
$_['tab_marketing']                 = 'Marketing';
$_['tab_online']                    = 'People Online';
$_['tab_activity']                  = 'Recent Activity';
$_['tab_recurring']                 = 'Recurring';
$_['tab_action']                    = 'Action';
$_['tab_google']                    = 'Google';
$_['tab_sms']                       = 'SMS';

// Error
$_['error_exception']               = 'Error Code(%s): %s in %s on line %s';
$_['error_upload_1']                = 'Warning: The uploaded file exceeds the upload_max_filesize directive in php.ini!';
$_['error_upload_2']                = 'Warning: The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form!';
$_['error_upload_3']                = 'Warning: The uploaded file was only partially uploaded!';
$_['error_upload_4']                = 'Warning: No file was uploaded!';
$_['error_upload_6']                = 'Warning: Missing a temporary folder!';
$_['error_upload_7']                = 'Warning: Failed to write file to disk!';
$_['error_upload_8']                = 'Warning: File upload stopped by extension!';
$_['error_upload_999']              = 'Warning: No error code available!';

$_['module_licence'] = '<h2>NeoSeo Software License Terms</h2>
<p>Thank you for purchasing our web studio software.</p>
<p>Below are the legal terms that apply to anyone who visits our site and uses our software products or services. These Terms and Conditions are intended to protect your interests and interests of LLC NEOSEO and its affiliated entities and individuals (hereinafter referred to as "we", "NeoSeo") acting in the agreements on its behalf.</p>
<p><strong>1. Introduction</strong></p>
<p>These Terms of Use of NeoSeo (the "Terms of Use"), along with additional terms that apply to a number of specific services or software products developed and presented on the NeoSeo website (s), contain terms and conditions that apply to each and every one of them. the visitor or user ("User", "You" or "Buyer") of the NeoSeo website, applications, add-ons and components offered by us along with the provision of services and the website, unless otherwise noted (all services and software, software Modules offered through the NeoSeo website or auxiliary servers Isa, web services, etc. Applications on behalf NeoSeo collectively referred to as - "NeoSeo Service" or "Services").</p>
<p>NeoSeo Terms are a binding contract between NeoSeo and you - so please carefully read them.</p>
<p>You may visit and / or use the NeoSeo Services only if you fully agree to the NeoSeo Terms: By using and / or signing up to any of the NeoSeo Services, you express and agree to these Terms of Use and other NeoSeo terms, for example, provide programming services in the context of typical and non-typical tasks that are outlined here: <a href = "https:/ /neoseo.com.ua/vse-chto-nujno-znat-klienty "target =" _ blank "class =" external "> https://neoseo.com.ua/vse-chto-nujno-znat-klienty </a>, (hereinafter the NeoSeo Terms).</p>
<p>If you are unable to read or agree to the NeoSeo Terms, you must immediately leave the NeoSeo Website and not use the NeoSeo Services.</p>
<p>By using our Software products, Services, and Services, you acknowledge that you have read our Privacy Policy at <a href = "https: //neoseo.com.ua/policy-konfidencialnosti "target =" _ blank "class =" external "> https://neoseo.com.ua/politika-konfidencialnosti </a> (" Privacy Policy ")</p>
<p>This document is a license agreement between you and NeoSeo.</p>
<p>By agreeing to this agreement or using the software, you agree to all these terms.</p>
<p>This agreement applies to the NeoSeo software, any fonts, icons, images or sound files provided as part of the software, as well as to all NeoSeo software updates, add-ons or services, if not applicable to them. miscellaneous. This also applies to NeoSeo apps and add-ons for the SEO-Store, which extend its functionality.</p>
<p>Prior to your use of some of the application features, additional NeoSeo and third party terms may apply. For the correct operation of some applications, additional agreements are required with separate terms and conditions of privacy, for example, with services that provide SMS-notification services.</p>
<p>Software is not sold, but licensed.</p>
<p>NeoSeo retains all rights (for example, the rights provided by intellectual property laws) that are not explicitly granted under this agreement. For example, this license does not entitle you to:</p>
<li> <span> </span> <span> </span> separately use or virtualize software components; </li>
<li> publish or duplicate (with the exception of a permitted backup) software, provide software for rental, lease or temporary use; </li>
<li> transfer the software (except as provided in this agreement); </li>
<li> Try to circumvent the technical limitations of the software; </li>
<li> study technology, decompile or disassemble the software, and make appropriate attempts, other than those to the extent and in cases where (a) it provides for the right; (b) authorized by the terms of the license to use the components of the open source code that may be part of this software; (c) necessary to make changes to any libraries licensed under the small GNU General Public License, which are part of the software and related; </li>
<p> You have the right to use this software only if you have the appropriate license and the software was properly activated using the genuine product key or in another permissible manner.
</p>
<p> The cost of the SEO-Shop license does not include installation services, settings, and more of its stylization, as well as other paid / free add-ons. These services are optional, the cost depends on the number of hours required for the implementation of the hours, here: <a href = "https://neoseo. com.ua/vse-chto-nujno-znat-klienty "target =" _ blank "class =" external "> https://neoseo.com.ua/vse-chto-nujno-znat-klienty </a>
</p>
<p> The complete version of the document can be found here:
</p>
<p> <a href="https://neoseo.com.ua/usloviya-licenzionnogo-soglasheniya" target="_blank" class="external"> https://neoseo.com.ua/usloviya-licenzionnogo-soglasheniya </a>
</p>';
$_['mail_support'] = '<h2>Terms of free and paid information and technical support in <a class="external" href="https://neoseo.com.ua/" target="_blank"> NeoSeo</a>.</h2>

<p>Since we are confident that any quality work must be paid, all consultations requiring preliminary preparation of the answer, pay, including and case studies: &quot; look, and why your module is not working here? &quot;</p>

<p>If the answer to your question is already ready, you will receive it for free. But if you need to spend time answering the question, studying files, finding a bug and analyzing it, then we&#39;ll ask you to make a payment before you can answer.</p>

<p>We are <strong>helping to install</strong> and <strong> fix bugs when installing </strong>our modules in our order.</p>

<p>For any questions, please contact support@neoseo.com.ua.</p>

<p>See the full version of the license agreement here:<strong> </strong><a class="external" href="https://neoseo.com.ua/usloviya-licenzionnogo-soglasheniya" target="_blank"> https://neoseo.com .ua / usloviya-licenzionnogo-soglasheniya</a></p>

<p><strong>A review spell bonus!</strong></p>

<p>Dear buyer, in order to make our software products even better and you get even more, please leave feedback on our product or collaboration with our web studio on our Facebook, Google, Yandex and OpenCartForum.com pages.</p>

<p>- Write as it is, it&#39;s important for us to hear an honest and relevant assessment of our decision.</p>

<p><<strong>Links to our modules and profiles:</strong></p>

<ul>
	<li>Company profile on Opencartforum <a class="external" href="https://opencartforum.com/topic/41195-neoseo/?page=5" target="_blank"> https://opencartforum.com/ topic / 41195-neoseo /? page = 5</a></li>
	<li><a class="external" href="https://www.facebook.com/neoseo.com.ua/" target="_blank">Facebook</a> click here: <a class="internal" href="https://word-to-html.ruhttps/word-to-html.ru/ http/prntscr.com/g67dtz " target=" _blank "> http://prntscr.com/g67dtz</a></li>
	<li><a class="external" href="https://goo.gl/N6t2xw" target="_blank">Google</a>, click here: <a class="internal" href="https://word-to-html.ruhttps/word-to-html.ru/ http/prntscr.com/g3se6f " target=" _blank "> http://prntscr.com/g3se6f</a></li>
	<li><a class="external" href="https://yandex.ru/maps/org/neoseo/1835577365/" target="_blank">Company profile on Yandex</a> (review must pass the moderation of the service )</li>
	<li>&nbsp;</li>
</ul>

<p>As a thank you for the time spent writing your feedback, we&#39;ve prepared a bonus for you:</p>

<ol>
	<li>for writing a review on any two sites: a discount of 50% for any module.</li>
	<li>for writing a review on 4 sites - we give you any module you need, except for the exchange module with 1C: Enterprise as a gift!</li>
</ol>

<p>A full list of possible functional solutions (modules) is available here:<b>&nbsp;<a href="https://neoseo.com.ua/module-prices-in-rubles" target="_blank">https://neoseo.com.ua/module-prices-in-rubles</a>&nbsp;</b></p>

<p>Once again, thank you very much for being with us!</p>

<p>The NeoSeo Team</p>';
$_['error_ioncube_missing']='<h3 style="color: red">No IonCube Loader! </h3>

<p>To use our module, you need to install the IonCube Loader.</p>

<p>For installation please contact your hosting TS</p>

<p>If you can not install IonCube Loader yourself, you can also ask for help from our specialists at <a href="mailto:support@neoseo.com.ua"> support@neoseo.com.ua </a> </p>';
$_['error_license_missing'] = '<h3 style = "color: red"> Missing file with key! </h3>

<p> To obtain a file with a key, contact NeoSeo by email <a href="mailto:license@neoseo.com.ua"> license@neoseo.com.ua </a>, with the following: </p>

<ul>
<li> the name of the site where you purchased the module, for example, https://neoseo.com.ua </li>
<li> the name of the module that you purchased, for example: NeoSeo Sharing with 1C: Enterprise </li>
<li> your username (nickname) on this site, for example, NeoSeo</ li>
<li> order number on this site, e.g. 355446</ li>
<li> the main domain of the site for which the key file will be activated, for example, https://neoseo.ua</li>
</ul>

<p>Put the resulting key file at the root of the site, that is, next to the robots.txt file and click the "Check again" button.</p>';
