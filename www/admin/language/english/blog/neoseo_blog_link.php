<?php
$_['text_blogs']          = 'Blogs';
$_['text_blog_author']    = 'Authors';
$_['text_blog_category']  = 'Categories';
$_['text_blog_article']   = 'Articles';
$_['text_blog_comment']   = 'Comments';
$_['text_blog_setting']   = 'Settings';
$_['text_blog_module']    = 'Modules';
$_['text_blog_report']    = 'Report';
$_['text_module_version'] = '';
