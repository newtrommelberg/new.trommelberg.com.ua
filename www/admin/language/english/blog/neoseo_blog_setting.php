<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Blog settings</p>';
$_['heading_title_raw'] = 'NeoSeo Blog settings';

// Entry
$_['entry_add_to_menu']               = 'Add Blog to navigation:';
$_['entry_comment_auto_approval']     = 'Comment auto approval:';
$_['entry_author_block_article']      = 'Add author information to article page:';
$_['entry_product_limit']             = 'Products limit:';
$_['entry_comment_limit']             = 'Comments limit:';
$_['entry_cache_results']             = 'Cache results:';
$_['entry_article_time_format']       = 'Article time format:';
$_['entry_comment_time_format']       = 'Comment time format:';

$_['entry_meta_date']                 = 'Show date icon:';
$_['entry_meta_author']               = 'Show author icon:';
$_['entry_meta_comments']             = 'Show comments icon:';
$_['entry_meta_category']             = 'Show category icon:';

$_['entry_heading']                   = 'Blog Heading:';
$_['entry_title']                     = 'Blog Title:';
$_['entry_meta_keyword']              = 'Meta Tag Keywords:';
$_['entry_meta_description']          = 'Meta Tag Description:';
$_['entry_articles_block_heading']    = 'Articles block Heading:';
$_['entry_products_block_heading']    = 'Products block Heading:';
$_['entry_comments_block_heading']    = 'Comments block Heading:';
$_['entry_author_block_heading']      = 'Author block Heading:';
$_['entry_gallery_heading']           = 'Gallery Heading:';
$_['entry_share_social_code']         = 'Share social code:';

$_['entry_image_article']             = 'Article Image Size';
$_['entry_image_article_list']        = 'Article Image List Size';
$_['entry_image_article_block']       = 'Article Image Module Size';
$_['entry_image_product_block']       = 'Product Image Module Size';
$_['entry_image_category_block']      = 'Category Image Module Size';
$_['entry_image_author_block']        = 'Author Image Size';
$_['entry_image_gallery_thumb']       = 'Gallery Thumb Size';
$_['entry_width']                     = 'width';
$_['entry_height']                    = 'height';

// Text
$_['text_success']                    = 'Success: You have modified module Blog Setings!';
$_['text_edit']                       = 'Edit settings';
$_['text_support']                    = 'Support';
$_['text_module_version'] = '';

// Help 
$_['help_time_format']                = '<a href="http://php.net/manual/en/function.strftime.php" target="_blank">time formats</a>';

// Error
$_['error_permission']                = 'Warning: You do not have permission to modify Blog settings!';