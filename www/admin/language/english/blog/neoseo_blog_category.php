<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Blog Category</p>';
$_['heading_title_raw'] = 'NeoSeo Blog Category';

// Text
$_['text_success']          = 'Success: You have modified categories!';
$_['text_edit']             = 'Edit Category';
$_['text_add']              = 'Add Category';
$_['text_module_version'] = '';

// Column
$_['column_name']           = 'Category Name';
$_['column_sort_order']     = 'Sort Order';
$_['column_status']         = 'Status';
$_['column_action']         = 'Action';

// Help
$_['help_seo_url']          = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_top']              = 'Display in the top menu bar. Only works for the top parent categories.';

// Entry
$_['entry_name']             = 'Category Name:';
$_['entry_meta_title'] 	     = 'HTML Tag Title';
$_['entry_meta_h1'] 	     = 'HTML Tag H1';
$_['entry_meta_keyword']     = 'Meta Tag Keywords:';
$_['entry_teaser']           = 'Teaser:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description']      = 'Description:';
$_['entry_parent']           = 'Parent Category:';
$_['entry_store']            = 'Stores:';
$_['entry_seo_url']          = 'SEO URL:';
$_['entry_image']            = 'Image:';
$_['entry_sort_order']       = 'Sort Order:';
$_['entry_status']           = 'Status:';
$_['entry_top']              = 'Top:';
$_['entry_layout']           = 'Layout Override:';

// Error 
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify categories!';
$_['error_name']             = 'Category Name must be between 2 and 255 characters!';
$_['error_seo_url']          = 'SEO keyword already in use!';
$_['error_article']          = 'Warning: This Category cannot be deleted as it is currently assigned to %s articles!';