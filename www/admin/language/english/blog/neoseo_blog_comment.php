<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Blog Comment</p>';
$_['heading_title_raw'] = 'NeoSeo Blog Comment';

// Text
$_['text_success']        = 'Success: You have modified comments!';
$_['text_edit']           = 'Edit Comment';
$_['text_add']            = 'Add Comment';
$_['text_module_version'] = '';
$_['text_parent_comment'] = 'Parent comment';
$_['text_child_comments'] = 'Child comments';
$_['text_goto_comment']   = 'edit';

// Columns
$_['column_article_name'] = 'Article Title';
$_['column_author_name']  = 'Author Name';
$_['column_status']       = 'Status';
$_['column_date_added']   = 'Date Added';
$_['column_action']       = 'Action';
$_['column_comment']      = 'Comment';

// Buttons
$_['button_add_reply']    = 'Add Comment Reply';

// Help
$_['help_article']        = 'Article name must select on autocomplete.';

// Tab
$_['tab_comment']         = 'Comment';

// Entry
$_['entry_author']        = 'Author Name:';
$_['entry_article']       = 'Article Name:';
$_['entry_rating']        = 'Rating:';
$_['entry_status']        = 'Status:';
$_['entry_comment']       = 'Comment:';
$_['entry_reply_comment'] = 'Reply';

// Error
$_['error_warning']       = 'Warning: Please check the form carefully for errors!';
$_['error_permission']    = 'Warning: You do not have permission to modify comment!';
$_['error_author']        = 'Author name must be between 3 to 63 characters!';
$_['error_comment']       = 'Comment must be between 3 to 1000 characters!';
$_['error_article_name']  = 'Article Title must be require!';
$_['error_article_not_found'] = 'Article Title not found in our list!';