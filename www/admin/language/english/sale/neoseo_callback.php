<?php

// Heading
$_['heading_title'] = 'Callback';

// Text
$_['text_neoseo_callback'] = 'Callback';
$_['text_success'] = "Success: You've edited the settings!";
$_['text_setting_success'] = 'Success: You changed the settings of the configuration manager!';
$_['text_module_version'] = '';
$_['text_edit'] = 'Editing a callback order';
$_['text_list'] = 'List of callback orders';
$_['text_status_new'] = 'New';
$_['text_status_done'] = 'Processed';

//Entry
$_['entry_date'] = 'Date';
$_['entry_name'] = 'Full name';
$_['entry_phone'] = 'Phone';
$_['entry_email'] = 'Email';
$_['entry_message'] = 'Message';
$_['entry_time'] = 'Time';
$_['entry_time_from'] = 'from';
$_['entry_time_to'] = 'by';
$_['entry_comment'] = 'Note';
$_['entry_manager'] = 'Manager';
$_['entry_status'] = 'Status';


// Button
$_['button_save'] = 'Save';
$_['button_close'] = 'Close';
$_['button_import'] = 'Import';
$_['button_export'] = 'Export';

// Column
$_['column_action'] = 'Action';

// Error
$_['error_permission'] = 'Error: You do not have permission to modify this module data!';
$_['error_file'] = 'Error: Import file not specified!';
$_['error_no_selection'] = 'Error: Specify the items to delete!';