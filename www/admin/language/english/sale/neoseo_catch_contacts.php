<?php

// Heading
$_['heading_title'] = 'Захват контактов';

// Text
$_['text_neoseo_catch_contacts'] = 'Захват контактов';
$_['text_success'] = 'Успех: Вы отредактировали настройки!';
$_['text_setting_success'] = 'Успех: Вы изменили настройки!';
$_['text_module_version'] = '';
$_['text_edit'] = 'Редактирование заявки';
$_['text_list'] = 'Список заявок';
$_['text_status_new'] = 'Новая';
$_['text_status_done'] = 'Обработанная';

//Entry
$_['entry_date'] = 'Дата';
$_['entry_name'] = 'ФИО';
$_['entry_email'] = 'E-mail';
$_['entry_status'] = 'Статус';
$_['entry_site'] = 'Сайт';
$_['entry_phone'] = 'Телефон';
$_['entry_description'] = 'Описание';
$_['entry_number_request'] = 'Заявка';
$_['entry_type_subscription'] = 'Тип заявки';

// Button
$_['button_save'] = 'Сохранить';
$_['button_close'] = 'Закрыть';

// Column
$_['column_action'] = 'Действие';

// Error
$_['error_permission'] = 'Ошибка: У вас нет прав на измененеи данных модуля!';
$_['error_file'] = 'Ошибка: Не указан файл импорта!';
$_['error_no_selection'] = 'Ошибка: Укажите элементы для удаления!';
