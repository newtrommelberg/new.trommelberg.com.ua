<?php
// Heading
$_['heading_title']    = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Payment</p>';
$_['heading_title_raw']    = 'NeoSeo Payment';

// Tabs
$_['tab_general'] = 'Settings';
$_['tab_methods'] = 'Methods';
$_['tab_logs'] = 'Logs';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';

// Text
$_['text_payment']      = 'Payment';
$_['text_success']       = 'Success changed!';
$_['text_edit']          = 'Module settings edit';
$_['text_edit_payment'] = 'Edit payment+ method';
$_['text_add']           = 'Add payment+ method';
$_['text_all_zones']     = 'All Zones';
$_['text_list']          = 'Payment+ methods list';

// Buttons
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Recheck';
$_['button_clear_log'] = 'Clear log';
$_['button_remove'] = 'Remove';
$_['button_insert'] = 'Add';

// Entry
$_['entry_name']         = 'Title:';
$_['entry_description']  = 'Description:';
$_['entry_geo_zone']     = 'GEO zones:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort order:';
$_['entry_price_min']    = 'Minimum payment price:';
$_['entry_price_max']    = 'Maximum payment price:';
$_['entry_cities']       = 'Cities list:';
$_['entry_order_status'] = 'Order status after payment:';
$_['entry_debug'] = 'Debug mode';
$_['entry_debug_desc']   = 'Some information will be written to module\'s logs';
$_['entry_stores']		 = 'Stores';

$_['column_name']        = 'Payment method title';
$_['column_status']      = 'Status';
$_['column_action']      = 'Actions';
$_['column_sort_order']  = 'Sort order';

// Help
$_['help_max']    = 'Leave blank for cancel maximum payment price limits';
$_['help_min']    = 'Leave blank for cancel minimun payment price limits';
$_['help_cities'] = 'Leave blank for cancel city limits.';

// Error
$_['error_permission'] = 'You have no rights to edit this module!';
$_['error_name'] = 'To short or to long or empty title';






