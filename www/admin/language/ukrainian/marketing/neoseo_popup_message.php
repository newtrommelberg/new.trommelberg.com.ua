<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Всплывающие окна сообщений</span>';
$_['heading_title_raw'] = 'NeoSeo Всплывающие окна сообщений';

//Tabs
$_['tab_general'] = 'Параметры';
$_['tab_header'] = 'Шапка';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_clear'] = 'Очистить';
$_['text_image_manager'] = 'Менеджер изображений';
$_['text_browse'] = 'Обзор';
$_['text_position_top_left'] = 'Слева сверху';
$_['text_position_top_right'] = 'Справа сверху';
$_['text_position_top_center'] = 'По центру вверху';
$_['text_position_bottom_left'] = 'Слева внизу';
$_['text_position_bottom_right'] = 'Справа внизу';
$_['text_position_bottom_center'] = 'По центру внизу';
$_['text_position_center'] = 'По центру окна';
$_['text_position_center_left'] = 'По центру слева';
$_['text_position_center_right'] = 'По центру справа';
$_['text_action_product_page'] = 'На странице товара';
$_['text_action_manufacturer_page'] = 'На странице производителя';
$_['text_action_catalog_page'] = 'На странице категории';
$_['text_action_blog_page'] = 'На странице блогов';
$_['text_action_information_page'] = 'На информационных страницах';
$_['text_action_all_page'] = 'На любой странице';
$_['text_action_home_page'] = 'Только на главной странице';
$_['text_custom_action'] = 'Свой маршрут';
$_['text_user_action_none'] = 'Автоматический показ';
$_['text_user_action_click'] = 'При клике на селектор';
$_['text_user_action_hover'] = 'При наведении на селектор';
$_['text_edit_title'] = 'Параметры всплывающего окна';
$_['text_list_title'] = 'Всплывающие окна';
$_['text_title'] = 'Заголовок';
$_['text_status'] = 'Статус';
$_['text_page'] = 'Страницы';
$_['text_user_action'] = 'Событие';
$_['text_action'] = 'Действие';
$_['text_filter_status'] = 'Фильтр по статусу';
$_['text_filter_action'] = 'Фильтр по событию';
$_['text_filter_user_action'] = 'Фильтр по странице';
$_['text_edit_window'] = 'Редактирование окна';
$_['text_template'] = 'Шаблон';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';

// Entry
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_debug_desc'] = "В логи модуля будет писаться различная информация для разработчика модуля";
$_['entry_status'] = 'Статус:';
$_['entry_popup_background'] = 'Цвет фона окна:';
$_['entry_popup_background_image'] = 'Изображение окна:';
$_['entry_popup_title'] = 'Заголовок окна:';
$_['entry_popup_message'] = 'Текст окна:';
$_['entry_popup_position'] = 'Положение окна:';
$_['entry_popup_close'] = 'Кнопка закрытия окна:';
$_['entry_popup_number_impressions'] = 'Количество показов:';
$_['entry_popup_number_impressions_desc'] = 'Сколько раз будет всплывать окно до его отключения. Если поле не заполнено окно появляется всегда.';
$_['entry_popup_time'] = 'Время до показа окна (мс):';
$_['entry_popup_min_height'] = 'Минимальная высота окна:';
$_['entry_popup_min_width'] = 'Минимальная ширина окна:';
$_['entry_popup_title_background'] = 'Цвет фона заголовка:';
$_['entry_popup_font_color'] = 'Цвет шрифта заголовка:';
$_['entry_popup_action'] = 'Страницы для показа окна';
$_['entry_popup_user_action'] = 'Действие пользователя для показа окна';
$_['entry_popup_selector'] = 'JQuery селектор';
$_['entry_popup_selector_desc'] = 'JQuery селектор для действия, если используется показ по клику или по наведению. Например .selector или #selector';
$_['entry_sort_order'] = 'Сортировка';
$_['entry_custom_action'] = 'Укажите свой маршрут';
$_['entry_custom_action_desc'] = 'Если необходимо использовать модуль для отображения окон в нестандартном контроллере, просто укажите роут к нему. К примеру catalog/my_module';
$_['entry_popup_template'] = 'Шаблон';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';