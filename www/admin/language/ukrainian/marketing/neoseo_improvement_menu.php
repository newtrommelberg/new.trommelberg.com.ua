<?php

// Text
$_['text_main_page'] = 'Главная страница';
$_['text_category_page'] = 'Страница категории';
$_['text_product_page'] = 'Карточка товара';
$_['text_checkout_page'] = 'Оформление заказа';
$_['text_account_page'] = 'Личный кабинет';
$_['text_admin_home_page'] = 'Административная панель';
$_['text_integration_services'] = 'Интеграция с внешними сервисами';
$_['text_adding_products'] = 'Наполнение магазина товарами';
$_['text_technical_modules'] = 'Технические модули';
$_['text_advancement'] = 'Для продвижения';
$_['text_improvement'] = 'Улучшения';
