<?php

// Heading
$_['heading_title'] = 'Адреса';

//Buttons
$_['button_refresh'] = 'Оновити базу адрес';
$_['button_delete_all'] = 'Видалити всі записи';

// Text
$_['text_success'] = 'Список адресов обновлен!';
$_['text_address'] = 'Адреси';
$_['text_city'] = 'Города';
$_['text_list'] = 'Список Адрес';
$_['text_add'] = 'Додавання Адресу';
$_['text_edit'] = 'Редагування Адресу';
$_['text_default'] = 'За замовченням';
$_['text_confirm_all'] = 'Ви впевнені що бажаєте видалити всі записи?';

// Column
$_['column_name'] = 'Адреса';
$_['column_zone'] = 'Регіон / Область';
$_['column_city'] = 'Місто';
$_['column_shipping_method'] = 'Доставка';
$_['column_action'] = 'Дія';
$_['column_language_id'] = 'Локалізація';



// Entry
$_['entry_language_id'] = 'Локалізація';
$_['entry_name'] = 'Адреса';
$_['entry_zone'] = 'Регіон / Область';
$_['entry_city'] = 'Місто';
$_['entry_shipping_method'] = 'Доставка';


// Error
$_['error_name'] = 'Назва адреси повинна містити від 3 до 128 символів!';
$_['error_warning'] = 'Увага: Уважно прогляньте форму на вміст помилок!';
$_['error_permission'] = 'Увага: Ви не маєте прав для зміни адрес!';
$_['error_action'] = 'Увага: Неможливо завершити дію!';
$_['error_filetype'] = 'Недопустимий тип файлу!';
