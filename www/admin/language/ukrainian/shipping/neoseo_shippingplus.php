<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Доставка</p>';
$_['heading_title_raw']    = 'NeoSeo Доставка';

// Tabs
$_['tab_general'] = 'Параметри';
$_['tab_methods'] = 'Методи';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Підтримка';
$_['tab_license'] = 'Ліцензія';

// Text
$_['text_shipping']      = 'NeoSeo Доставка';
$_['text_success']       = 'Налаштування успішно оновлені!';
$_['text_edit']          = 'Редагування модулю';
$_['text_edit_shipping'] = 'Редагувати метод доставки+';
$_['text_add']           = 'Додати метод доставки+';
$_['text_all_zones']     = 'Всі зони';
$_['text_list']          = 'Список методів доставки+';

// Buttons
$_['button_save'] = 'Зберегти';
$_['button_save_and_close'] = 'Зберегти та Закрити';
$_['button_close'] = 'Закрити';
$_['button_recheck'] = 'Перевірити ще раз';
$_['button_clear_log'] = 'Очистити лог';
$_['button_remove'] = 'Видалити';
$_['button_insert'] = 'Додати';

// Entry
$_['entry_name']        = 'Назва:';
$_['entry_description'] = 'Опис:';
$_['entry_geo_zone']    = 'Географічна зона:';
$_['entry_geo_zone_desc']    = 'Якщо залишити без вибору, використовуються всі зони';
$_['entry_status']      = 'Статус:';
$_['entry_sort_order']  = 'Порядок сортування:';
$_['entry_price_min']   = 'Мінимальна сума замовлення:';
$_['entry_price_max']   = 'Максимальна сума замовлення:';
$_['entry_fix_payment'] = 'Фіксована вартість доставки:';
$_['entry_cities']      = 'Список міст:';
$_['entry_stores']		= 'Магазини';

$_['entry_weight_price_zone']        = 'Географічна зона:';
$_['entry_weight_price_types']        = 'Ціна на вагу, (Шаблон: <i> Вага = Вартість; Вага = Вартість </i>):';
$_['entry_weight_price_types_desc']        = '<b> Шаблон: </b> Вага = Вартість; Вага = Вартість';

$_['column_name']       = 'Назва методу доставки';
$_['column_status']     = 'Статус';
$_['column_action']     = 'Дії';
$_['column_sort_order'] = 'Порядок сортування';

// Help
$_['help_max']    = 'Для того, щоб не задавати обмеження на максимальну суму залиште поле порожнім';
$_['help_min']    = 'Для того, щоб не задавати обмеження на мінімальну суму залиште поле порожнім';
$_['help_fix_payment']    = 'Для того щоб не встановлювати фіксовану вартість, залиште поле порожнім';
$_['help_cities'] = 'Введіть міста, через разділювач «;». Для того, щоб доставка діяла у всіх містах, залиште поле порожнім';

// Error
$_['error_permission'] = 'У вас немає прав для керування модулем!';
$_['error_name'] = 'Не задана або неправильно задана назва доставки (Повинна бути від 2 до 64 символів)';






