<?php

// Heading
$_['heading_title'] = 'Обратный звонок';

// Text
$_['text_neoseo_callback'] = 'Обратный звонок';
$_['text_success'] = 'Успех: Вы отредактировали настройки!';
$_['text_setting_success'] = 'Успех: Вы изменили настройки менеджера настройки!';
$_['text_module_version'] = '';
$_['text_edit'] = 'Редактирование заказа обратного звонка';
$_['text_list'] = 'Список заказов обратного звонка';
$_['text_status_new'] = 'Новый';
$_['text_status_done'] = 'Обработан';

//Entry
$_['entry_date'] = 'Дата';
$_['entry_name'] = 'ФИО';
$_['entry_phone'] = 'Телефон';
$_['entry_email'] = 'Email';
$_['entry_message'] = 'Сообщение';
$_['entry_time'] = 'Время';
$_['entry_time_from'] = 'с';
$_['entry_time_to'] = 'по';
$_['entry_comment'] = 'Примечание';
$_['entry_manager'] = 'Менеджер';
$_['entry_status'] = 'Статус';


// Button
$_['button_save'] = 'Сохранить';
$_['button_close'] = 'Закрыть';
$_['button_import'] = 'Импорт';
$_['button_export'] = 'Экспорт';

// Column
$_['column_action'] = 'Действие';

// Error
$_['error_permission'] = 'Ошибка: У вас нет прав на измененеи данных модуля!';
$_['error_file'] = 'Ошибка: Не указан файл импорта!';
$_['error_no_selection'] = 'Ошибка: Укажите элементы для удаления!';
