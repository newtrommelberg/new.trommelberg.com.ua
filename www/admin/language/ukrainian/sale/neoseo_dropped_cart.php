<?php

// Heading
$_['heading_title'] = 'Покинутий кошик';
$_['heading_title_info'] = 'Перегляд кошика';
$_['text_dropped_cart'] = 'Покинуті кошики';

$_['column_email'] = 'Ел. скринька';
$_['column_name'] = 'Ім\'я';
$_['column_total'] = 'Сума';
$_['column_subtotal'] = 'Підсумок';
$_['column_modified'] = 'Змінено';
$_['column_phone'] = 'Телефон';
$_['column_action'] = 'Дія';
$_['column_quantity'] = 'Кількість';
$_['column_options'] = 'Опції';
$_['column_price'] = 'Ціна';
$_['column_notification_count'] = 'Надіслано повідомлень';

$_['entry_email'] = 'Ел. скринька';
$_['entry_name'] = 'Ім\'я';
$_['entry_phone'] = 'Телефон';
$_['entry_total'] = 'Загальна сума';
$_['entry_modified'] = 'Змінено';
$_['entry_notification_count'] = 'Надіслано повідомлень';

$_['error_missing_cart_id'] = 'Відсутній ідентифікатор кошика';
$_['error_cart_not_found'] = 'Кошика не знайдено';
$_['error_template_not_found'] = 'Шаблону "%s" не знайдено';

$_['email_send_successfully'] = 'Повідомлення надіслано';
$_['email_sending_error'] = 'Помилка надсилання повідомлення';

$_['button_send_notification'] = 'Надіслати повідомлення';
$_['button_view'] = 'Переглянути';
