<?php
// Heading
$_['heading_title']    = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Оплата</p>';
$_['heading_title_raw']    = 'NeoSeo Оплата';

// Tabs
$_['tab_general'] = 'Параметри';
$_['tab_methods'] = 'Методи';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Підтримка';
$_['tab_license'] = 'Ліцензія';

// Text
$_['text_payment']      = 'Оплата';
$_['text_success']       = 'Налаштування модулю успешно оновлені!';
$_['text_edit']          = 'Редагування модулю';
$_['text_edit_payment'] = 'Редагувати метод оплаты+';
$_['text_add']           = 'Додати метод оплаты+';
$_['text_all_zones']     = 'Всі зони';
$_['text_list']          = 'Список методів оплаты+';

// Buttons
$_['button_save'] = 'Зберегти';
$_['button_save_and_close'] = 'Зберегти та Закрити';
$_['button_close'] = 'Закрити';
$_['button_recheck'] = 'Перевірити ще раз';
$_['button_clear_log'] = 'Очистити лог';
$_['button_remove'] = 'Видалити';
$_['button_insert'] = 'Додати';

// Entry
$_['entry_name']        = 'Назва:';
$_['entry_description'] = 'Опис:';
$_['entry_geo_zone']    = 'Географічна зона:';
$_['entry_status']      = 'Статус:';
$_['entry_sort_order']  = 'Порядок сортування:';
$_['entry_price_min']   = 'Мінімальна сума замовлення:';
$_['entry_price_max']   = 'Максимальна сума замовлення:';
$_['entry_cities']      = 'Список міст:';
$_['entry_order_status'] = 'Статус заказа після оплати:';
$_['entry_debug'] = 'Отладочный режим';
$_['entry_debug_desc']  = 'В логи модуля будет писаться различная информация для разработчика модуля';
$_['entry_stores']		= 'Магазини';

$_['column_name']       = 'Назва методу оплати';
$_['column_status']     = 'Статус';
$_['column_action']     = 'Дії';
$_['column_sort_order'] = 'Порядок сортування';

// Help
$_['help_max']    = 'Для того, щоб не обмежувати по максимальній сумі залиште порожнім';
$_['help_min']    = 'Для того, щоб не обмежувати по мінімальній сумі залиште порожнім';
$_['help_cities'] = 'Введіть міста, через розділювач «;». Для того щоб оплата працювала у всіх містах, залиште поле пустим';

// Error
$_['error_permission'] = 'У Вас немає прав для керування модулем!';
$_['error_name'] = 'Не задана або неправильно задана назва методу оплати (Повинна бути від 2 до 64 символів)';






