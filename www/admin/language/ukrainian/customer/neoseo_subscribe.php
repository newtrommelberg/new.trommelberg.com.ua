<?php

// Heading
$_['heading_title'] = 'Подписчики';

// Text
$_['text_module_version'] = '';
$_['text_neoseo_subscribe'] = 'Подписчики';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_setting_success'] = 'Настройки модуля обновлены!';
$_['text_store_default'] = 'Магазин по умолчанию';
$_['text_module'] = 'Модули';
$_['text_add'] = 'Добавление подписки';
$_['text_list'] = 'Список подписчиков';
$_['text_edit'] = 'Редактирование подписки';
$_['text_confirm_delete'] = 'Вы действительно хотите удалить указанные записи?';
$_['text_edit_item'] = 'Изменить';
$_['text_delete_item'] = 'Удалить';

//Entry
$_['entry_date'] = 'Дата подписки';
$_['entry_email'] = 'E-mail';
$_['entry_action'] = 'Действие';
$_['entry_name'] = 'Ім’я';
$_['entry_store'] = 'Магазин';
$_['entry_all_stores'] = 'Всі магазини';

// Button
$_['button_save'] = 'Сохранить';
$_['button_cancel'] = 'Отменить';
$_['button_import'] = 'Импорт';
$_['button_export'] = 'Экспорт';
$_['button_close'] = 'Закрыть';

// Error
$_['error_permission'] = 'Ошибка: У вас нет прав на измененеи данных модуля!';
$_['error_file'] = 'Ошибка: Не указан файл импорта!';
$_['error_no_selection'] = 'Ошибка: Укажите элементы для удаления!';
$_['error_url_group'] = 'Ошибка: Поле обязательно для заполнения!';
$_['error_delete_redirect'] = 'Ошибка: Вы пытаетесь удалить не  существующий редирект!';
