<?php

$_['heading_title'] = 'Обратный звонок';

//Columns
$_['column_callback_number'] = '№';
$_['column_customer'] = 'Покупатель';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата создания';
$_['column_comment'] = 'Комментарий';
$_['column_manager'] = 'Менеджер';
$_['column_message'] = 'Сообщение';

//Entry
$_['entry_domain'] = 'Домен';
$_['entry_email'] = 'Email';

//Text
$_['text_status_enabled'] = 'Обработан';
$_['text_status_disabled'] = 'Новый';
$_['text_new_request'] = 'Новых заявок:';
$_['text_no_information'] = 'Информация отсутствует. Требуется включить модуль "NeoSeo Обратный звонок"';
$_['text_no_install'] = 'Информация отсутствует. Требуется установить модуль "NeoSeo Отслеживание битых ссылок"';
$_['text_buy'] = 'Купить модуль';
$_['text_error'] = 'Домен и почта обязательны для заполнения';
$_['text_success'] = 'Ваша заявка на info@neoseo.com.ua успешно отправлена.';

//Buttons
$_['button_request'] = 'Оставить заявку';

//Params
$_['params_email'] = 'info@neoseo.com.ua';
$_['params_subject'] = 'Новая заявка на покупку модуля "NeoSeo Отслеживание битых ссылок"';
$_['params_message'] = 'Домен: %s. Почта: %s.';
$_['params_link'] = 'https://neoseo.com.ua/uk/zayavka-na-obratnyj-zvonok';
