<?php

$_['heading_title'] = 'Отзывы о магазине';

//Entry
$_['entry_domain'] = 'Домен';
$_['entry_email'] = 'Email';

//Columns
$_['column_testimonial_number'] = '№';
$_['column_author'] = 'Автор';
$_['column_text'] = 'Отзыв';
$_['column_admin_text'] = 'Ответ администратора';
$_['column_rating'] = 'Рейтинг';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата создания';
$_['column_youtube'] = 'Ссылка на YouTube';
$_['column_image'] = 'Изображение';

//Text
$_['text_status_enabled'] = 'Опубликован';
$_['text_status_disabled'] = 'Скрыт';
$_['text_new_request'] = 'Новых отзывов о магазине:';
$_['text_no_information'] = 'Информация отсутствует. Требуется включить модуль "NeoSeo Отзывы о магазине"';
$_['text_no_install'] = 'Информация отсутствует. Требуется установить модуль "NeoSeo Отзывы о магазине"';
$_['text_buy'] = 'Купить модуль';
$_['text_error'] = 'Домен и почта обязательны для заполнения';
$_['text_success'] = 'Ваша заявка на info@neoseo.com.ua успешно отправлена.';

//Buttons
$_['button_request'] = 'Оставить заявку';

//Params
$_['params_email'] = 'info@neoseo.com.ua';
$_['params_subject'] = 'Новая заявка на покупку модуля "NeoSeo Отслеживание битых ссылок"';
$_['params_message'] = 'Домен: %s. Почта: %s.';
$_['params_link'] = 'https://neoseo.com.ua/uk/otzyvy-pokupatelej';

