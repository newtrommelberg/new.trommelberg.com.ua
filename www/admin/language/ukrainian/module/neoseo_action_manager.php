<?php
// Heading
$_['heading_title']      = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Менеджер Акций</p>';
$_['heading_title_raw']  = 'NeoSeo Менеджер Акций';

//Tabs
$_['tab_general']       = 'Параметры';
$_['tab_support']       = 'Поддержка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_success']       = 'Настройки модуля обновлены!';
$_['text_module']        = 'Модули';
$_['text_description']   = '<p>Таблица со списком акций находится в меню Каталог \ Акции. Но увидеть ее сможет только тот, у кого есть права на просмотр \ удаление для этого модуля</p><p>Соответственно, сразу после установки вы должны зайти в Система \ Пользователи \ Группы пользователей и добавить права на просмотр \ модификацию нужным группам пользователей</p>';
$_['clear']='Очистить логи';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_clear_log'] = 'Очистить логи';
$_['button_download_log'] = 'Скачать файл логов';

// Entry
$_['entry_status']       = 'Статус:';
$_['entry_debug']        = 'Отладка:';
$_['entry_meta_title']        = 'Метатег Title:';
$_['entry_meta_description']        = 'Метатег Description:';
$_['entry_meta_keyword']        = 'Метатег Keywords:';

// Error
$_['error_permission']   = 'У Вас нет прав для управления этим модулем!';
$_['error_other_errors']   = 'Нет нужных компонентов!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';




$_['text_module_version'] = '';