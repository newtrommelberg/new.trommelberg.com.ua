<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Обратный звонок</p>';
$_['heading_title_raw'] = 'NeoSeo Обратный звонок';

// Tab
$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_fields'] = 'Поля';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';

// Button
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_debug_desc'] = 'В логи модуля будет писаться различная информация для разработчика модуля';
$_['entry_message'] = 'Сообщение о принятой заявке';
$_['entry_notify'] = 'Получатели сообщения о заявке, через запятую';
$_['entry_notify_subject'] = 'Тема письма о новой заявке';
$_['entry_notify_message'] = 'Сообщение письма о новой заявке';
$_['entry_notify_message_desc'] = 'Допускается использование {callback_id},{name},{email},{message},{time_from},{time_to}';
$_['entry_phone_mask'] = 'Маска ввода для телефона';
$_['entry_phone_mask_desc'] = 'Укажите маску ввода, чтобы минимизировать ошибки';
$_['entry_use_email'] = 'Выводить email на форме заказа';
$_['entry_title'] = 'Заголовок формы';

//Params
$_['param_message'] = "Ваша заявка #{callback_id} на обратный звонок записана!\nНаш менеджер перезвонит вам в течение 15 минут в рабочее время.";
$_['param_notify_subject'] = "Получена заявка на обратный звонок №{callback_id}";
$_['param_notify_message'] = "Посетитель {name} оставил заявку №{callback_id} на обратный звонок. Он(а) просит перезвонить ему на номер {phone} по вопросу {message}.";
$_['param_title'] = "Укажите ваши данные";

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_supplier_info'] = 'Это поле обязательно для заполнения!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';




$_['text_module_version'] = '';

