<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Віджет нотаток на панелі управління</span>';
$_['heading_title_raw'] = 'NeoSeo Віджет нотаток';

//Tabs
$_['tab_general'] = 'Параметри';
$_['tab_notes'] = 'Нотатки';
$_['tab_header'] = 'Шапка';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Підтримка';
$_['tab_license'] = 'Ліцензія';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Налаштування модуля оновлені!';
$_['text_module'] = 'Модулі';
$_['text_success_clear'] = 'Лог файл успішно очищений!';
$_['text_clear_log'] = 'Очистити лог';
$_['text_clear'] = 'Очистити';
$_['text_title'] = 'Нотатки';
$_['text_add'] = 'Додати нотатку';
$_['text_edit'] = 'Редагувати нотатку';
$_['text_date_notification'] = 'Повідомити:';

//Buttons
$_['button_save'] = 'Зберегти';
$_['button_save_and_close'] = 'Зберегти i Закрити';
$_['button_close'] = 'Закрити';
$_['button_recheck'] = 'Перевірити ще раз';
$_['button_clear_log'] = 'Очистити лог';
$_['button_download_log'] = 'Завантажити файл логів';
$_['button_add_note'] = 'Додати нотатку';

// Entry
$_['entry_debug'] = 'Налагоджувальний режим<br /><span class="help">У логи модуля буде записуватися різна інформація для розробника модуля.</span>';
$_['entry_status'] = 'Статус';
$_['entry_title'] = 'Назва блоку';
$_['entry_name'] = 'Назва нотатки';
$_['entry_text'] = 'Текст нотатки';
$_['entry_use_notification'] = 'Повідомити';
$_['entry_date_notification'] = 'Дата повідомлення';
$_['entry_text_notification'] = 'Текст повідомлення';
$_['entry_text_notification_desc'] = 'У зазначений день та час на панелі управління буде виведено оповіщення';
$_['entry_show_dashboard'] = 'Виводити у віджет на панелі управління';
$_['entry_sort_order'] = 'Порядок сортування';
$_['entry_color'] = 'Колір нотатки';
$_['entry_font_color'] = 'Колір шрифту';

// Error
$_['error_permission'] = 'У Вас немає прав для управління цим модулем!';
$_['error_download_logs'] = 'Файл логів порожній або відсутній!';
$_['error_empty'] = 'Поле обо’язкове для заповнення!';



