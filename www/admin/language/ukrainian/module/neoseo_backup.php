﻿<?php
// Heading
$_['heading_title']	  = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Резервні копії</span>';
$_['heading_title_raw']  = 'NeoSeo Резервні копії';

// Tab
$_['tab_general']		= 'Параметри';
$_['tab_support']		= 'Підтримка';
$_['tab_usefull'] = 'Корисні посилання';

// Text
$_['text_success']	   = 'Налаштування модуля оновлені!';
$_['text_module']		= 'Модулі';
$_['text_description']   = '<p>Резервні копії знаходяться в меню Система \ Резервні копії. Але побачити її зможе тільки той, у кого є права на перегляд \ видалення для цього модуля. Відповідно, відразу після встановлення ви повинні зайти в Система \ Користувачі \ Групи користувачів і додати права на перегляд \ модифікацію потрібним групам користувачів</p>';
$_['text_destination_yandex.disk']	= 'Яндекс.Диск';
$_['text_destination_ftp']	= 'FTP-Сервер';
$_['text_destination_drive']	= 'Гугл Диск';
$_['text_check_password']	= 'Перевірити пароль';
$_['text_get_token']	= 'Отримати код-відповідь від дропбокс';
$_['text_get_access']	= 'Підключитись до Гугл Диску';
$_['text_check_token']	= 'Перевірити чи отримати Токен';
$_['text_success_clear']	= 'Логи успішно видалені';
$_['text_module_version']= '';

// Button
$_['button_save']		= 'Зберегти';
$_['button_save_and_close'] = 'Зберегти і Закрити';
$_['button_close']	   = 'Закрити';
$_['button_recheck']	 = 'Перевірити ще раз';

// Entry
$_['entry_status']	   = 'Статус:';
$_['entry_debug']		= 'Налагодження:';
$_['entry_replace_system_backup'] = 'Замінити системні резервні копії';
$_['entry_destination']  = 'Куди зберігати:';
$_['entry_notify_list']  = 'Кому повідомляти:<br><i>( роздільник - ; )</i>';
$_['entry_max_copies']   = 'Максимальна кількість копій:<br><br>Зайві резервні копії будуть видалені перед створенням нової резервної копії';
$_['entry_server']	   = 'Сервер:';
$_['entry_token']	   = 'Токен:';
$_['entry_api_key']	   = 'Api ключ:';
$_['entry_api_secret']	   = 'Api секрет:';
$_['entry_google_api']	   = 'Api ключ:';
$_['entry_client_id']	   = 'Клієнт id:';
$_['entry_client_secret']	   = 'Клієнт секрет:';
$_['entry_token_desc']   = 'Токен необхідний для звязку вашого акаунта Дропбокс з додатком';
$_['entry_folder']	   = 'Каталог:';
$_['entry_username']	 = 'Логін:';
$_['entry_password']	 = 'Пароль:';
$_['entry_confirm_password'] = 'Підтвердіть пароль:';
$_['entry_cron']		 = "Запуск із крону: ";
$_['entry_exclude_files']	 = 'Виключити ці файли:<br><br><i>Один рядок - одне правило, маски дозволені</i><br><br><i>Рекомендується:<br>*cache/*<br>*.log</i>';
$_['entry_exclude_tables']	= 'Виключити ці таблиці:<br><br><i>Один рядок - одне правило, маски дозволені</i>';
$_['entry_instruction'] = 'Інструкція до модуля:';
$_['entry_history'] = 'Історія змін:';
$_['entry_faq'] = 'Найчастіші запитання:';

// Error
$_['error_permission']   = 'У Вас немає прав для керування цим модулем!';
$_['error_empty_folder']   = 'Вкажіть каталог.';
$_['error_zip_archive_missing'] = '<p>Увага!!! Відсутній критично необхідний php-клас <b>ZipArchive</b>. Без цього класу неможливе створення резервної копії. Зверніться до вашого хостера.</p>';
$_['error_ioncube_missing'] = "";
$_['error_license_missing'] = "";

$_['mail_support'] = "";
$_['module_licence'] = "";

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/rezervnoe-kopirovanie-s-ispolzovaniem-google-drive">https://neoseo.com.ua/rezervnoe-kopirovanie-s-ispolzovaniem-google-drive</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/rezervnye-kopii#module_history">https://neoseo.com.ua/rezervnye-kopii#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/rezervnye-kopii#faqBox">https://neoseo.com.ua/rezervnye-kopii#faqBox</a>';