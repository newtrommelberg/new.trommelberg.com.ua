<?php
// Heading
$_['heading_title']    = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Слайдшоу';
$_['heading_title_raw']= 'NeoSeo Слайдшоу';
$_['new_title'] 	   = 'Слайдшоу';

//Tab
$_['tab_general'] 	   = 'Параметри';
$_['tab_logs'] 		   = 'Логи';
$_['tab_license'] 	   = 'Ліцензія';
$_['tab_support'] 	   = 'Підтримка';

//Button
$_['button_clear_log'] = 'Очистити лог';
$_['button_recheck']   = 'Перевірити ще раз';
$_['button_save'] 	   = 'Зберегти';
$_['button_save_and_close']= 'Зберегти та Закрити';
$_['button_close'] 	   = 'Закрити';

// Text
$_['text_module']      = 'Модулі';
$_['text_success']     = 'Налаштування успішно змінені!';
$_['text_edit']        = 'Налаштування модуля';
$_['text_clear_log']   = 'Очистити лог';
$_['text_module_version']= '';
$_['text_success_clear']= 'Лог файл вдало очищений!';
$_['text_default_title']= 'Слайдшоу';

// Entry
$_['entry_name']       = 'Назва модуля';
$_['entry_banner']     = 'Головний банер';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Висота';
$_['entry_banner2']    = 'Додаткові банери';
$_['entry_width2']     = 'Ширина';
$_['entry_height2']    = 'Висота';
$_['entry_status']     = 'Статус';
$_['entry_debug'] 	   = 'Отладка';

// Error
$_['error_permission'] = 'Ви не маєте повноважень для управління цим модулем!';
$_['error_name']       = 'Назва модуля повинна містити від 3 до 64 символів!';
$_['error_width']      = 'Введіть ширину зображення!';
$_['error_height']     = 'Введіть висоту зображення!';





$_['text_module_version']   = '';
