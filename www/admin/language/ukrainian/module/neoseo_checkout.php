<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Оформлення замовлення';
$_['heading_title_raw'] = 'NeoSeo Оформлення замовлення';

// Tab
$_['tab_general'] = 'Основні';
$_['tab_customer'] = 'Покупець';
$_['tab_payment'] = 'Оплата';
$_['tab_shipping'] = 'Доставка';
$_['tab_shipping_type'] = 'Типи доставки';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Підтримка';
$_['tab_license'] = 'Ліцензія';

// Text
$_['text_module'] = 'Модулі';
$_['text_success'] = 'Успіх: Вы успішно змінили налаштування модуля NeoSeo Оформлення замовлення!';
$_['text_title'] = 'Оформлення замовлення';
$_['text_radio_type'] = 'Перемикач';
$_['text_select_type'] = 'Список';
$_['text_text_type'] = 'Text Type';
$_['text_city_select_default'] = 'Вручну вводити країну і регіон';
$_['text_city_select_cities'] = 'Використовувати міста з адмінки';
$_['text_clear_log'] = 'Очистити логи';
$_['text_success_clear'] = 'Логи успешно очищены';
$_['text_type-0'] = 'Фізична особа';
$_['text_type-1'] = 'Юридична особа';
$_['text_dependency_disabled'] = 'Оплата і доставка не залежать одне від одного';
$_['text_dependency_payment_for_shipping'] = 'Оплата залежить від доставки';
$_['text_dependency_shipping_for_payment'] = 'Доставка залежить від оплати';
$_['text_types_of_customers'] = 'Типи покупців';
$_['text_block_customers'] = 'Поля для блоку "Покупець":';
$_['text_block_payment'] = 'Поля для методу оплати';
$_['text_block_shipping'] = 'Поля для методу доставки';
$_['text_shipping'] = 'Метод доставки';
$_['text_payment'] = 'Метод оплати';
$_['text_type_shipping'] = 'Тип доставки';
$_['text_pick_your_own'] = 'Забрати самостійно';
$_['text_order_delivery'] = 'Замовити доставку';
$_['text_select_all'] = 'Виділити все';
$_['text_name'] = 'Назва:';
$_['text_field'] = 'Поле:';
$_['text_type'] = 'Тип:';
$_['text_identifier'] = 'Ідентифікатор:';
$_['text_mask'] = 'Маска введення:';
$_['text_initial_value'] = 'Початкове значенн:';
$_['text_show'] = 'Показувати:';
$_['text_required'] = "Обов'язкове:";

// Entry
$_['entry_status'] = 'Стан: <a class="tooltip-trigger" title="Central function to turn on/off this extension."><i class="fa fa-question-circle"></i></a>';
$_['entry_payment_logo'] = 'Показувати логотип методу оплати: <a class="tooltip-trigger" title="Display the payment logo for payment methods. This only works when payment method is displayed in radio mode."><i class="fa fa-question-circle"></i></a>';
$_['entry_payment_control'] = 'Вибір методу оплати: <a class="tooltip-trigger" title="Sets the mode payment methods are displayed in. Either radio buttons or select drop down."><i class="fa fa-question-circle"></i></a>';
$_['entry_shipping_control'] = 'Вибір методу доставки: <a class="tooltip-trigger" title="Sets the mode shipping methods are displayed in. Either radio buttons or select drop down."><i class="fa fa-question-circle"></i></a>';
$_['entry_shipping_city_select'] = 'Введення міста';
$_['entry_shipping_country_select'] = 'Введення країни';
$_['entry_shipping_country_default'] = 'Країна за замовчуванням';
$_['entry_shipping_zone_default'] = 'Регіон за замовчуванням';
$_['entry_shipping_city_default'] = 'Місто за замовчуванням';
$_['entry_novaposhta_city_name'] = 'Город за замовчуванням для доставки "Нова пошта"';
$_['entry_novaposhta_city_name_desc'] = 'В системе знайдено встановлений модуль "NeoSeo Нова пошта" з функцією створення накладних. Для його корректної работы необхшдно обрати город за замовчуванням для отримувача, якщо ви хочете використовувати параметр "Город за замовчуванням"';
$_['entry_shipping_title'] = 'Вывод названия группы доставок';
$_['entry_agreement_id'] = 'Вимагати прийняття умов угоди';
$_['entry_warehouse_types'] = 'Тип відділення нової пошти';
$_['entry_agreement_default'] = 'Початкове значення для галки "Я приймаю умови угоди"';
$_['entry_agreement_text'] = 'Виводити вимоги текстом';
$_['entry_stock_control'] = 'Перевіряти наявність товару з корзини';
$_['entry_min_amount'] = 'Минимальная сумма заказа';
$_['entry_use_shipping_type'] = 'Використовувати типи доставки';
$_['entry_compact'] = 'Компактный вывод';
$_['entry_dependency_type'] = 'Тип залежності';
$_['entry_dropped_cart_template'] = 'Шаблон повідомлення покинутого кошика';
$_['entry_dropped_cart_email_subject'] = 'Тема повідомлення покинутого кошика';
$_['entry_onestep'] = 'Оформление в 1 шаг';
$_['entry_cart_redirect'] = 'Перенаправляти з кошика на оплату';
$_['entry_hide_menu'] = 'Приховати меню під час оформлення замовлення';
$_['entry_hide_footer'] = 'Приховати футер під час оформлення замовлення';
$_['entry_api_key'] = 'API key Нової пошти';
$_['entry_api_key_desc'] = 'Використовується для поновлення відділень нової пошти. <a href="https://devcenter.novaposhta.ua/" target="_blank">Отримати ключ</a>';
$_['entry_use_international_phone_mask'] = 'Використовувати маску для телефону';
$_['entry_use_international_phone_mask_desc'] = 'Використовувати плагін International Telephone Input для введення і перевірки міжнародних телефонних номерів. Скасовує введену маску в налаштуваннях покупця.';

// Technical
$_['entry_debug'] = 'Налагодження: <a class="tooltip-trigger" title="Turn on debug mode for checkout. Only turn this on if you know what you are doing."><i class="fa fa-question-circle"></i></a>';
$_['entry_payment_reloads_cart'] = 'Оновлювати корзину при зміні методу оплати: <a class="tooltip-trigger" title="Only enable if your payment methods have surcharges. Disable to reduce ajax requests."><i class="fa fa-question-circle"></i></a>';
$_['entry_shipping_reloads_cart'] = 'Оновлювати корзину при зміні методу доставки: <a class="tooltip-trigger" title="Only enable if your payment methods are dependent on your shipping methods. Disable to reduce ajax requests."><i class="fa fa-question-circle"></i></a>';
$_['entry_shipping_novaposhta'] = 'Метод доставки для новой почты';

// Module
$_['entry_coupon'] = 'Відображати Модуль Купонів: <a class="tooltip-trigger" title="Turn on/off the coupon module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_voucher'] = 'Відображати Модуль Ваучерів: <a class="tooltip-trigger" title="Turn on/off the voucher module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_reward'] = 'Відображати Модуль Бонусних Балів: <a class="tooltip-trigger" title="Turn on/off the reward module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_cart'] = 'Відображати Модуль Корзини: <a class="tooltip-trigger" title="Turn on/off the cart module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_shipping_module'] = 'Відображати Модуль Типу Доставки: <a class="tooltip-trigger" title="Turn on/off the shipping method module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_payment_module'] = 'Відображати Модуль Способу Оплати: <a class="tooltip-trigger" title="Turn on/off the payment method module on the checkout page."><i class="fa fa-question-circle"></i></a>';
$_['entry_login_module'] = 'Відображати Модуль Логіну: <a class="tooltip-trigger" title="Turn on/off the login module on the checkout page."><i class="fa fa-question-circle"></i></a>';

$_['entry_aways_show_delivery_block'] = 'Показувати доставку перед вибором міста';
$_['entry_aways_show_delivery_block_desc'] = "Увага! Не всі методи доставки можуть бути відображені. Деякі методи доставки вимагають обов'язкового вибору міста до того, як вони будуть показані. При включенні даної опції блок введення міста буде приховано, і відображений тільки для служб доставки, обраних нижче.";
$_['entry_shipping_require_city'] = "Список методів доставки, яким необхідний вибір міста";
$_['entry_shipping_require_city_desc'] = "При використанні опції 'Показувати доставку перед вибором міста' блок вибору міста буде відображений тільки для обраних методів доставки";

// Button
$_['button_add'] = 'Додати';
$_['button_continue'] = 'Застосувати';
$_['button_save_and_close'] = 'Зберегти і Закрити';
$_['button_clear_log'] = 'Видалити лог';

// Error
$_['error_permission'] = '';
$_['error_warehouse_types'] = 'Ошибка выполнения запроса получения типов отделений. Смотрите лог для подробностей.';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';

$_['mail_support'] = '';
$_['module_licence'] = '';
$_['text_module_version'] = '';
