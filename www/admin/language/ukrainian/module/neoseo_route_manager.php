<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Менеджер ЛПУ</span>';
$_['heading_title_raw'] = 'NeoSeo Менеджер ЛПУ';
$_['text_neoseo_route_manager'] = 'NeoSeo Менеджер ЛПУ';

$_['tab_general'] = 'Параметри';
$_['tab_support'] = 'Підтримка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Ліцензія';

// Text
$_['text_module'] = 'Модулі';
$_['button_clear_log'] = 'Очистить лог';
$_['button_save'] = 'Зберегти';
$_['button_save_and_close'] = 'Зберегти і Закрити';
$_['button_close'] = 'Закрити';
$_['button_recheck'] = 'Перевірити еще раз';
// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Налагодження:';

$_['text_description'] = '<p>Опис</p>';

// Text
$_['text_success_clear'] = 'Лог файл успішно очищено!';
$_['text_success_options'] = 'Налаштування модуля оновлені!';
$_['text_success'] = 'SEO Keyword успішно оновлені!';
$_['text_success_clear'] = 'Кеш SEO успішно скинуто!';
$_['text_default'] = 'За замовчуванням';
$_['text_module'] = 'Модулі';
$_['text_clear_log'] = 'Очистити лог';
$_['button_clear_cache'] = 'Скинути кеш';

// Column
$_['column_query'] = 'Query';
$_['column_keyword'] = 'SEO Keyword';
$_['column_action'] = 'Дія';

// Error
$_['error_permission'] = 'У Вас немає прав для керування цим модулем!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';

$_['mail_support'] = '';
$_['module_licence'] = '';

$_['text_module_version'] = '';