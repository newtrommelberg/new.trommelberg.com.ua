<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Быстрый заказ</p>';
$_['heading_title_raw'] = 'NeoSeo Быстрый заказ';

// Tab
$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_fields'] = 'Поля';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_order_complete'] = 'Ваш заказ №{id} принят в обработку!<br><br>Наш менеджер свяжется с вами в ближайшее рабочее время';
$_['text_template'] = 'Шаблон';

// Button
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_status_product'] = 'Использовать в карточке товара:';
$_['entry_status_popup_cart'] = 'Шаблон в всплывающей корзине:';
$_['entry_status_cart'] = 'Использовать на странице корзины:';
$_['entry_status_popup_form'] = 'Использовать в окне "Товар добавлен в корзину":';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_debug_desc'] = 'В логи модуля будет писаться различная информация для разработчика модуля';
$_['entry_order_status_id'] = 'Статус заказа:';
$_['entry_ecommerce'] = 'Отсылать e-сommerce статистику';
$_['entry_image_width'] = 'Ширина изображения:';
$_['entry_image_height'] = 'Высота изображения:';
$_['entry_phone_mask'] = 'Маска ввода для телефона';
$_['entry_phone_mask_desc'] = 'Укажите маску ввода чтобы минимизировать ошибки';
$_['entry_text_complete'] = 'Текст сообщения, в случае успешного оформления заказа:';
$_['entry_text_complete_desc'] = 'Допускается использование {id}';
$_['entry_shipping_method'] = 'Метод доставки';
$_['entry_payment_method'] = 'Метод оплаты';
$_['entry_country_id'] = 'Страна';
$_['entry_city'] = 'Город';
$_['entry_product_template'] = 'Шаблон для карточки товара';
$_['entry_popup_cart_template'] = 'Шаблон всплывающей корзины';
$_['entry_cart_template'] = 'Шаблон в корзине';
$_['entry_popup_form_template'] = 'Шаблон всплывающего окна "Товар добавлен в корзину"';
$_['entry_agreement_id'] = 'Вимагати прийняття умов угоди';
$_['entry_agreement_default'] = 'Початкове значення для галки "Я приймаю умови угоди"';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';





$_['text_module_version'] = '';
