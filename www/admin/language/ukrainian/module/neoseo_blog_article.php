<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Статті';
$_['heading_title_raw'] = 'NeoSeo Блог Статті';

// Text
$_['text_module']      = 'Модулі';
$_['text_success']     = 'Модуль Блог Статті успішно змінений!';
$_['text_edit']        = 'Редагування модуля Блог Статті';
$_['text_latest']      = 'Останні статті';
$_['text_popular']     = 'Популярні статті';
$_['text_selected']    = 'Вибрані статті';
$_['text_featured']    = 'Рекомендовані по категорії';
$_['text_module_version'] = '';

// Buttons
$_['button_add_articles'] = 'Додати статтю';

// Entry
$_['entry_name']       = 'Назва модуля';
$_['entry_title']      = 'Заголовок модуля';
$_['entry_category']   = 'Категорія';
$_['entry_root_category'] = 'Обмежити категорією:';
$_['entry_type']       = 'Тип';
$_['entry_limit']      = 'Ліміт';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Висота';
$_['entry_status']     = 'Статус';
$_['entry_template']   = 'Шаблон';
$_['entry_selected_articles'] = 'Вибрані статті';
$_['entry_article_name'] = 'Назва статті:';
$_['entry_sort_order']   = 'Сортування:';
$_['entry_status']       = 'Статус:';

// Error
$_['error_permission'] = 'У вас немає прав змінювати модуль Блог Статті!';
$_['error_name']       = 'Назва модуля повинна бути від 3 до 64 символів!';
$_['error_width']      = 'Ширина обов\'язкова!';
$_['error_height']     = 'Висота обов\'язкова!';