<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Особистий кабінет користувача</span>';
$_['heading_title_raw'] = 'NeoSeo Особистий кабінет користувача';

$_['tab_general'] = 'Параметри';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Підтримка';
$_['tab_license'] = 'Ліцензія';

// Text
$_['text_title_default'] = 'Авторизуватись через соціальні мережі:';
$_['text_module_version'] = '';
$_['text_success'] = 'Налаштування модулів модуля обновлены!';
$_['text_module'] = 'Модулі';
$_['text_success_clear'] = 'Лог успішно очищений!';
$_['text_facebook'] = 'Facebook';
$_['text_vkontakte'] = 'vk.com';
$_['text_odnoklassniki'] = 'Одноклассники';
$_['text_googleplus'] = 'Google+';
$_['text_twitter'] = 'Twitter.com';
$_['text_instagram'] = 'Instagram';
$_['text_yandex'] = 'Yandex.ru';
$_['text_mailru'] = 'Mail.ru';
$_['text_google'] = 'Google';
$_['text_livejournal'] = 'Livejournal.com';
$_['text_openid'] = 'OpenID';
$_['text_lastfm'] = 'lastFM';
$_['text_linkedin'] = 'Linkedin.com';
$_['text_liveid'] = 'LiveID';
$_['text_soundcloud'] = 'SoundCloud';
$_['text_steam'] = 'Steam';
$_['text_flickr'] = 'Flickr.com';
$_['text_uid'] = 'uid.me';
$_['text_youtube'] = 'YouTube';
$_['text_webmoney'] = 'Webmoney';
$_['text_foursquare'] = 'Foursquare';
$_['text_dudu'] = 'Dudu.com';
$_['text_tumblr'] = 'Tumblr';
$_['text_vimeo'] = 'Vimeo.com';
$_['text_wargaming'] = 'Wargaming.net';

// Button
$_['button_save'] = 'Зберегти';
$_['button_save_and_close'] = 'Зберегти і закрити';
$_['button_close'] = 'Закрити';
$_['button_recheck'] = 'Перевірити ще раз';
$_['button_clear_log'] = 'Очистити лог';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Режим відладки:';
$_['entry_social_status'] = 'Авторизація через соціальні мережі';
$_['entry_social_sort'] = 'Сортування за уподобаннями соц-мереж';
$_['entry_social_sort_desc'] = 'Соціальні мережі будуть відсортовані згідно з уподобаннями користувачів';
$_['entry_social_networks'] = 'Соціальні мережі';
$_['entry_social_title'] = 'Заголовок блоку соціальних мереж';
// Error
$_['error_permission'] = 'У Вас немає прав для керування модулем!';
$_['error_save_settings'] = 'Не вдалось зберегти конфігураційні файли';




$_['text_module_version'] = '';
