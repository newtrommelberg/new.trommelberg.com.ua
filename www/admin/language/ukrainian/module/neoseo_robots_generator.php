<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Генератор robots.txt</p>';
$_['heading_title_raw'] = 'NeoSeo Генератор robots.txt';

//Tab
$_['tab_general'] = 'Загальні';
$_['tab_support'] = 'Підтримка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Ліцензія';
$_['tab_support'] = 'Підтримка';

//Button
$_['button_save'] = 'Зберегти';
$_['button_save_and_close'] = 'Зберегти и Закрити';
$_['button_close'] = 'Закрити';
$_['button_recheck'] = 'Перевірити ще раз';
$_['button_clear_log'] = 'Почистити логи';

// Text
$_['text_module_version'] = '';
$_['text_edit'] = 'Параметри';
$_['text_success'] = 'Налаштування модуля успішно оновлені!';
$_['text_success_clear'] = 'Логи успішно видалені';
$_['text_default'] = 'За замовчванням';
$_['text_feed'] = 'Канали просування';
$_['text_gzip_0'] = 'Відключено';
$_['text_gzip_1'] = 'Рівень 1';
$_['text_gzip_2'] = 'Рівень 2';
$_['text_gzip_3'] = 'Рівень 3';
$_['text_gzip_4'] = 'Рівень 4';
$_['text_gzip_5'] = 'Рівень 5';
$_['text_gzip_6'] = 'Рівень 6';
$_['text_gzip_7'] = 'Рівень 7';
$_['text_gzip_8'] = 'Рівень 8';
$_['text_gzip_9'] = 'Рівень 9';
$_['text_seo_0'] = 'Відключено';
$_['text_seo_1'] = 'SEO Pro';
$_['text_seo_2'] = 'SEO Url';
$_['text_blog_0'] = 'Відключено';
$_['text_blog_1'] = 'SEO CMS PRO';
$_['text_blog_2'] = 'PAV Blog';
$_['text_blog_3'] = 'Blog Manager';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Налагоджувальний режим:';
$_['entry_image_status'] = 'Виводити картинки:<br><br><small><i>Не рекомендується, не всі роботи розуміють</i></small>';
$_['entry_status_seo'] = 'ЛПУ товарів:';
$_['entry_filterpro_status_seo'] = 'SEO FilterPro:';
$_['entry_category_brand_status'] = 'Категорії з брендами:<br><br><small><i>Будуть згенеровані посилання виду категорія/бренд</i></small>';
$_['entry_status_addresses'] = 'Адресна інформація:';
$_['entry_partition_status'] = 'Розбити мапу на частини:';
$_['entry_partition_volume'] = 'Розмір частини мапи:';
$_['entry_status_multistore'] = 'Мультістор:';
$_['entry_status_gzip'] = 'Стискання:';
$_['entry_status_blog'] = 'Посилання для блогу:';
$_['entry_url'] = 'Посилання на sitemap.xml:';
$_['entry_use_url_date'] = 'Формувати дату зміни:';
$_['entry_use_url_frequency'] = 'Формувати частоту оновлення';
$_['entry_use_url_priority'] = 'Формувати пріоритет оновлення';

// Error
$_['error_permission'] = 'У Вас недостатньо прав для зміни "NeoSeo Резервні копії"!';





