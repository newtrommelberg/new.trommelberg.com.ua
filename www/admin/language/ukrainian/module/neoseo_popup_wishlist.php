<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Всплывающие избранные товары</p>';
$_['heading_title_raw'] = 'NeoSeo Всплывающие избранные товары';

$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_fields'] = 'Поля';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';


// Text
$_['text_payment'] = 'Оплата';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_order_date_created'] = 'Дата создания';
$_['text_order_date_modified'] = 'Дата изменения';
$_['text_order_date_current'] = 'Текущая дата';
$_['text_separate_column'] = 'В отдельной колонке';
$_['text_product_column'] = 'В колонке продукта';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';

$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';

// Entry
$_['entry_supplier_info'] = 'Поставщик:';
$_['entry_text'] = 'Дополнительный текст:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_debug_desc'] = 'В логи модуля будет писаться различная информация для разработчика модуля';
$_['entry_status'] = 'Статус:';
$_['entry_replace_status'] = 'Убрать старую кнопку:';
$_['entry_customer_info_format'] = 'Информация о клиенте:<br><br>Поддерживаются теги:<br><small>{country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}</small>';
$_['entry_payment_info_format'] = 'Информация об оплате:<br><br>Поддерживаются теги:<br><small>{method}, {country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}</small>';
$_['entry_shipping_info_format'] = 'Информация о доставке:<br><br>Поддерживаются теги:<br><small>{method}, {country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}</small>';
$_['entry_store_name'] = 'Название магазина:<br><small>Если не заполнено, будет взято из настроек магазина</small>';
$_['entry_store_url'] = 'URL магазина:<br><small>Если не заполнено, то выводиться не будет</small>';
$_['entry_store_phone'] = 'Телефон магазина:<br><small>Если не заполнен, будет взят из настроек магазина</small>';
$_['entry_store_email'] = 'Почта магазина:<br><small>Если не заполнена, будет взята из настроек магазина</small>';
$_['entry_store_logo'] = 'Логотип магазина:<br><small>Если не выбран, будет взят из настроек магазина</small>';
$_['entry_store_logo_width'] = 'Ширина логотипа магазина';
$_['entry_store_logo_height'] = 'Высота логотипа магазина';
$_['entry_order_date'] = 'Дата заказа:<br><small>Дата, которая будет выводиться в чеке</small>';
$_['entry_column_sku_status'] = 'Показывать артикул:<br><small>Показывать или нет артикулу в списке товаров</small>';
$_['entry_column_image_status'] = 'Показывать картинку:<br><small>Показывать или нет картинку товара</small>';
$_['entry_column_model_status'] = 'Показывать модель:<br><small>Показывать или нет модель в списке товаров</small>';
$_['entry_field_list_name'] = 'Шаблон';
$_['entry_field_list_desc'] = 'Описание';
$_['entry_show_comment'] = 'Показывать примечание к заказу';

$_['field_desc_order_id'] = 'Номер заказа';
$_['field_desc_invoice_no'] = 'Номер счета';
$_['field_desc_date_added'] = 'Дата создания заказа';
$_['field_desc_date_modified'] = 'Дата изменения заказа';
$_['field_desc_date_current'] = 'Текущая дата';
$_['field_desc_store_name'] = 'Название магазина';
$_['field_desc_store_url'] = 'Ссылка на магазин';
$_['field_desc_store_address'] = 'Адрес магазина';
$_['field_desc_store_email'] = 'Email магазина';
$_['field_desc_store_phone'] = 'Телефон магазина';
$_['field_desc_store_fax'] = 'Факс магазина';
$_['field_desc_store_owner'] = 'Владелец магазина';
$_['field_desc_text'] = 'Дополнительный текст из настроек модуля';
$_['field_desc_email'] = 'Email покупателя';
$_['field_desc_customer_info'] = 'Информация о покупателя согласно настройкам модуля';
$_['field_desc_firstname'] = 'Имя покупателя';
$_['field_desc_lastname'] = 'Фамилия покупателя';
$_['field_desc_telephone'] = 'Телефон покупателя';
$_['field_shipping_firstname'] = 'Имя получателя ';
$_['field_shipping_lastname'] = 'Фамилия получателя';
$_['field_desc_shipping_company'] = 'Компания получателя';
$_['field_desc_shipping_address_1'] = 'Адрес 1 получателя';
$_['field_desc_shipping_address_2'] = 'Адрес 2 получателя';
$_['field_desc_shipping_city'] = 'Город получателя';
$_['field_desc_shipping_postcode'] = 'Почтовый код получателя';
$_['field_desc_shipping_zone'] = 'Регион получателя';
$_['field_desc_shipping_zone_code'] = 'Код региона получателя';
$_['field_desc_shipping_country'] = 'Страна получателя';
$_['field_desc_shipping_info'] = 'Информация о доставке согласно настройкам модуля';
$_['field_desc_shipping_method'] = 'Название метода доставки';
$_['field_desc_payment_firstname'] = 'Имя плательщика';
$_['field_desc_payment_lastname'] = 'Фамилия плательщика';
$_['field_desc_payment_company'] = 'Компания плательщика';
$_['field_desc_payment_address_1'] = 'Адрес 1 плательщика';
$_['field_desc_payment_address_2'] = 'Адрес 2 плательщика';
$_['field_desc_payment_city'] = 'Город плательщика';
$_['field_desc_payment_postcode'] = 'Почтовый код плательщика';
$_['field_desc_payment_zone'] = 'Регион плательщика';
$_['field_desc_payment_zone_code'] = 'Код региона плательщика';
$_['field_desc_payment_country'] = 'Страна плательщика';
$_['field_desc_payment_info'] = 'Информация об оплате согласно настройкам модуля';
$_['field_desc_payment_method'] = 'Название метода оплаты';
$_['field_desc_product'] = 'Список товаров ( name, model, option, quantity, price, total)';
$_['field_desc_voucher'] = 'Список ваучеров ( description, amount )';
$_['field_desc_total'] = 'Итоги заказа ( code, title, text, value )';
$_['field_desc_total_str'] = 'Сумма прописью';
$_['field_desc_comment'] = 'Примечание к заказу';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_supplier_info'] = 'Это поле обязательно для заполнения!';



// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';




$_['text_module_version'] = '';