<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Коментарі';
$_['heading_title_raw'] = 'NeoSeo Блог Коментарі';

// Text
$_['text_module']      = 'Модулі';
$_['text_success']     = 'Модуль Блог Коментарі успішно змінений!';
$_['text_edit']        = 'Редагування модуля Блог Коментарі';
$_['text_latest']      = 'Останні коментарі';
$_['text_popular']     = 'Популярні коментарі';
$_['text_module_version'] = '';

// Entry
$_['entry_name']       = 'Назва модуля';
$_['entry_title']      = 'Заголовок модуля';
$_['entry_category']   = 'Категорія';
$_['entry_root_category'] = 'Обмежити категорією:';
$_['entry_type']       = 'Тип';
$_['entry_limit']      = 'Ліміт';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Висота';
$_['entry_status']     = 'Статус';
$_['entry_template']   = 'Шаблон';

// Error
$_['error_permission'] = 'У вас немає прав змінювати модуль Блог Статті!';
$_['error_name']       = 'Назва модуля повинна бути від 3 до 64 символів!';
$_['error_width']      = 'Ширина обов\'язкова!';
$_['error_height']     = 'Висота обов\'язкова!';