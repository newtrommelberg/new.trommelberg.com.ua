<?php

$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Генератор ЛПУ та метаданих</span>';
$_['heading_title_raw'] = 'NeoSeo Генератор ЛПУ та метаданих';

// Column
$_['column_status_name'] = 'Назва статусу';
$_['column_template_subject'] = 'Повідомлення';

// Text
$_['text_success'] = 'Налаштування модуля оновлено!';
$_['text_success_seogen'] = 'Генерація пройшла успішно!';
$_['text_module'] = 'Модулі';
$_['text_success_clear'] = 'Лог файл успішно очищений!';
$_['text_clear_log'] = 'Очистити журнал';
$_['text_select_template'] = 'Виберіть шаблон';
$_["text_new_order"] = 'Отримано нове замовлення';
$_['text_customer_templates'] = 'Повідомлення покупцям';
$_['text_admin_templates'] = 'Повідомлення адмінам';
$_['text_generate'] = 'Генерувати';

// Button
$_['button_clear_log'] = 'Очистити лог';
$_['button_save'] = 'Зберегти';
$_['button_save_and_close'] = 'Зберегти і закрити';
$_['button_close'] = 'Закрити';
$_['button_clear_log'] = 'Очистити журнал';
$_['button_recheck'] = 'Перевірити ще раз';
$_['button_insert'] = 'Створити';
$_['button_delete'] = 'Видалити';

// Entry
$_['entry_status'] = 'Статус:<br><span class="help">Генерувати дані при збереженні/редагуванні товарів, категорій та ін</span>';
$_['entry_rewrite'] = 'Запис Мета:<br><span class="help">Перезаписувати мета-поля при збереженні/редагуванні товарів, категорій та ін</span>';
$_['entry_debug'] = 'Налагодження:';
$_['entry_limit_title'] = 'Ліміт довжини title:';
$_['entry_limit_description'] = 'Ліміт довжини description:';
$_['entry_option_name_value_separator'] = 'Роздільник між ім`ям та значенням опції :';
$_['entry_option_values_separator'] = 'Роздільник між значеннями опцій:';
$_['entry_options_separator'] = 'Роздільник між опциями:';
$_['entry_attribute_name_value_separator'] = 'Роздільник між ім`ям та значенням атрибута:';
$_['entry_attribute_values_separator'] = 'Роздільник між значеннями атрибута:';
$_['entry_attributes_separator'] = 'Роздільник між атрибутами:';
$_['entry_language'] = 'Мова для формування ЛПУ:';
$_['entry_category'] = 'Категорії:<br><span class="help">Категорії не обрані = вибрані всі Категорії</span>';
$_['entry_manufacturer'] = 'Виробники:<br><span class="help">Виробники не обрані = вибрані всі Виробники</span>';

$_['entry_products_seo_url'] = 'SEO URL:<br><span class="help">Доступні теги: [product_id], [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name]</span>';
$_['entry_products_h1'] = 'HTML-тег h1:<br><span class="help">Доступні теги: [product_id], [product_h1],  [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name]</span>';
$_['entry_products_title'] = 'HTML-тег title:<br><span class="help">Доступні теги: [product_id], [product_name], [product_title], [sku], [price], [model_name], [manufacturer_name], [category_name]</span>';
$_['entry_products_description'] = 'HTML-тег description:<br><span class="help">Доступні теги: [product_id], [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name], [product_options], [product_attributes], [product_description]</span>';
$_['entry_products_product_description'] = 'Опис продукту:<br><span class="help"> Доступні теги: [product_id], [product_name], [№], [price], [model_name], [manufacturer_name], [category_name], [product_options], [product_attributes], [product_description]</span>';
$_['entry_products_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступні теги: [product_id], [product_name], [product_keyword], [sku], [price], [model_name], [manufacturer_name], [category_name], [product_options], [product_attributes]</span>';

$_['entry_categories_seo_url'] = 'SEO URL:<br><span class="help">Доступні теги: [category_name],[parent_category_name]</span>';
$_['entry_categories_h1'] = 'HTML-тег h1:<br><span class="help">Доступні теги: [category_name], [category_h1],[parent_category_name]</span>';
$_['entry_categories_title'] = 'HTML-тег title:<br><span class="help">Доступні теги: [category_name], [category_title],[parent_category_name]</span>';
$_['entry_categories_description'] = 'HTML-тег description:<br><span class="help">Доступні теги: [category_name], [category_description],[parent_category_name]</span>';
$_['entry_categories_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступні теги: [category_name], [category_keyword],[parent_category_name]</span>';

$_['entry_manufacturers_seo_url'] = 'SEO URL:<br><span class="help">Доступні теги: [manufacturer_name]</span>';
$_['entry_manufacturers_h1'] = 'HTML-тег h1:<br><span class="help">Доступні теги: [manufacturer_name], [manufacturer_h1]</span>';
$_['entry_manufacturers_title'] = 'HTML-тег title:<br><span class="help">Доступні теги: [manufacturer_name], [manufacturer_title]</span>';
$_['entry_manufacturers_description'] = 'HTML-тег description:<br><span class="help">Доступні теги: [manufacturer_name], [manufacturer_description]</span>';
$_['entry_manufacturers_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступні теги: [manufacturer_name], [manufacturer_keyword]</span>';
$_['entry_articles_seo_url'] = 'SEO URL:<br><span class="help">Доступні теги: [information_title]</span>';

$_['entry_articles_h1'] = 'HTML-тег h1:<br><span class="help">Доступні теги: [information_title], [information_h1]</span>';
$_['entry_articles_title'] = 'HTML-тег title:<br><span class="help">Доступні теги: [information_title]</span>';
$_['entry_articles_description'] = 'HTML-тег description:<br><span class="help">Доступні теги: [information_title], [information_description]</span>';
$_['entry_articles_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступні теги: [information_title], [information_keyword]</span>';

$_['entry_blogs_articles_seo_url'] = 'SEO URL:<br><span class="help">Доступні теги: [blog_article_name]</span>';
$_['entry_blogs_articles_h1'] = 'HTML-тег h1:<br><span class="help">Доступні теги: [blog_article_name], [blog_article_h1]</span>';
$_['entry_blogs_articles_title'] = 'HTML-тег title:<br><span class="help">Доступні теги: [blog_article_name], [blog_article_title]</span>';
$_['entry_blogs_articles_description'] = 'HTML-тег description:<br><span class="help">Доступні теги: [blog_article_name], [blog_article_description]</span>';
$_['entry_blogs_articles_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступні теги: [blog_article_name], [blog_article_keyword]</span>';

$_['entry_blogs_authors_seo_url'] = 'SEO URL:<br><span class="help">Доступні теги: [blog_author_name]</span>';
$_['entry_blogs_authors_h1'] = 'HTML-тег h1:<br><span class="help">Доступні теги: [blog_author_name], [blog_author_h1]</span>';
$_['entry_blogs_authors_title'] = 'HTML-тег title:<br><span class="help">Доступні теги: [blog_author_name], [blog_author_title]</span>';
$_['entry_blogs_authors_description'] = 'HTML-тег description:<br><span class="help">Доступні теги: [blog_author_name], [blog_author_description]</span>';
$_['entry_blogs_authors_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступні теги: [blog_author_name], [blog_author_keyword]</span>';

$_['entry_blogs_categories_seo_url'] = 'SEO URL:<br><span class="help">Доступні теги: [blog_category_name]</span>';
$_['entry_blogs_categories_h1'] = 'HTML-тег h1:<br><span class="help">Доступні теги: [blog_category_name], [blog_category_h1]</span>';
$_['entry_blogs_categories_title'] = 'HTML-тег title:<br><span class="help">Доступні теги: [blog_category_name], [blog_category_title]</span>';
$_['entry_blogs_categories_description'] = 'HTML-тег description:<br><span class="help">Доступні теги: [blog_category_name], [blog_category_description]</span>';
$_['entry_blogs_categories_keywords'] = 'HTML-тег keywords:<br><span class="help">Доступні теги: [blog_category_name], [blog_category_keyword]</span>';

$_['entry_filter_page_manufacturer'] = 'Найменування для виробника:<br><span class="help">Вкажіть найменування для виробника, якщо він використовується в посадочної</span>';
$_['entry_filter_page_h1'] = 'H1 :<br><span class="help">Доступні теги: [filter_page_name], [filter_page_h1], [filter_page_title], [filter_page_category], [filter_page_filters]</span>';
$_['entry_filter_page_title'] = 'Document Title :<br><span class="help">Доступні теги: [filter_page_h1], [filter_page_title], [filter_page_category], [filter_page_filters]</span>';
$_['entry_filter_page_description'] = 'Meta Description:<br><span class="help">Доступні теги: [filter_page_h1], [filter_page_description], [filter_page_category], [filter_page_filters]</span>';
$_['entry_filter_page_keywords'] = 'Meta Keywords :<br><span class="help">Доступні теги: [filter_page_h1], [filter_page_keyword], [filter_page_category], [filter_page_filters]</span>';
$_['entry_instruction'] = 'Інструкція до модуля:';
$_['entry_history'] = 'Історія змін:';
$_['entry_faq'] = 'Найчастіші запитання:';

$_['entry_cron'] = 'CRON';

// Tab
$_['tab_general'] = 'Загальне';
$_['tab_magazine'] = 'Магазин';
$_['tab_blogs'] = 'Блоги';
$_['tab_filters'] = 'Фільтри';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Підтримка';
$_['tab_license'] = 'Ліцензія';
$_['tab_usefull'] = 'Корисні посилання';

// Sub tabs
$_['subtab_magazine_categories'] = 'Категории';
$_['subtab_magazine_products'] = 'Товары';
$_['subtab_magazine_manufacturers'] = 'Производители';
$_['subtab_magazine_articles'] = 'Статьи';

$_['subtab_blogs_categories'] = 'Категории';
$_['subtab_blogs_authors'] = 'Авторы';
$_['subtab_blogs_articles'] = 'Статьи';

$_['subtab_filter_pages'] = 'NeoSeo Посадочні сторінки';

// Params
$_['params_products_seo_url'] = '[product_name]';
$_['params_products_h1'] = '[product_h1]';
$_['params_products_title'] = '[product_title]';
$_['params_products_keywords'] = '[product_keyword]';
$_['params_products_description'] = '[product_description]';
$_['params_products_product_description'] = '[category_name] [product_name] [model_name] [manufacturer_name]';

$_['params_categories_seo_url'] = '[category_name]';
$_['params_categories_h1'] = '[category_h1]';
$_['params_categories_title'] = '[category_title]';
$_['params_categories_description'] = '[category_description]';
$_['params_categories_keywords'] = '[category_keyword]';
$_['params_categories_parent_name'] = '[parent_category_name]';

$_['params_manufacturers_seo_url'] = '[manufacturer_name]';
$_['params_manufacturers_h1'] = '[manufacturer_h1]';
$_['params_manufacturers_title'] = '[manufacturer_title]';
$_['params_manufacturers_description'] = '[manufacturer_description]';
$_['params_manufacturers_keywords'] = '[manufacturer_keyword]';

$_['params_articles_seo_url'] = '[information_name]';
$_['params_articles_h1'] = '[information_h1]';
$_['params_articles_title'] = '[information_title]';
$_['params_articles_description'] = '[information_description]';
$_['params_articles_keywords'] = '[information_keyword]';

$_['params_blogs_articles_seo_url'] = '[blog_article_name]';
$_['params_blogs_articles_h1'] = '[blog_article_h1]';
$_['params_blogs_articles_title'] = '[blog_article_title]';
$_['params_blogs_articles_description'] = '[blog_article_description]';
$_['params_blogs_articles_keywords'] = '[blog_article_keyword]';

$_['params_blogs_authors_seo_url'] = '[blog_author_name]';
$_['params_blogs_authors_h1'] = '[blog_author_h1]';
$_['params_blogs_authors_title'] = '[blog_author_title]';
$_['params_blogs_authors_description'] = '[blog_author_description]';
$_['params_blogs_authors_keywords'] = '[blog_author_keyword]';

$_['params_blogs_categories_seo_url'] = '[blog_category_name]';
$_['params_blogs_categories_h1'] = '[blog_category_h1]';
$_['params_blogs_categories_title'] = '[blog_category_title]';
$_['params_blogs_categories_description'] = '[blog_category_description]';
$_['params_blogs_categories_keywords'] = '[blog_category_keyword]';

$_['params_filter_page_manufacturer'] = 'Виробник';
$_['params_filter_page_title'] = '[filter_page_h1]';
$_['params_filter_page_title'] = '[filter_page_title]';
$_['params_filter_page_description'] = '[filter_page_description]';
$_['params_filter_page_keywords'] = '[filter_page_keyword]';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';





$_['text_module_version'] = '';

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-generator-chpu-i-metadannyh">https://neoseo.com.ua/nastroyka-modulya-generator-chpu-i-metadannyh</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/generator-chpu-i-metadannyh#module_history">https://neoseo.com.ua/generator-chpu-i-metadannyh#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/generator-chpu-i-metadannyh#faqBox">https://neoseo.com.ua/generator-chpu-i-metadannyh#faqBox</a>';