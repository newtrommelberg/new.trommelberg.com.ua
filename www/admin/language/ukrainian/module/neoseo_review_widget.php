<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Виджет отзывы о товарах на панели управления</span>';
$_['heading_title_raw'] = 'NeoSeo Віджет відгуків про товари';

//Tabs
$_['tab_general'] = 'Параметри';
$_['tab_header'] = 'Шапка';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Підтримка';
$_['tab_license'] = 'Ліцензія';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Налаштування модуля оновлено!';
$_['text_module'] = 'Модулі';
$_['text_success_clear'] = 'Лог файл успішно очищено!';
$_['text_clear_log'] = 'Очистити лог';
$_['text_clear'] = 'Очистити';
$_['text_title'] = 'Відгуки про товари';

//Buttons
$_['button_save'] = 'Зберегти';
$_['button_save_and_close'] = 'Зберегти і Закрити';
$_['button_close'] = 'Закрити';
$_['button_recheck'] = 'Перевірити ще раз';
$_['button_clear_log'] = 'Очистити лог';
$_['button_download_log'] = 'Скачати файл логів';

// Entry
$_['entry_debug'] = 'Налагоджувальний режим<br /><span class="help">У логи модуля буде записуватися різна інформація для розробника модуля.</span>';
$_['entry_status'] = 'Статус';
$_['entry_show_review_number'] = 'Виводити номер відгуку';
$_['entry_show_author'] = 'Виводити автора';
$_['entry_show_text'] = 'Виводити текст відгуку';
$_['entry_show_product'] = 'Виводити товар';
$_['entry_show_product_image'] = 'Виводити зображення товару';
$_['entry_show_rating'] = 'Виводити рейтинг відгуку';
$_['entry_show_status'] = 'Виводити статус відгуку';
$_['entry_show_date_added'] = 'Виводити дату створення відгуку';
$_['entry_limit'] = 'Кількість виведених записів';
$_['entry_title'] = 'Заголовок блоку';
$_['entry_height_image'] = 'Висота зображення товару';
$_['entry_width_image'] = 'Ширина зображення товару';

// Error
$_['error_permission'] = 'У Вас немає прав для управління цим модулем!';
$_['error_download_logs'] = 'Файл логів пустий або відсутній!';



