<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Категорія';
$_['heading_title_raw'] = 'NeoSeo Блог Категорія';

// Text
$_['text_module']      = 'Модулі';
$_['text_success']     = 'Модуль Блог Категорія успішно змінені!';
$_['text_edit']        = 'Редагування модуля Категорія Блогу';
$_['text_module_version'] = '';

// Entry
$_['entry_name']       = 'Назва модуля';
$_['entry_title']      = 'Заголовок модуля';
$_['entry_root_category']     = 'Коренева категорія';
$_['entry_status']     = 'Статус';
$_['entry_template']   = 'Шаблон';

// Error
$_['error_permission'] = 'У вас немає прав змінювати модуль Блог Категорія!';