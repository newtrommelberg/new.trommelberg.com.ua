<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Автоматическое создание тысяч посадочных страниц для SEO-фильтра</span>';
$_['heading_title_raw'] = 'NeoSeo Автоматическое создание тысяч посадочных страниц для SEO-фильтра';

//Tabs
$_['tab_general'] = 'Основные настройки';
$_['tab_generate_page'] = 'Генерация посадочных страниц';
$_['tab_generate_rule'] = 'Генерация правил';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';
$_['tab_patterns'] = 'Описание шаблонов';
$_['tab_usefull'] = 'Полезные ссылки';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_clear'] = 'Очистить';
$_['text_success_generate'] = 'Генерация правил для генератора посадочных страниц завершено!';
$_['text_pattern_name_default'] = '[page_category], [page_filters], [page_options], [page_option_values]';
$_['text_pattern_title_default'] = '[page_category], [page_filters], [page_options], [page_option_values]';
$_['text_pattern_meta_description_default'] = '[page_category], [page_filters], [page_options], [page_option_values]';
$_['text_pattern_url_default'] = '[page_title]';
$_['text_pattern_h1_default'] = '[page_title]';
$_['text_manufacturer_default'] = 'Производитель';
$_['text_pattern_description_default'] = '{h1}';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';
$_['button_generate_rules'] = 'Сгенерировать';

// Entry
$_['entry_debug'] = 'Отладочный режим<br /><span class="help">В логи модуля будет писаться различная информация для разработчика модуля.</span>';
$_['entry_status'] = 'Статус';
$_['entry_ip_list'] = 'Разрешенные IP адреса';
$_['entry_ip_list_desc'] = 'Список IP адресов, с которых разрешена генерация. Каждый адрес с новой строки. Пустой список - без ограничений.';
$_['entry_limit_pagination'] = 'Лимит записей на странице';
$_['entry_cron'] = 'Планировщик';

$_['entry_page_status_default'] = 'Статус посадочной страницы по умолчанию';
$_['entry_limit_url'] = 'Длина ЧПУ посадочной страницы';
$_['entry_limit_records'] = 'Количество записей для одной итерации';
$_['entry_limit_records_desc'] = 'Используется при ручной генерации посадочных страниц для одного правила';
$_['entry_pattern_manufacturer'] = 'Наименование для производителя';
$_['entry_pattern_manufacturer_desc'] = 'Укажите наименование для производителя, если он используется в посадочной';
$_['entry_pattern_name_default'] = 'Шаблон названия посадочной страницы по умолчанию';
$_['entry_pattern_name_default_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values]';
$_['entry_pattern_title_default'] = 'Шаблон Title посадочной страницы по умолчанию';
$_['entry_pattern_title_default_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values], [page_name]';
$_['entry_pattern_meta_description_default'] = 'Шаблон Meta Description посадочной страницы по умолчанию';
$_['entry_pattern_meta_description_default_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values], [page_name]';
$_['entry_pattern_h1_default'] = 'Шаблон H1 посадочной страницы по умолчанию';
$_['entry_pattern_h1_default_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values], [page_name], [page_title]';
$_['entry_pattern_url_default'] = 'Шаблон URL посадочной страницы по умолчанию';
$_['entry_pattern_url_default_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values], [page_name], [page_title], [page_h1]';
$_['entry_separator_page_options'] = 'Разделитель опций фильтра';
$_['entry_separator_page_options_desc'] = 'Применяется при использовании шаблона [page_options]. <br> По умолчанию ",". <br> Например: цвет, размер';
$_['entry_separator_page_option_values'] = 'Разделитель значений опций фильтра';
$_['entry_separator_page_option_values_desc'] = 'Применяется при использовании шаблона [page_option_values]. <br> По умолчанию ",". <br> Например: синий, зеленный, XS';
$_['entry_separator_page_filters'] = 'Разделитель опция фильтра+значение опции фильтра';
$_['entry_separator_page_filters_desc'] = 'Применяется при использовании шаблона [page_filters]. По умолчанию ":". <br> Например: цвет: синий, зеленный';
$_['entry_separator_page_option_option_values'] = 'Разделитель набора опция фильтра + значение опции фильтра';
$_['entry_separator_page_option_option_values_desc'] = 'Применяется при использовании шаблона [page_filters]. <br> По умолчанию ";". <br> Например: Цвет: синий, зеленный; Размер: XS';

$_['entry_use_direct_link_default'] = 'Ипользовать пряме ссылки по умолчанию';
$_['entry_use_direct_link_default_desc'] = 'Будут ли посадочные страницы доступны по прямым ссылкам "/filter-page-url" или вместе с категорией "/category/filter-page-url"';
$_['entry_pattern_description_default'] = 'Шаблон описания посадочной страницы по умолчанию';
$_['entry_pattern_description_default_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values],[page_name],[page_title],[page_h1] {h1}, чтобы добавлять в описание содержимое поля h1';
$_['entry_use_end_slash_default'] = 'Использовать слэш в конце УРЛ Посадочной страницы';
$_['entry_use_end_slash_default_desc'] = 'При включении данной опции к УРЛ посадочной страницы будет добавлен / в конце. Например https://mysite.com/filterpage/ ';

$_['entry_generate_rules'] = 'Создать набор правил';
$_['entry_limit_categories'] = 'Количество категорий для одной итерации';
$_['entry_limit_categories_desc'] = 'Используется при ручной генерации набора правил';
$_['entry_generate_rules_desc'] = 'Создать набор правил для генерации посадочных страниц на основе существующих опций фильтра';

$_['entry_pattern_list_name'] = 'Шаблон';
$_['entry_pattern_list_desc'] = 'Описание шаблона';

$_['entry_instruction'] = 'Инструкция к модулю:';
$_['entry_history'] = 'История изменений:';
$_['entry_faq'] = 'Часто задаваемые вопросы:';

//fields_desc
$_['pattern_desc_page_category'] = 'Категория посадочной страницы';
$_['pattern_desc_page_filters'] = 'Набор опций фильтров со значениями опций фильтра';
$_['pattern_desc_page_options'] = 'Список опций фильтра';
$_['pattern_desc_page_option_values'] = 'Список значений опций фильтра';
$_['pattern_desc_page_name'] = 'Название посадочной страницы';
$_['pattern_desc_page_title'] = 'Title посадочной страницы';
$_['pattern_desc_page_page_h1'] = 'H1 посадочной страницы';


// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_no_filter_tables'] = 'Для генерации посадочных страниц модуль "SEO-Фильтр от веб-студии NeoSeo" должен быть установлен в системе! ';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-generator-posadochnyh-stranic">https://neoseo.com.ua/nastroyka-modulya-generator-posadochnyh-stranic</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/index.php?route=product/product&product_id=318#module_history">https://neoseo.com.ua/generator-posadochnyh-stranic-seo-filtra-2-1-2-3#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/index.php?route=product/product&product_id=318#faqBox">https://neoseo.com.ua/generator-posadochnyh-stranic-seo-filtra-2-1-2-3#faqBox</a>';
$_['text_module_version']='7';
$_['error_license_missing']='<h3 style = "color: red"> Відсутній файл з ключем! </h3>

<p> Для отримання файлу з ключем зв`яжіться з NeoSeo по email <a href="mailto:license@neoseo.com.ua"> license@neoseo.com.ua </a>, при цьому вкажіть: </p>
<ul>
	<li> назву сайту, на якому ви купили модуль, наприклад, https://neoseo.com.ua </li>
	<li> назву модуля, який ви купили, наприклад: NeoSeo Обмін з 1С:Підприємство </li>
	<li> ваше ім`я користувача (нік-нейм) на цьому сайті, наприклад, NeoSeo </li>
	<li> номер замовлення на цьому сайті, наприклад, 355446 </li>
	<li> основний домен сайту для якого буде активований файл з ключем, наприклад, https://neoseo.ua </li>
</ul>

<p> Отриманий файл з ключем покладіть у корінь сайту, тобто поруч з файлом robots.txt і натисніть кнопку "Перевірити ще раз".</p>';
$_['error_ioncube_missing']='<h3 style="color: red">Відсутній IonCube Loader!</h3>

<p>Щоб користуватися нашим модулем, вам потрібно встановити IonCube Loader.</p>

<p>Для установки зверніться до ТП Вашого хостингу</p>

<p>Якщо ви не можете встановити IonCube Loader самостійно, у вас є можливість попросити допомоги у наших фахівців за адресою <a href="mailto:info@neoseo.com.ua"> info@neoseo.com.ua </a></p>';
$_['module_licence']='<h2>Ліцензійні умови згоди на використання програмного забезпечення NeoSeo</h2>
<p>Дякуємо вам за покупку програмних продуктів нашої веб-студії.</p>
<p>Нижче наведені юридичні умови, які поширюються на всіх, хто відвідує наш сайт і користується нашими програмними продуктами або послугами. Дані умови спрямовані на те, щоб захистити ваші інтереси та інтереси ТОВ "НЕОСЕО" і його афілійованих структур і осіб (далі, "ми", "NeoSeo"), які виступають в договорах від його імені.</p>
<p><strong>1. Введення</strong></p>
<p>Ці Умови використання NeoSeo ( "Умови використання") поряд з додатковими умовами, які стосуються ряду конкретних послуг або програмних продуктів, розробок і представлені на веб-сайті (-ів) NeoSeo, містять умови і положення, дія яких поширюється на кожного відвідувача або користувача ( "Користувач", "ви" або "Покупець") веб-сайту NeoSeo, додатків, доповнень і компонентів, запропонованих нами поряд з наданням послуг і веб-сайту, якщо не вказано інше (всі послуги та програмне забезпечення, програмні модулі запропоновані через веб-сайт NeoSeo або допоміжні сервіси, веб-сервіси, ін. додатки від імені NeoSeo спільно іменуються - "Послуги NeoSeo" або "Послуги").</p>
<p>Умови NeoSeo є обов`язковим контрактом між NeoSeo і вами - тому просимо вас уважно ознайомитися з ними.</p>
<p>Ви можете відвідувати і / або використовувати Послуги NeoSeo тільки в тому випадку, якщо повністю згодні з Умовами NeoSeo: використовуючи будь-яку з Послуг NeoSeo і / або підписуючись на неї, ви висловлюєте і підтверджуєте свою згоду з даними Умовами користування та іншими Умовами NeoSeo, наприклад, умови надання послуг програмування в розрізі типових і нетипових завдань, які виписані тут:   <a href="https://neoseo.com.ua/vse-chto-nujno-znat-klienty" target="_blank" class="external">https://neoseo.com.ua/vse-chto-nujno-znat-klienty</a> , (далі Умови NeoSeo).</p>
<p>Якщо ви не можете прочитати Умови NeoSeo, не знаєте їх або не згодні з ними, ви повинні покинути Веб-сайт NeoSeo і не користуватися Послугами NeoSeo.</p>
<p>Використовуючи наші Програмні продукти, Сервіси і Послуги, ви підтверджуєте, що ознайомилися з нашою Політикою конфіденційності, доступною до перегляду за адресою <a href="https://neoseo.com.ua/politika-konfidencialnosti" target="_blank" class="external">https://neoseo.com.ua/politika-konfidencialnosti</a>   ("Політика конфіденційності").</p>
<p>Цей документ є ліцензійною угодою між вами і NeoSeo.</p>
<p>Погоджуючись на цю угоду або використовуючи програмне забезпечення, ви погоджуєтеся з усіма цими умовами.</p>
<p>Ця угода відноситься до програмного забезпечення NeoSeo, будь-яких шрифтів, піктограм, зображень або звукових файлів, що надаються у складі програмного забезпечення, а також до всіх оновлень, доповнень або служб NeoSeo для програмного забезпечення, якщо в їх відношенні не застосовуються інші умови. Це також стосується розроблених в NeoSeo додатків і доповнень для SEO-Магазину, які забезпечують розширення його функціональних можливостей.</p>
<p>До використання вами деяких функцій додатків можуть застосовуватися додаткові умови NeoSeo і третіх осіб. Для коректної роботи деяких додатків необхідні додаткові угоди з окремими умовами та політиками конфіденційності, наприклад з сервісами, які надають послуги смс-нотифікації.</p>
<p>Програмне забезпечення не продається, а надається за ліцензією.</p>
<p>NeoSeo зберігає за собою всі права (наприклад, права, передбачені законами про захист інтелектуальної власності), які не надаються явно в рамках цієї угоди. Наприклад, ця ліцензія не надає вам права:</p>
<li><span></span><span></span>окремо використовувати або віртуалізувати компоненти програмного забезпечення;</li>
<li>публікувати або копіювати (за винятком дозволеної резервної копії) програмне забезпечення, надавати програмне забезпечення у прокат, в оренду або в тимчасове користування;</li>
<li>передавати програмне забезпечення (за винятком випадків, передбачених цією угодою);</li>
<li>намагатися обійти технічні обмеження програмного продукту;</li>
<li>вивчати технологію, декомпілювати або деасемблувати програмне забезпечення, а також вживати відповідні спроби, крім тих що в мірі та у випадках, коли це (а) передбачено відповідним правом; (б) дозволено умовами ліцензії на використання компонентів з відкритим вихідним кодом, які можуть входити до складу цього програмного забезпечення; (c) необхідно для налагодження змін будь-яких бібліотек, що ліцензуються по малій стандартній громадській ліцензії GNU, які входять до складу програмного забезпечення та пов`язані з ним;</li>
<p>Ви маєте право використовувати дане програмне забезпечення, тільки якщо у вас є відповідна ліцензія і програмне забезпечення було належним чином активовано з використанням справжнього ключа продукту або в інший дозволений спосіб.</p>
<p>У вартість ліцензії SEO-Магазину не включені послуги установки, настройки і тим більше його стилізації, також як і інші платні / безкоштовні доповнення до нього. Дані послуги є додатковими, вартість залежить від кількості необхідних для реалізації годин, детальніше тут: <a href="https://neoseo.com.ua/vse-chto-nujno-znat-klienty" target="_blank" class="external">https://neoseo.com.ua/vse-chto-nujno-znat-klienty</a></p>
<p>З повною версією документа можна ознайомитися тут:</p>
<p><a href="https://neoseo.com.ua/usloviya-licenzionnogo-soglasheniya" target="_blank" class="external">https://neoseo.com.ua/usloviya-licenzionnogo-soglasheniya</a>
</p>';
$_['mail_support']='<h2>Умови безкоштовної та платної інформаційної та технічної підтримки в <a class="external" href="https://neoseo.com.ua/" target="_blank">NeoSeo</a>.</h2>

<p>Оскільки ми впевнені у тому, що будь-яка Якісна робота повинна бути оплачуваною, всі консультаційні відповіді, що вимагають попередньої підготовки - платні, в т.ч. і ситуації у стилі: &quot;подивіться, а чому тут ВАШ модуль не працює?&quot;</p>

<p>Якщо відповідь на Ваше питання у нас вже готова, Ви її отримаєте безкоштовно. Але якщо для відповіді потрібно витратити час, щоб розібратися в питанні, вивчити файли, знайти помилку і проаналізувати, перш ніж отримати відповідь, попросимо здійснити оплату.</p>

<p>Ми допомагаємо безкоштовно <strong>по встановленні</strong> і <strong>усуненні помилок при встановленні</strong> куплених у нас модулів у порядку загальної черги.</p>

<p>За будь-якими питаннями звертайтесь на www.opencartmasters.com.</p>

<p>З повною версією ліцензійної угоди ознайомтесь тут: <a class="external" href="https://neoseo.com.ua/usloviya-licenzionnogo-soglasheniya" target="_blank">https://neoseo.com.ua/usloviya-licenzionnogo-soglasheniya</a></p>

<p><strong>Акція: напишіть відгук - отримайте модуль в подарунок :)</strong></p>

<p>Шановні Клієнти веб-студії NeoSeo,</p>

<p>скажіть, що може бути краще для розвитку компанії, ніж публічні відгуки? Це чудовий спосіб почути свого Клієнта і зробити свої продукти та сервіс ще краще.</p>

<p>Будь ласка, залиште відгук про співпрацю з нашою веб-студією або про роботу наших програмних рішень (додатків) на наших сторінках в Facebook, Google, Яндекс і на OpenCartForum.com.</p>

<p>Пишіть як є, нам важливо почути чесну та об\'єктивну оцінку, а в знак подяки за витрачений на написання відгуків час, ми підготували приємний бонус. Детальніше про умови акції тут: <a href="https://neoseo.com.ua/akciya-modul-v-podarok " target="_blank">https://neoseo.com.ua/akciya-modul-v-podarok </a></p>

<p>Ще раз, велике спасибі за те, що ви з нами!</p>

<p>Команда NeoSeo</p>';
