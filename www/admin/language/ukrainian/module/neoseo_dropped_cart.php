<?php

// Column
$_['column_name'] = 'Назва';
$_['column_quantity'] = 'Кількість';
$_['column_price'] = 'Ціна';

// Text
$_['text_top'] = 'Ви залишили свій кошик №%d від %s';
$_['text_total'] = 'Підсумок:';
$_['text_bottom'] = 'Перейдіть за посиланням для оформлення замовлення чи продовження покупок.';

// Error
$_['error_missing_cart_id'] = 'Отсутствует ID корзины.';
$_['error_cart_not_found'] = 'Корзина не найдена.';
$_['error_template_not_found'] = 'Шаблон письма не найден.';
$_['error_empty_email'] = 'Пустой email';
