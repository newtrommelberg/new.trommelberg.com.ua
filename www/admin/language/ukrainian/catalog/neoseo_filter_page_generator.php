<?php

// Heading
$_['heading_title'] = 'NeoSeo Автоматическое создание тысяч посадочных страниц для SEO-фильтра';

// Text
$_['text_success'] = 'Список правил генератора обновлен!';
$_['text_list'] = 'NeoSeo Автоматическое создание тысяч посадочных страниц для SEO-фильтра';
$_['text_add'] = 'Добавление правила генератора';
$_['text_edit'] = 'Редактирование правила генератора';
$_['text_action'] = 'Введите название';
$_['text_module_version'] = '';
$_['text_select_category'] = 'Выберите категорию';
$_['text_empty_option'] = 'Для выбранной категории отсутствуют опции';
$_['text_manufacturer'] = 'Производитель';
$_['text_list_rules'] = 'Список правил генератора';

// Column
$_['column_category'] = 'Категория';
$_['column_status'] = 'Статус';
$_['column_action'] = 'Действие';
$_['column_options'] = 'Опции фильтра категории';
$_['column_cron'] = 'Ссылка для планировщика';
$_['column_generate'] = 'Ручная генерация';

// Entry
$_['entry_status'] = 'Статус';
$_['entry_category'] = 'Категория';
$_['entry_category_id'] = 'Категория';
$_['entry_options'] = 'Опции фильтра категории';
$_['entry_pattern_name'] = 'Шаблон названия посадочной страницы';
$_['entry_pattern_name_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values]';
$_['entry_pattern_title'] = 'Шаблон Title посадочной страницы';
$_['entry_pattern_title_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values], [page_name]';
$_['entry_pattern_meta_description'] = 'Шаблон Meta Description посадочной страницы';
$_['entry_pattern_meta_description_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values], [page_name]';
$_['entry_pattern_h1'] = 'Шаблон H1 посадочной страницы';
$_['entry_pattern_h1_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values], [page_name], [page_title]';
$_['entry_pattern_url'] = 'Шаблон URL посадочной страницы';
$_['entry_pattern_url_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values], [page_name], [page_title], [page_h1]';
$_['entry_use_direct_link'] = 'Ипользовать пряме ссылки';
$_['entry_use_direct_link_desc'] = 'Будут ли посадочные страницы доступны по прямым ссылкам "/filter-page-url" или вместе с категорией "/category/filter-page-url"';
$_['entry_pattern_description'] = 'Шаблон описания посадочной страницы';
$_['entry_pattern_description_desc'] = 'Доступные шаблоны: [page_category], [page_filters], [page_options], [page_option_values],[page_name],[page_title],[page_h1] {h1}, чтобы добавлять в описание содержимое поля h1';
$_['entry_use_end_slash'] = 'Использовать слэш в конце УРЛ Посадочной страницы';
$_['entry_use_end_slash_desc'] = 'При включении данной опции к УРЛ посадочной страницы будет добавлен / в конце. Например https://mysite.com/filterpage/ ';

//Buttons
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_generate'] = 'Сгенерировать посадочные';
$_['button_select_rule_generate'] = 'Сгенерировать вручную посадочные для выбранных правил';
$_['button_all_rule_generate'] = 'Сгенерировать вручную посадочные для всех правил';

// Error
$_['error_permission'] = 'У Вас нет прав для изменения списка правил генератора!';
