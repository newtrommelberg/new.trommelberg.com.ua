<?php

// Heading
$_['heading_title'] = '<img width="36" height="36" src="view/image/neoseo.png" style="float: left;">NeoSeo Отзывы о магазине';
$_['text_neoseo_testimonials'] = 'Отзывы о магазине';
$_['heading_title_raw'] = 'NeoSeo Отзывы о магазине';
$_['text_home'] = 'Главная';

// Text
$_['text_success'] = 'Настройки успешно изменены!';
$_['text_list'] = 'Отзывы пользователей';
$_['text_add'] = 'Добавить';
$_['text_edit'] = 'Редактирование';
$_['text_enabled'] = 'Опубликовать';
$_['text_disabled'] = 'Скрыть';
$_['text_user_image'] = 'Пользовательское изображение:';

//Button
$_['button_filter'] = 'Фильтр';
$_['button_add'] = 'Добавить';
$_['button_edit'] = 'Редактировать';

// Column
$_['column_description'] = 'Сообщение';
$_['column_admin_text'] = 'Ответ';
$_['column_name'] = 'Автор';
$_['column_rating'] = 'Рейтинг';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата';
$_['column_action'] = 'Действие';
$_['column_store'] = 'Магазин';

// Entry
$_['entry_name'] = 'Автор';
$_['entry_rating'] = 'Рейтинг';
$_['entry_status'] = 'Статус';
$_['entry_youtube'] = 'Ссылка на видео отзыв YouTube';
$_['entry_description'] = 'Сообщение';
$_['entry_admin_text'] = 'Ответ администратора';
$_['entry_date_added'] = 'Дата';
$_['entry_date_admin_added'] = 'Дата ответа администратора';
$_['entry_user_image'] = 'Пользовательское изображение';
$_['entry_store'] = 'Магазин';

// Error
$_['error_permission'] = 'У Вас нет прав для изменения отзывов!';
$_['error_text'] = 'Текст отзыва должен содержать хотя бы 1 символ!';
$_['error_rating'] = 'Требуется установить рейтинг!';

