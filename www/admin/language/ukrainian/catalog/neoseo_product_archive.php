<?php

// Text category
$_['text_archive'] = 'Архивный';
$_['text_product_similar_category'] = 'Редирект на категорию, если категория архивная:';

// Text product
$_['entry_product_similar_category'] = 'Список похожих категорий, если товар архивный:';
$_['entry_product_similar_product'] = 'Список похожих товаров, если товар архивный:';
