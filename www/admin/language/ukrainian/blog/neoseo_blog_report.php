<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Статистика переглядів сторінок</p>';
$_['heading_title_raw'] = 'NeoSeo Блог Статистика переглядів сторінок';

// Column
$_['column_article_name'] = 'Назва статті';
$_['column_author_name']  = 'Ім\'я автора';
$_['column_viewed']       = 'Переглядів';
$_['column_percent']      = 'У відсотках';
$_['text_module_version'] = '';