<?php
// Heading
$_['heading_title']                   = 'Налаштування блогу';
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Налаштування блогу</p>';
$_['heading_title_raw'] = 'NeoSeo Налаштування блогу';

// Entry
$_['entry_add_to_menu']               = 'Додати посилання на блог в меню:';
$_['entry_comment_auto_approval']     = 'Автоперевірка коментарів:';
$_['entry_author_block_article']      = 'Додати інформацію про автора в статтю:';
$_['entry_product_limit']             = 'К-ть товарів в категорії:';
$_['entry_comment_limit']             = 'К-ть коментарів:';
$_['entry_cache_results']             = 'Кешувати результати:';
$_['entry_article_time_format']       = 'Формат часу для статті:';
$_['entry_comment_time_format']       = 'Формат часу для коментарів:';

$_['entry_meta_date']                 = 'Відображати дату в статті:';
$_['entry_meta_author']               = 'Відображати автора в статті:';
$_['entry_meta_comments']             = 'Відображати к-ть коментарів у статті:';
$_['entry_meta_category']             = 'Відображати категорію в статті:';

$_['entry_heading']                   = 'Заголовок блогу:';
$_['entry_title']                     = 'Блог Title:';
$_['entry_meta_keyword']              = 'Meta Keywords:';
$_['entry_meta_description']          = 'Meta Description:';
$_['entry_articles_block_heading']    = 'Заголовок блоку статтей:';
$_['entry_products_block_heading']    = 'Заголовок блоку товарів:';
$_['entry_comments_block_heading']    = 'Заголовок блоку коментарів:';
$_['entry_author_block_heading']      = 'Заголовок блоку автора:';
$_['entry_gallery_heading']           = 'Заголовок галереї:';
$_['entry_share_social_code']         = 'Код блоку "Поділитись":';

$_['entry_image_article']             = 'Розмір зображення статті';
$_['entry_image_article_list']        = 'Розмір зображення статті в категорії';
$_['entry_image_article_block']       = 'Розмір зображення статті в блоці';
$_['entry_image_product_block']       = 'Розмір зображення товару в блоці';
$_['entry_image_category_block']      = 'Розмір зображення категорії';
$_['entry_image_author_block']        = 'Розмір зображення автора';
$_['entry_image_gallery_thumb']       = 'Розмір зображення галереї';
$_['entry_width']                     = 'Ширина';
$_['entry_height']                    = 'Висота';

// Text
$_['text_success']                    = 'Налаштування змінені!';
$_['text_edit']                       = 'Змінити налаштування';
$_['text_support']                    = 'Підтримка модуля';
$_['text_module_version'] = '';

// Help 
$_['help_time_format']                = '<a href="http://php.net/manual/en/function.strftime.php" target="_blank">формати часу</a>';

// Error
$_['error_permission']                = 'У вас немає прав змінювати налаштування блогу!';