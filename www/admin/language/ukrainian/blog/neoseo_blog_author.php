<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Автори</p>';
$_['heading_title_raw'] = 'NeoSeo Блог Автори';

// Columns
$_['column_author_name'] = 'Ім\'я автора';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата додавання';
$_['column_action'] = 'Дія';

// Text
$_['text_success'] = 'Автор успішно змінений!';
$_['text_edit'] = 'Редагування автора';
$_['text_add'] = 'Створення автора';
$_['text_module_version'] = '';

// Help
$_['help_name'] = 'Ім\'я автора має бути унікальним.';
$_['help_seo_url'] = 'Повинно бути унікальним, не використовуйте пробіли, заміняйте їх на "-".';

// Entry
$_['entry_name'] = 'Ім\'я автора:';
$_['entry_seo_url'] = 'SEO URL:';
$_['entry_image'] = 'Аватар:';
$_['entry_type'] = 'Тип:';
$_['entry_teaser'] = 'Короткий опис:';
$_['entry_description'] = 'Опис:';
$_['entry_meta_title'] = 'HTML-тег Title';
$_['entry_meta_h1'] = 'HTML-тег H1';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_meta_keyword'] = 'Meta Tag Keywords:';
$_['entry_status'] = 'Статус:';


// Errors
$_['error_warning'] = 'Перевірте форму на наявність помилок!';
$_['error_permission'] = 'У вас немає прав змінювати цей матеріал!';
$_['error_name'] = 'Ім\'я автора повинно бути більше 3 і менше 255 Символів!';
$_['error_author_found'] = 'Ім\'я автора вже існує, воно повинно бути унікальним!';
$_['error_seo_url'] = 'Цей SEO URL вже використовується!';
$_['error_article'] = 'Цей автор не може бути видалений, так як в нього є %s статтей!';
