<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Статті</p>';
$_['heading_title_raw'] = 'NeoSeo Блог Статті';

// Text
$_['text_success'] = 'Стаття успішно змінена!';
$_['text_edit'] = 'Редагування статті';
$_['text_add'] = 'Створення статті';
$_['text_module_version'] = '';

// Buttons
$_['button_add_articles'] = 'Додати статтю';

// Columns
$_['column_article_name'] = 'Назва статті';
$_['column_author_name'] = 'Ім\'я автора';
$_['column_category'] = 'Категорія';
$_['column_sort_order'] = 'Сортування';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата додавання';
$_['column_action'] = 'Дія';

// Tab
$_['tab_related'] = 'Пов\'язані матеріали';

// Help
$_['help_title'] = 'Ім\'я статті повинно бути унікальним.';
$_['help_teaser'] = 'Відображається в списку статтей';
$_['help_seo_url'] = 'Повинно бути унікальним, не використовуйте пробіли, заміняйте їх на "-".';
$_['help_image'] = 'Зображення статті.';
$_['help_related_article_name'] = 'Додається через автодоповнення.';


// Entry
$_['entry_title'] = 'Назва статті:';
$_['entry_description'] = 'Опис:';
$_['entry_teaser'] = 'Короткий опис:';
$_['entry_author_name'] = 'Ім\'я автора:';
$_['entry_meta_title'] = 'HTML-тег Title';
$_['entry_meta_h1'] = 'HTML-тег H1';
$_['entry_meta_description'] = 'Meta Description:';
$_['entry_meta_keyword'] = 'Meta keyword:';
$_['entry_allow_comment'] = 'Дозволити коментарі?:';
$_['entry_seo_url'] = 'SEO URL:';
$_['entry_image'] = 'Зображення:';
$_['entry_sort_order'] = 'Сортування:';
$_['entry_status'] = 'Статус:';
$_['entry_category'] = 'Категорія:';
$_['entry_main_category'] = 'Головна категорія';
$_['entry_store'] = 'Магазин:';
$_['entry_layout'] = 'Шаблон:';
$_['entry_related_products'] = 'Пов\'язані товари:';
$_['entry_related_products_product'] = 'Товар:';
$_['entry_related_products_category'] = 'Категорія:';
$_['entry_related_products_manufacturer'] = 'Виробник:';
$_['entry_related_articles'] = 'Пов\'язані статті';
$_['entry_related_article_name'] = 'Назва статті:';
$_['entry_date_added'] = 'Дата створення:';
$_['entry_date_modified'] = 'Дата модифікації:';

// Error
$_['error_warning'] = 'Перевірте форму на помилки!';
$_['error_permission'] = 'У вас немає прав змінювати даний матеріал!';
$_['error_title'] = 'Назва статті повинна бути більшою 3 і менше 100 символів!';
$_['error_title_found'] = 'Назва статті існує, вона повинна бути унікальною!';
$_['error_description'] = 'Опис повинен бути більше 3 символів!';
$_['error_author_name'] = 'Ім\'я автора не може бути пустим!';
$_['error_author_not_found'] = 'Автор не знайдений!';
$_['error_seo_url'] = 'Цей SEO URL вже використовується!';
$_['error_article_related'] = 'Ви не можете видалити статтю, тому що є пов\'язані з нею %s статті!';
