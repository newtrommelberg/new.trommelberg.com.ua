<?php
// HTTP
define('HTTP_SERVER', 'http://new.trommelberg.com.ua/admin/');
define('HTTP_CATALOG', 'http://new.trommelberg.com.ua/');

// HTTPS
define('HTTPS_SERVER', 'http://new.trommelberg.com.ua/admin/');
define('HTTPS_CATALOG', 'http://new.trommelberg.com.ua/');

// DIR
define('DIR_APPLICATION', '/var/www/www-root/data/www/new.trommelberg.com.ua/admin/');
define('DIR_SYSTEM', '/var/www/www-root/data/www/new.trommelberg.com.ua/system/');
define('DIR_LANGUAGE', '/var/www/www-root/data/www/new.trommelberg.com.ua/admin/language/');
define('DIR_TEMPLATE', '/var/www/www-root/data/www/new.trommelberg.com.ua/admin/view/template/');
define('DIR_CONFIG', '/var/www/www-root/data/www/new.trommelberg.com.ua/system/config/');
define('DIR_IMAGE', '/var/www/www-root/data/www/new.trommelberg.com.ua/image/');
define('DIR_CACHE', '/var/www/www-root/data/www/new.trommelberg.com.ua/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/www-root/data/www/new.trommelberg.com.ua/system/storage/download/');
define('DIR_LOGS', '/var/www/www-root/data/www/new.trommelberg.com.ua/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/www-root/data/www/new.trommelberg.com.ua/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/www-root/data/www/new.trommelberg.com.ua/system/storage/upload/');
define('DIR_CATALOG', '/var/www/www-root/data/www/new.trommelberg.com.ua/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'new.trommelberg.com.ua');
define('DB_PASSWORD', 'S9l0J2k6');
define('DB_DATABASE', 'new.trommelberg.com.ua');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
