<?php

require_once(DIR_SYSTEM . "/engine/neoseo_model.php");

class ModelModuleNeoSeoPopupMessage extends NeoSeoModel
{

	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->_moduleSysName = 'neoseo_popup_message';
		$this->_logFile = $this->_moduleSysName . '.log';
		$this->debug = $this->config->get($this->_moduleSysName . '_status') == 1;

		$this->load->model('localisation/language');
		foreach ($this->model_localisation_language->getLanguages() as $language) {
			$title[$language['language_id']] = "";
			$message[$language['language_id']] = "";
		}

		$this->params = array(
			'status' => 1,
			'debug' => 0,
		);
	}

	public function installTables(){
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "popup_windows` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`status` int(11) NOT NULL,
			`title_background` varchar(12) NOT NULL,
			`font_color` varchar(12) NOT NULL,
			`background_color` varchar(12) NOT NULL,
			`background_image` varchar(255) NOT NULL,
			`action` varchar(255) NOT NULL,
			`action_type` varchar(25) NOT NULL,
			`title` text,
			`message` text,
			`position` varchar(25) NOT NULL,
			`number_impressions` int(11) DEFAULT NULL,
			`time` int(11) DEFAULT NULL,
			`min_height` int(11) DEFAULT NULL,
			`min_width` int(11) DEFAULT NULL,
			`selector` varchar(50) DEFAULT NULL,
			`template` varchar(50) DEFAULT NULL,
			`sort_order` int(11) NOT NULL,
			`close` int(11) NOT NULL,
			PRIMARY KEY (`id`),
			KEY `action` (`action`(191))
		) ENGINE=InnoDB;");

		$sql = "SHOW COLUMNS FROM `" . DB_PREFIX . "popup_windows` LIKE 'template'";
		$query = $this->db->query($sql);
		if( !$query->num_rows ) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "popup_windows` ADD `template` varchar(50) AFTER `selector`;");
		}

	}

	public function install()
	{
		// Значения параметров по умолчанию
		$this->initParams($this->params);

		$this->installTables();

		$this->load->model('user/user_group');
		$this->addPermission($this->user->getGroupId(), 'access', 'marketing/' . $this->_moduleSysName());
		$this->addPermission($this->user->getGroupId(), 'modify', 'marketing/' . $this->_moduleSysName());
		return TRUE;
	}

	public function upgrade()
	{

		// Добавляем недостающие новые параметры
		$this->initParams($this->params);

		$this->installTables();
	}

	public function uninstall()
	{
		$this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "popup_windows");

		$this->load->model('user/user_group');
		$this->model_user_user_group->removePermission($this->user->getGroupId(), 'access', 'marketing/' . $this->_moduleSysName());
		$this->model_user_user_group->removePermission($this->user->getGroupId(), 'modify', 'marketing/' . $this->_moduleSysName());
		return TRUE;
	}

}

