<?php

require_once( DIR_SYSTEM . "/engine/soforp_model.php");

class ModelModuleNeoseoInformativeMessage extends SoforpModel
{

	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->_moduleSysName = "neoseo_informative_message";
		$this->_logFile = $this->_moduleSysName . ".log";
		$this->debug = $this->config->get($this->_moduleSysName . "_status") == 1;
	}

	public function install()
	{
		$this->load->model('localisation/language');
		
		foreach ($this->model_localisation_language->getLanguages() as $language) {
			$message[$language['language_id']] = '';
			$button_text[$language['language_id']] = 'Закрыть';
		}

		// Значения параметров по умолчанию
		$this->initParamsDefaults(array(
			'status' => 1,
			'debug' => 0,
			'show_close_button' => 0,
			'close_button_text' => $button_text,
			'color_background' => '#0655c9',
			'color_close_button' => '#1292d9',
			'text' => $message,
		));

		return TRUE;
	}

	public function upgrade()
	{
		return TRUE;
	}

	public function uninstall()
	{
		return TRUE;
	}

}

?>