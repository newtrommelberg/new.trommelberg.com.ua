<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">

    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <?php if( !isset($license_error) ) { ?>
                <a href="<?php echo $add_link; ?>" class="btn btn-success"><i class="fa fa-plus"></i></a>
                <?php } else { ?>
                <a href="<?php echo $recheck; ?>" data-toggle="tooltip" title="<?php echo $button_recheck; ?>"
                   class="btn btn-primary"/><i class="fa fa-check"></i> <?php echo $button_recheck; ?></a>
                <?php } ?>
                <a href="<?php echo $close; ?>" data-toggle="tooltip" title="<?php echo $button_close; ?>"
                   class="btn btn-default"><i class="fa fa-close"></i> <?php echo $button_close; ?></a>
            </div>

            <img width="36" height="36" style="float:left" src="view/image/neoseo.png" alt=""/>
            <h1><?php echo $heading_title_raw . " " . $text_module_version; ?></h1>

            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>

        </div>
    </div>

    <div class="container-fluid">

        <?php if ($error_warning) { ?>
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>

        <?php if (isset($success) && $success) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle"></i>
            <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list_title; ?></h1>
            </div>
            <div class="panel-body">
                <form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="form">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <?php if( !isset($license_error) ) { ?>
                                <!-- -->
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group col-sm-4">
                                            <label class="control-label" for="input-name"><?php echo $text_filter_status; ?></label>
                                            <select name="filter_status" class="form-control">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <option value="1" <?php echo $filter_status==1?"selected":""; ?>><?php echo $text_enabled; ?></option>
                                                <option value="0" <?php echo ($filter_status==0 && $filter_status!="")?"selected":""; ?>><?php echo $text_disabled; ?></option>

                                            </select>
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label class="control-label" for="input-name"><?php echo $text_filter_action; ?></label>
                                            <select name="filter_action" class="form-control">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach( $user_action as $key => $a_title) { ?>
                                                <option value="<?php echo $key; ?>" <?php if( $filter_action == $key ) echo " selected "; ?>><?php echo $a_title; ?></option>
                                                <?php } ?>
                                            </select>                                        </div>
                                        <div class="form-group col-sm-4">
                                            <label class="control-label" for="input-name"><?php echo $text_filter_user_action; ?></label>
                                            <select name="filter_page" class="form-control">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach( $actions_title as $key => $a_title) { ?>
                                                <option value="<?php echo $key; ?>" <?php if( $filter_page == $key ) echo " selected "; ?>><?php echo $a_title; ?></option>
                                                <?php } ?>
                                            </select>                                        </div>
                                        <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                                    </div>
                                </div>
                            </div>

                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <td class="text-left"><?php echo $text_title; ?></td>
                                    <td class="text-left"><a href ="<?php echo $sort_status_link; ?>" class="<?php echo($sort == 'status')?$order:""; ?>"><?php echo $text_status; ?></a></td>
                                    <td class="text-left"><a href ="<?php echo $sort_action_link; ?>" class="<?php echo($sort == 'action_type')?$order:""; ?>"><?php echo $text_user_action; ?></a></td>
                                    <td class="text-left"><a href ="<?php echo $sort_page_link; ?>" class="<?php echo($sort == 'action')?$order:""; ?>"><?php echo $text_page; ?></a></td>
                                    <td class="text-left"><a href ="<?php echo $sort_order_link; ?>" class="<?php echo($sort == 'sort_order')?$order:""; ?>"><?php echo $entry_sort_order; ?></a></td>
                                    <td class="text-left"><?php echo $text_action; ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach( $windows_data as $window) { ?>
                                    <tr>
                                        <td><?php echo $window['title']; ?></td>
                                        <td><?php echo $window['status']; ?></td>
                                        <td><?php echo $window['type']; ?></td>
                                        <td><?php echo $window['action']; ?></td>
                                        <td><?php echo $window['sort_order']; ?></td>
                                        <td><a href="<?php echo $window['edit']; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> <a href="<?php echo $window['delete']; ?>" class="btn btn-danger"  onclick="return confirm('<?php echo $text_confirm; ?>');"><i class="fa fa-trash"></i></a> </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
                        </div>
                                <!-- -->

                            <?php } else { ?>
                            <?php echo $license_error; ?>
                            <?php } ?>
                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>
</div>

<script type="text/javascript"><!--
    if (window.location.hash.indexOf('#tab') == 0 && $("[href=" + window.location.hash + "]").length) {
        $(".panel-body > .nav-tabs li").removeClass("active");
        $("[href=" + window.location.hash + "]").parents('li').addClass("active");
        $(".panel-body:first .tab-content:first .tab-pane:first").removeClass("active");
        $(window.location.hash).addClass("active");
    }
    $(".nav-tabs li a").click(function () {
        var url = $(this).prop('href');
        window.location.hash = url.substring(url.indexOf('#'));
    });

    // Специальный фикс системной функции, поскольку даниель понятия не имеет о том что в url может быть еще и hash
    // и по итогу этот hash становится частью token
    function getURLVar(key) {
        var value = [];

        var url = String(document.location);
        if (url.indexOf('#') != -1) {
            url = url.substring(0, url.indexOf('#'));
        }
        var query = url.split('?');

        if (query[1]) {
            var part = query[1].split('&');

            for (i = 0; i < part.length; i++) {
                var data = part[i].split('=');

                if (data[0] && data[1]) {
                    value[data[0]] = data[1];
                }
            }

            if (value[key]) {
                return value[key];
            } else {
                return '';
            }
        }
    }
    $('#button-filter').on('click', function() {
        var url = 'index.php?route=marketing/<?php echo $sysname; ?>&token=<?php echo $token; ?>';
        var filter_status = $('select[name=\'filter_status\']').val();
        if (filter_status) {
            url += '&filter_status=' + encodeURIComponent(filter_status);
        }
        var filter_action = $('select[name=\'filter_action\']').val();
        if (filter_action) {
            url += '&filter_action=' + encodeURIComponent(filter_action);
        }
        var filter_page = $('select[name=\'filter_page\']').val();
        if (filter_page) {
            url += '&filter_page=' + encodeURIComponent(filter_page);
        }

        location = url;
    });
    //--></script>

<?php echo $footer; ?>