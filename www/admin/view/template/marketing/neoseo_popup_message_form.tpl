<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">

    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <?php if( !isset($license_error) ) { ?>
                <button type="submit" name="action" value="save" form="form" data-toggle="tooltip"
                        title="<?php echo $button_save; ?>" class="btn btn-primary"><i
                            class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <a onclick="$('#form').attr('action', '<?php echo $save_and_close; ?>'); $('#form').submit();"
                   class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;<?php echo $button_save_and_close; ?></a>
                <?php } else { ?>
                <a href="<?php echo $recheck; ?>" data-toggle="tooltip" title="<?php echo $button_recheck; ?>"
                   class="btn btn-primary"/><i class="fa fa-check"></i> <?php echo $button_recheck; ?></a>
                <?php } ?>
                <a href="<?php echo $close; ?>" data-toggle="tooltip" title="<?php echo $button_close; ?>"
                   class="btn btn-default"><i class="fa fa-close"></i> <?php echo $button_close; ?></a>
            </div>

            <img width="36" height="36" style="float:left" src="view/image/neoseo.png" alt=""/>
            <h1><?php echo $heading_title_raw . " " . $text_module_version; ?></h1>

            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>

        </div>
    </div>

    <div class="container-fluid">

        <?php if ($error_warning) { ?>
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>

        <?php if (isset($success) && $success) { ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle"></i>
            <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit_title; ?></h1>
            </div>
            <div class="panel-body">
                <form action="<?php echo $save; ?>" method="post" enctype="multipart/form-data" id="form">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <?php if( !isset($license_error) ) { ?>
                            <input type="hidden" value="<?php echo $window_id; ?>" name="window_id">
                            <?php $widgets->dropdown('status',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                            <?php $widgets->dropdown('popup_position', $params_positions);?>
                            <?php $widgets->dropdown('popup_close',array( 0 => $text_disabled, 1 => $text_enabled)); ?>
                            <?php $widgets->dropdown('popup_action',$actions_title); ?>
                            <?php $widgets->input('custom_action'); ?>
                            <?php $widgets->dropdown('popup_user_action',$user_action); ?>
                            <?php $widgets->input('popup_selector'); ?>
                            <?php $widgets->input('sort_order'); ?>
                            <?php $widgets->input('popup_number_impressions');?>
                            <?php $widgets->dropdown('popup_template',$templates); ?>
                            <?php $widgets->input('popup_time');?>
                            <?php $widgets->input('popup_min_height');?>
                            <?php $widgets->input('popup_min_width');?>
                            <?php $widgets->inputColor('popup_font_color');?>
                            <?php $widgets->inputColor('popup_title_background');?>
                            <?php $widgets->inputColor('popup_background');?>
                            <?php $widgets->inputImage('popup_background_image', $placeholder, $background_logo);?>
                            <?php $widgets->localeInput('popup_title',$languages);?>
                            <?php $widgets->localeTextarea('popup_message',$languages); ?>
                            <?php } else { ?>
                            <?php echo $license_error; ?>
                            <?php } ?>
                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>
</div>

<script type="text/javascript"><!--
    if (window.location.hash.indexOf('#tab') == 0 && $("[href=" + window.location.hash + "]").length) {
        $(".panel-body > .nav-tabs li").removeClass("active");
        $("[href=" + window.location.hash + "]").parents('li').addClass("active");
        $(".panel-body:first .tab-content:first .tab-pane:first").removeClass("active");
        $(window.location.hash).addClass("active");
    }
    $(".nav-tabs li a").click(function () {
        var url = $(this).prop('href');
        window.location.hash = url.substring(url.indexOf('#'));
    });

    // Специальный фикс системной функции, поскольку даниель понятия не имеет о том что в url может быть еще и hash
    // и по итогу этот hash становится частью token
    function getURLVar(key) {
        var value = [];

        var url = String(document.location);
        if (url.indexOf('#') != -1) {
            url = url.substring(0, url.indexOf('#'));
        }
        var query = url.split('?');

        if (query[1]) {
            var part = query[1].split('&');

            for (i = 0; i < part.length; i++) {
                var data = part[i].split('=');

                if (data[0] && data[1]) {
                    value[data[0]] = data[1];
                }
            }

            if (value[key]) {
                return value[key];
            } else {
                return '';
            }
        }
    }

    //--></script>

<script type="text/javascript">
	<?php foreach($languages as $language){ ?>
	<?php if ($ckeditor) { ?>
			ckeditorInit("neoseo_popup_message_popup_message<?php echo $language['language_id']?>", '<?php echo $token; ?>');
		<?php } else { ?>
			$("#neoseo_popup_message_popup_message<?php echo $language['language_id']?>").summernote({height: 300, lang:'ru-RU'});
		<?php } ?>
	<?php } ?>
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".colorpicker-component").colorpicker();
    });
    <?php if(${$sysname.'_popup_action'} != 'custom_route') { ?>
        $('#field_custom_action').hide();
    <?php } ?>

    <?php if(${$sysname.'_popup_user_action'} == 'auto') { ?>
        $('#field_popup_selector').hide();
    <?php } ?>

    $('#neoseo_popup_message_popup_action').on('change', function(){

        if($(this).val() == 'custom_route') {
            $('#field_custom_action').fadeIn();
        } else {
            $('#field_custom_action').hide();
        }
    });

    $('#neoseo_popup_message_popup_user_action').on('change', function(){

        if($(this).val() != 'auto') {
            $('#field_popup_selector').fadeIn();
        } else {
            $('#field_popup_selector').hide();
        }
    });

</script>

<?php echo $footer; ?>