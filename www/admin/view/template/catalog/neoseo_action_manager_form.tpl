<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-links" data-toggle="tab"><?php echo $tab_other_data; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="action_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($action_description[$language['language_id']]) ? $action_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_name[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>f
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_text_short; ?></label>
                    <div class="col-sm-10">
                      <textarea name="action_description[<?php echo $language['language_id']; ?>][short_text]" placeholder="<?php echo $entry_text_short; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($action_description[$language['language_id']]) ? $action_description[$language['language_id']]['short_text'] : ''; ?></textarea>
                    </div>
                  </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-textlong<?php echo $language['language_id']; ?>"><?php echo $entry_text_long; ?></label>
                        <div class="col-sm-10">
                            <textarea name="action_description[<?php echo $language['language_id']; ?>][full_text]" placeholder="<?php echo $entry_text_long; ?>" id="input-textlong<?php echo $language['language_id']; ?>"><?php echo isset($action_description[$language['language_id']]) ? $action_description[$language['language_id']]['full_text'] : ''; ?></textarea>
                        </div>
                    </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="action_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($action_description[$language['language_id']]) ? $action_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="action_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($action_description[$language['language_id']]) ? $action_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                    <div class="col-sm-10">
                      <textarea name="action_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($action_description[$language['language_id']]) ? $action_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>

            <div class="tab-pane" id="tab-links">

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-action_status"><span data-toggle="tooltip" title="<?php echo $help_action_status; ?>"><?php echo $entry_action_status; ?></span></label>
                    <div class="col-sm-10">

                        <select name="action_status" id="input-action_status" class="form-control">
                            <?php foreach ($action_status_list as $id => $value) { ?>
                            <?php if ($id == $action_status) { ?>
                            <option value="<?php echo $id; ?>" selected="selected"><?php echo $value; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $id; ?>"><?php echo $value; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>

                        <?php if ($error_action_status) { ?>
                        <div class="text-danger"><?php echo $error_action_status; ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
                    <div class="col-sm-10">
                        <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                        <?php if ($error_keyword) { ?>
                        <div class="text-danger"><?php echo $error_keyword; ?></div>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-date-end"><?php echo $entry_date_end; ?></label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <input type="text" name="date_end" value="<?php echo $date_end; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_image; ?></label>
                    <div class="col-sm-10">
                    <table class="table table-striped table-bordered table-hover">
                        <tbody>
                        <tr>
                            <td class="text-left"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" /></td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_image_width; ?></label>
                    <div class="col-sm-10">
                            <input type="text" name="image_width" value="<?php echo $image_width; ?>" placeholder="<?php echo $entry_image_width; ?>" class="form-control" />
                    </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_image_height; ?></label>
                    <div class="col-sm-10">
                            <input type="text" name="image_height" value="<?php echo $image_height; ?>" placeholder="<?php echo $entry_image_height; ?>" class="form-control" />
                    </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_main_page; ?></label>
                    <div class="col-sm-10">
                        <label class="radio-inline">
                            <?php if ($main_page) { ?>
                            <input type="radio" name="main_page" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <?php } else { ?>
                            <input type="radio" name="main_page" value="1" />
                            <?php echo $text_yes; ?>
                            <?php } ?>
                        </label>
                        <label class="radio-inline">
                            <?php if (!$main_page) { ?>
                            <input type="radio" name="main_page" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                            <?php } else { ?>
                            <input type="radio" name="main_page" value="0" />
                            <?php echo $text_no; ?>
                            <?php } ?>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo $entry_all_category; ?></label>
                    <div class="col-sm-10">
                        <label class="radio-inline">
                            <?php if ($all_category) { ?>
                            <input type="radio" name="all_category" id="all_cat_y" value="1" checked="checked" />
                            <?php echo $text_yes; ?>
                            <?php } else { ?>
                            <input type="radio" name="all_category" value="1" />
                            <?php echo $text_yes; ?>
                            <?php } ?>
                        </label>
                        <label class="radio-inline">
                            <?php if (!$all_category) { ?>
                            <input type="radio" name="all_category" value="0" checked="checked" />
                            <?php echo $text_no; ?>
                            <?php } else { ?>
                            <input type="radio" name="all_category" value="0" />
                            <?php echo $text_no; ?>
                            <?php } ?>
                        </label>
                    </div>
                </div>

                <div class="form-group">

                    <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related_category; ?></span></label>
                    <div class="col-sm-10">
                        <input type="text" name="related_category" value="" placeholder="<?php echo $entry_related_category; ?>" id="input-related" class="form-control" />
                        <div id="category-related" class="well well-sm" style="height: 150px; overflow: auto;">
                            <?php foreach ($actions_categories as $action_category) { ?>
                            <div id="category-related<?php echo $action_category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $action_category['name']; ?>
                                <input type="hidden" name="action_category[]" value="<?php echo $action_category['category_id']; ?>" />
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="related" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                  <div id="product-related" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($actions_products as $action_product) { ?>
                    <div id="product-related<?php echo $action_product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $action_product['name']; ?>
                      <input type="hidden" name="action_product[]" value="<?php echo $action_product['product_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>

            </div>




          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
<?php if ($ckeditor) { ?>
    ckeditorInit('input-description<?php echo $language['language_id']; ?>', '<?php echo $token; ?>');
    ckeditorInit('input-textlong<?php echo $language['language_id']; ?>', '<?php echo $token; ?>');
<?php } else { ?>
    $('#input-description<?php echo $language['language_id']; ?>').summernote({height: 300, lang:'<?php echo $lang; ?>'});
    $('#input-textlong<?php echo $language['language_id']; ?>').summernote({height: 300, lang:'<?php echo $lang; ?>'});
<?php } ?>
<?php } ?>
//--></script>
  <script type="text/javascript"><!--

// Related
$('input[name=\'related\']').autocomplete({
	'source': function(request, response) {

		$.ajax({
			url: 'index.php?route=catalog/neoseo_action_manager/autocomplete_product&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'related\']').val('');

		$('#product-related' + item['value']).remove();

		$('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="action_product[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-related').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//-->


      // Related
      $('input[name=\'related_category\']').autocomplete({
          'source': function(request, response) {

              $.ajax({
                  url: 'index.php?route=catalog/neoseo_action_manager/autocomplete_category&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                  dataType: 'json',
                  success: function(json) {

                      response($.map(json, function(item) {
                          return {
                              label: item['name'],
                              value: item['category_id']
                          }
                      }));
                  }
              });
          },
          'select': function(item) {
              $('input[name=\'related_category\']').val('');

              $('#category-related' + item['value']).remove();

              $('#category-related').append('<div id="category-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="action_category[]" value="' + item['value'] + '" /></div>');
          }
      });

      $('#category-related').delegate('.fa-minus-circle', 'click', function() {
          $(this).parent().remove();
      });


  </script>


  <script type="text/javascript"><!--

      $('#all_cat_y').change(function() {
          if( $(this).checked) {
              alert('ok');
          }


      });

$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
$('#option a:first').tab('show');
//--></script></div>
<?php echo $footer; ?>
