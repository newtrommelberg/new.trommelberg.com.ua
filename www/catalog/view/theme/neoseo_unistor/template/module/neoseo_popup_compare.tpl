<div class="modal modal--compare fade ">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close-button close" data-dismiss="modal">
                    <span></span>
                    <span></span>
                </button>
                <h3 class="modal-title"><i class="ns-clone" aria-hidden="true"></i><?php echo $heading_title; ?></h3>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-long-arrow-left" aria-hidden="true"></i><?php echo $button_continue; ?></button>
                <a href="<?php echo $action; ?>" class="btn btn-primary"><?php echo $button_action; ?></a>
            </div>
        </div>
    </div>
</div>
