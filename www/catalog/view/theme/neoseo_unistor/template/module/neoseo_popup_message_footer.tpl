<?php if(isset($window_data['hover'])) { ?>
<script>
    <?php foreach($window_data['hover'] as $hover) { ?>
        $("<?php echo $hover['selector']; ?>").on({
            mouseenter: function () {
                var language = "";
                var showed<?php echo $hover['id']; ?>;
                if(window.showed<?php echo $hover['id']; ?> == true) return;
                if (window.current_language) {
                    language = window.current_language;
                }
                $.ajax({
                    url: language + 'index.php?route=module/neoseo_popup_message',
                    type: 'post',
                    data: {'window_id':'<?php echo $hover['id']; ?>'},
                    dataType: 'json',
                    success: function (json) {
                        $('.alert').remove();

                        if (json['popup']) {
                            popupMessage(json);
                            window.showed<?php echo $hover['id']; ?> = true;
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            },
        });
    <? } ?>
</script>
<? } ?>
<?php if(isset($window_data['click'])) { ?>
<script>
    <?php foreach($window_data['click'] as $click) { ?>
        $('<?php echo $click['selector']; ?>').on('click', function(){
            var language = "";
            if (window.current_language) {
                language = window.current_language;
            }
            $.ajax({
                url: language + 'index.php?route=module/neoseo_popup_message',
                type: 'post',
                data: {'window_id':'<?php echo $click['id']; ?>'},
                dataType: 'json',
                success: function (json) {
                    $('.alert').remove();

                    if (json['popup']) {
                        popupMessage(json);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    <? } ?>
</script>
<? } ?>
<?php if(isset($window_data['auto'])) { ?>
<script>
    $(function(){
        $.ajax({
            url: 'index.php?route=module/neoseo_popup_message/getPopupTime',
            type: 'post',
            data: {'window_id':'<?php echo $window_data['auto']; ?>'},
            dataType: 'json',
            success: function (json) {
                setTimeout(showPopupMessage, json['time']);
            },
        });

    });

    function showPopupMessage() {
        var language = "";
        if (window.current_language) {
            language = window.current_language;
        }
        $.ajax({
            url: language + 'index.php?route=module/neoseo_popup_message',
            type: 'post',
            data: {'window_id':'<?php echo $window_data['auto']; ?>'},
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();

                if (json['popup']) {
                    popupMessage(json);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
</script>
<? } ?>
<script>
    function popupMessage(json) {
        if ($("#popup-message").length == 0) {
            $("body").append("<div id=\"popup-message\"></div>");
        }
        $("#popup-message").html(json["popup"]);
        $('#popup-message > div').modal();
    }
</script>