
<div id="single-menu-catalog">
    <div class="container">
        <button class="single-menu-catalog__toggle-button" onclick="$(this).next().slideToggle();">
            <i class="fa fa-bars"></i>
            <span><?php echo $text_menu; ?></span>
        </button>
        <?php if ($categories) { ?>
        <ul class="single-menu-catalog__menu">
            <?php foreach ($categories as $category) {  ?>
            <?php if (preg_match('~single-menu-catalog~', $category['class'])) { ?>
            <li class="single-menu-catalog__title <?php echo $category['class']?>" style="<?php echo $category['style']?>" >
                <span onclick="($(window).width() < 992) ? $(this).next('.single-menu-catalog__children').slideToggle() : false;"><i class="fa fa-bars hidden-xs hidden-sm"></i><?php echo $category['name'] ?><i class="fa fa-angle-down hidden-md hidden-lg"></i></span>
                <?php if ($category['children']) { ?>
                <ul class="single-menu-catalog__children">
                    <?php foreach($category['children'] as $children) { ?>
                    <li class="<?php echo $children['class']?>" >
                        <a href="<?php echo  $children['href']; ?>" style="<?php echo $children['style']?>" onclick="($(window).width() < 992) ? $(this).next('.single-menu-catalog__submenu').slideToggle() : false;">
                            <?php // Проверяем есть ли иконка(изображение) у этого пункта ?>
                            <?php if ($children['icon']) { ?>
                            <img src="<?php echo  $children['icon']; ?>" alt="<?php echo $children['name']; ?>">
                            <?php } ?>

                            <?php echo  $children['name']; ?>

                            <?php // Проверяем есть ли дочерние элементы ?>
                            <?php if ($children['children']) { ?>
                            <i class="fa fa-angle-right"></i>
                            <?php } ?>
                        </a>

                        <?php if ($children['children']) { ?>
                        <div class="single-menu-catalog__submenu">
                            <ul class="single-menu-catalog__children2 <?php echo $children['class']?>">
                                <?php foreach($children['children'] as $children2) { ?>
                                <li class="<?php echo $children2['class']?>" style="<?php echo $children2['style']?>">
                                    <a <?php if ($children2['href']) { ?>href="<?php echo $children2['href']; ?>"<?php } else { ?>class="no-link"<?php } ?> onclick="($(window).width() < 992) ? $(this).next('.single-menu-catalog__children3').slideToggle() : false;">
                                    <?php // Проверяем есть ли изображение у этого пункта ?>
                                    <?php if ($children2['image']) { ?>
                                    <img class="children2-image" src="<?php echo  $children2['image']; ?>" alt="<?php echo $children2['name']; ?>">
                                    <?php } ?>
                                    <span class="children2-item-box">
                                                                           <?php // Проверяем есть ли иконка(изображение) у этого пункта ?>
                                        <?php if ($children2['icon']) { ?>
                                        <img src="<?php echo  $children2['icon']; ?>" alt="<?php echo $children2['name']; ?>">
                                        <?php } ?>
                                        <?php echo $children2['name']; ?>

                                        <?php if ($children2['children']) { ?>
                                        <i class="fa fa-angle-down hidden-md hidden-lg"></i>
                                        <?php } ?>
                                                                    </span>
                                    </a>

                                    <?php if ($children2['children']) { ?>
                                    <ul class="single-menu-catalog__children3">
                                        <?php foreach($children2['children'] as $children3) { ?>
                                        <li class="<?php echo $children3['class']?>" >
                                            <?php // Проверяем есть ли изображение у этого пункта ?>
                                            <?php if ($children3['image']) { ?>
                                            <a class="--image" href="<?php echo $children3['href']; ?>">
                                                <img class="children3-image" src="<?php echo  $children3['image']; ?>" alt="<?php echo $children3['name']; ?>">
                                            </a>
                                            <?php } ?>
                                            <a <?php if ($children3['href']) { ?>href="<?php echo $children3['href']; ?>"<?php } else { ?>class="no-link"<?php } ?> style="<?php echo $children3['style']; ?>">
                                            <?php // Проверяем есть ли иконка(изображение) у этого пункта ?>
                                            <?php if ($children3['icon']) { ?>
                                            <img src="<?php echo  $children3['icon']; ?>" alt="<?php echo $children3['name']; ?>">
                                            <?php } ?>
                                            <?php echo $children3['name']; ?>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                                </li>
                                <?php } ?>
                                <?php if ($children['image']) { ?>
                                <li class="hidden-xs hidden-sm">
                                    <img src="<?php echo $children['image']; ?>" alt="<?php echo $children['name']; ?>">
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php } ?>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </li>
            <?php } else { ?>
            <li><a href="<?php echo  $category['href']; ?>"><?php echo  $category['name']; ?></a></li>
            <?php } ?>
            <?php } ?>
        </ul>
        <?php } ?>
    </div>
</div>
<script>

    if ($(window).width() < 992) {
        $('#main-menu-catalog .single-menu-catalog__children > li').each(function () {
            if ($(this).children('#main-menu-catalog .single-menu-catalog__submenu').length) {
                $(this).children('a').removeAttr('href');
            }
        });

        $('#main-menu-catalog .single-menu-catalog__children2 > li').each(function () {
            if ($(this).children('#main-menu-catalog .single-menu-catalog__children3').length) {
                $(this).children('a').removeAttr('href');
            }
        });

        $('#main-menu-catalog .single-menu-catalog__children2 > li > a').on('click', function () {
            $(this).children('#main-menu-catalog  .children2-item-box').find('.fa').toggleClass('fa-angle-down fa-angle-up');
        });


    } else {
        $('#main-menu-catalog  .single-menu-catalog__submenu').css('width', ( $('#main-menu-catalog .single-menu-catalog__menu').width() - 298));
        $('#stiky_box   .single-menu-catalog__submenu').css('width', ( $('#stiky_box .container').width() - 298));

        $(window).resize(function () {
            $('#main-menu-catalog  .single-menu-catalog__submenu').css('width', ( $('#main-menu-catalog .single-menu-catalog__menu').width() - 298));
            $('#stiky_box  .single-menu-catalog__submenu').css('width', ( $('#stiky_box  .single-menu-catalog__menu').width() - 298));

        });

        $('.single-menu-catalog__children3').each(function () {
            if ($(this).find('.child-hidden').length) {
                if (!$('.popup-hidden-bg').length) {
                    $('.single-menu-catalog__submenu').prepend('<li class="popup-hidden-bg"></li>')
                }
                $(this).append('<li><a class="popup-link" onclick="showChildPopup($(this));"><?php echo $text_all_categories; ?> (' + $(this).find('.child-hidden').length + ')</a></li>');
                $(this).append('<ul class="popup-child"></ul>');
                $($(this).find('.popup-child')).append($(this).find('.child-hidden')).append('<li><a class="popup-link" onclick="closeChildPopup();"><?php echo $text_hide; ?></a></li>');
            }
        });

        $('.single-menu-catalog__submenu').mouseleave(function () {
            $('.popup-hidden-bg').fadeOut(300);
            $('.popup-child').fadeOut(300);
        });

        function showChildPopup(e) {
            var mainParent = e.parents('.single-menu-catalog__submenu');
            var parentUl = e.parents('.single-menu-catalog__children3');

            mainParent.find('.popup-hidden-bg').fadeIn(300);
            parentUl.find('.popup-child').fadeIn(300);
        }

        function closeChildPopup() {
            $('.popup-hidden-bg').fadeOut(300);
            $('.popup-child').fadeOut(300);
        }

    }

</script>

