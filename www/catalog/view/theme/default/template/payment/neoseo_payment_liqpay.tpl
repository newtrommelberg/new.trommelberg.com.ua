<form action="<?php echo $action; ?>" method="post" id="liqpay-form">
  <input type="hidden" name="operation_xml" value="<?php echo $xml; ?>">
  <input type="hidden" name="signature" value="<?php echo $signature; ?>">
  <div class="buttons">
    <div class="pull-right">
      <input type="submit" value="<?php echo $button_confirm; ?>" class="btn btn-primary" />
    </div>
  </div>
</form>
<script type="text/javascript">
	function cofirmlp() {
		$.ajax({
			type: 'get',
			url: 'index.php?route=payment/neoseo_payment_liqpay/confirm',
			cache: false,
			success: function() {
				return true;
			}
		});
	}
    $('#liqpay-form').on('submit', function(e) {
      e.preventDefault();
      $.ajax({
        type: 'get',
        url: 'index.php?route=payment/neoseo_payment_liqpay/confirm',
        cache: false,
        success: function() {
          $('#liqpay-form').off('submit').submit();
        }
      });
    });
</script>
