<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
                <h3 class="modal-title"><?php echo $heading_title; ?></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <img style="width:70%" src="/image/neoseo_callback.jpg">
                    </div>
                    <div class="col-sm-6">
                        <p><?php echo $message; ?></p>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_continue; ?></button>
            </div>
        </div>
    </div>
</div>
