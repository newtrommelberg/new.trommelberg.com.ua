<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove"></i></button>
                <h3 class="modal-title"><?php echo $heading_title; ?></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <img style="width:70%" src="/image/neoseo_catch_contacts.jpg">
                    </div>
                    <div class="col-sm-6">
                        <span class='text_wrap_consultation'></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_continue; ?></button>
            </div>
        </div>
    </div>
</div>
