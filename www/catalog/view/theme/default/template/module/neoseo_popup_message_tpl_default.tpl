<div class="popup-fade  modal fade">
	<div class="modal-dialog <?php echo $popup_position; ?>" style="
	     width: <?php echo $popup_min_width ? $popup_min_width : '600';  ?>px;
	     height: <?php echo $popup_min_height ? $popup_min_height : '200'; ?>px;">
		<div class="modal-content" style="
		     width: <?php echo $popup_min_width ? $popup_min_width : '600';  ?>px;
		     height: <?php echo $popup_min_height ? $popup_min_height : '200'; ?>px;">
			<div class="modal-header" style="background-color: <?php echo $popup_title_background; ?>;min-height: 58px;">
				<?php if ($popup_close) { ?>
				<button style="color: <?php echo $popup_font_color; ?>" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<?php } ?>
				<?php if($popup_title) { ?>
				<h4 style="color: <?php echo $popup_font_color; ?>" class="modal-title"><?php echo $popup_title;?></h4>
				<?php } ?>
			</div>
			<div class="modal-body" style="
                background-color: <?php echo $popup_background ? $popup_background : '#ffffff'; ?>;
		        background-image: url('image/<?php echo $popup_background_image; ?>');">
				<?php if($popup_message) { ?>
				<?php echo $popup_message;?>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
