<?php

// Heading
$_['heading_title'] = 'Quick order';
$_['text_quick_order'] = 'Your phone';

// Text
$_['text_no_phone'] = 'Enter your contact number';
$_['text_name'] = 'Your name';
$_['text_phone'] = 'Contact number';
$_['text_price'] = 'Price';
$_['text_agree'] = 'I have read and agree with <a class="colorbox" href="%s" target="_blank">%s</a>';

// Button
$_['button_quick_order'] = 'Quick order';
$_['button_continue'] = 'Continue shopping';
$_['button_action'] = 'Order';
