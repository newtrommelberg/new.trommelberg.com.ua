<?php
// Entry
$_['entry_name']					    = 'Your Name:';
$_['entry_captcha']					  = 'Enter the code in the box below:';
$_['entry_comment']					  = 'Your Comment:';

$_['text_author_information']	= '(Author)';
$_['text_posted_by']				  = 'Posted by';
$_['text_on']						      = 'On';
$_['text_updated']					  = 'Update on';
$_['text_view_comment']				= ' View Comments';
$_['text_write']         			= 'Leave your comment';
$_['text_cancel_reply']  			= 'cancel reply';
$_['text_note']						    = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_comments']					  = ' Comments';
$_['text_comment']					  = ' Comment';
$_['text_success']      			= 'Thank you for your comment!';
$_['text_success_approval']		= 'Thank you for your comment. It has been submitted to the webmaster for approval!';
$_['text_wait']						    = 'Wait';
$_['text_reply_comment']			= 'Reply';
$_['text_said']						    = 'said:';
$_['text_authors']					  = 'Authors';
$_['text_no_comments']        = 'There are no comments for this blog.';
$_['entry_rating']             = 'Rating';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

// Error
$_['error_name']        			= 'Warning: Author Name must be between 3 and 25 characters!';
$_['error_text']        			= 'Warning: Comment Text must be between 3 and 1000 characters!';
$_['error_captcha']     			= 'Warning: Verification code does not match the image!';