<?php
// Heading
$_['heading_title'] = 'Subscription!';

// Text
$_['text_title'] = 'Subscribe now';
$_['text_email'] = 'Yours E-mail';
$_['text_name'] = 'Enter your name';
$_['text_success'] = 'Thank you for subscribing to our newsletter!';
$_['text_empty'] = 'Enter an email address and try again';
$_['text_wrong'] = 'Enter correct email and try again';

// Button
$_['button_action'] = 'Send';

// Error
$_['error_exist'] = 'Warning! You haven\'t subscribed';