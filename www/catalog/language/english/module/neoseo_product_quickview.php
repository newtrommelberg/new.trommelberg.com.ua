<?php

// Heading
$_['heading_title'] = 'Product quickview';

// Button
$_['button_continue'] = 'Continue shopping';

// Text
$_['text_price'] = 'Price';

// TimeFlicr
$_['text_days'] = 'Days';
$_['text_hours'] = 'Hours';
$_['text_minutes'] = 'Minutes';
$_['text_seconds'] = 'Seconds';

