<?php
// Label
$_['label_name'] = 'by name';
$_['label_model'] = 'by model';
$_['label_sku'] = 'by sku';

// Text
$_['text_filter'] = 'refine by:';
$_['text_search_page'] = 'search page:';
$_['text_look_results'] = 'View all results';