<?php

// Heading
$_['heading_title'] = 'Thank you for your application!';

// Text
$_['text_name'] = 'Name';
$_['text_email'] = 'E-mail';
$_['text_phone'] = 'Phone';

// Button
$_['button_continue'] = 'Continue shopping';
$_['button_action'] = 'Submit your application';
$_['button_unsubscribe'] = 'Unsubscribe';
$_['button_subscribe'] = 'Notify';