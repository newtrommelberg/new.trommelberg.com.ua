<?php

// Heading
$_['actions_heading'] = 'Stocks';

$_['action_status_list'][0] = 'Gift';
$_['action_status_list'][1] = 'Discount';
$_['action_status_list'][2] = '';

$_['action_status_data'][0] = 'Gift';
$_['action_status_data'][1] = 'Discount';
$_['action_status_data'][2] = '';

$_['read_more'] = 'More details';
$_['till_finish'] = 'Until the end of the promotion';
$_['action_finish'] = 'The stock is over';


$_['days_left'] = 'Days';
$_['hours_left'] = 'Hours';
$_['minutes_left'] = 'Min.';
$_['seconds_left'] = 'Sec.';

$_['text_action_products'] = 'Products participating in the promotion';
