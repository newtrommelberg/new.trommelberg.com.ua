<?php
// Text
$_['text_search']       = 'Articles meeting the search criteria';
$_['text_keyword']      = 'Keywords';
$_['text_category']     = 'All Categories';

// Entry
$_['entry_search']      = 'Search Criteria';