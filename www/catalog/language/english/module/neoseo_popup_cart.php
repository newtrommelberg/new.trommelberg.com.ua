<?php

// Heading
$_['text_popup_cart_title'] = 'Your cart';
$_['text_subtotal'] = "Total";

// Column
$_['column_id'] = '#';
$_['column_name'] = 'Product';
$_['column_quantity'] = 'Qty';
$_['column_price'] = 'Price';
$_['column_action'] = '';

// Button
$_['button_continue'] = 'Continue shopping';
$_['button_checkout'] = 'Proceed checkout';

$_['message_cart_single'] = 'You habe bought product! You can continue or checkout';
?>