<?php

// Heading 
$_['heading_title'] = 'Checkout';
$_['heading_title_login'] = 'Authorization';

// Text
$_['text_checkout_option'] = 'Authorization';
$_['text_checkout_account'] = 'Buyer';
$_['text_checkout_payment_address'] = 'Billing Details';
$_['text_checkout_shipping_method'] = 'shipping and payment';
$_['text_checkout_payment_method'] = 'Payment';
$_['text_checkout_confirm'] = 'Confirm order';
$_['text_modify'] = 'Modify &raquo;';
$_['text_new_customer'] = 'New customer';
$_['text_returning_customer'] = 'returning customer';
$_['text_checkout'] = 'Checkout Options:';
$_['text_i_am_returning_customer'] = 'returning customer';
$_['text_register'] = 'Register';
$_['text_guest'] = 'Order without registering';
$_['text_register_account'] = 'By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_forgotten'] = 'Forgotten Password';
$_['text_your_details'] = 'Your Personal Details';
$_['text_your_address'] = 'Your Address';
$_['text_your_password'] = 'Your Password';
$_['text_agree'] = 'I have read and agree to the <a class="colorbox" href="%s">%s</a>';
$_['text_shipping_method'] = 'shipping Method:';
$_['text_payment_method'] = 'payment Method:';
$_['text_comments'] = 'note to the order';
$_['text_create_account'] = 'Register';
$_['text_please_wait'] = 'Wait...';
$_['text_coupon'] = 'Success: the coupon code has been applied!';
$_['text_voucher'] = 'Success: Gift certificate has been applied!';
$_['text_reward'] = 'Success: Bonus points have been applied!';
$_['text_use_coupon'] = 'Use';
$_['text_use_voucher'] = 'Use';
$_['text_use_reward'] = 'Use';
$_['text_cart'] = 'Basket';
$_['text_image'] = 'Image';
$_['text_name'] = 'Product';
$_['text_quantity'] = 'number';
$_['text_price'] = 'Price';
$_['text_total'] = 'total';
$_['text_action'] = 'Actions';
$_['text_items'] = '%s';
$_['text_shipping'] = 'shipping';
$_['text_country'] = 'Country:';
$_['text_region'] = 'Region:';
$_['text_city'] = 'City:';
$_['text_payment'] = 'Payment:';
$_['text_login'] = 'Login';
$_['text_сontact_details'] = 'Contact details';
$_['text_delivery_payment_details'] = 'Delivery and payment method';
$_['text_edit'] = 'Edit';
$_['text_not_have_account'] = 'I do not have an account';
$_['text_next'] = 'Next';
$_['text_unit'] = 'x';
$_['text_files_upload'] = 'Select files';
$_['text_contact_info'] = 'Contact info';

// Column
$_['column_name'] = 'Product';
$_['column_model'] = 'Model';
$_['column_quantity'] = 'number';
$_['column_price'] = 'Price';
$_['column_total'] = 'total';

// Entry
$_['entry_account'] = 'Account:';
$_['entry_coupon'] = 'apply coupon';
$_['entry_voucher'] = 'Use certificate';
$_['entry_reward'] = 'to Use bonus points';
$_['entry_email'] = 'Email ( email ):';
$_['entry_password'] = 'Password:';

// Error
$_['error_required'] = 'required Field';
$_['error_warning'] = 'There was a problem while trying to process your order! If the problem persists please try selecting a different payment method or you can contact the store owner by <a href="%s">clicking here</a>.';
$_['error_login'] = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_approved'] = 'Note: Your account must be approved before you can login.';
$_['error_exists'] = 'Note: This email address is already registered!';
$_['error_firstname'] = 'Your name must be between 1 and 32 characters!';
$_['error_lastname'] = 'Your last name must be between 1 and 32 characters!';
$_['error_email'] = 'the Email address contains an error!';
$_['error_telephone'] = 'Telephone must be between 3 and 32 characters!';
$_['error_fax'] = 'Fax must be between 3 and 32 characters!';
$_['error_company'] = 'Company must be between 3 and 32 characters!';
$_['error_password'] = 'Password must be between 3 and 20 characters!';
$_['error_confirm'] = 'password Confirmation does not match password!';
$_['error_company_id'] = 'Specify OKPO Company!';
$_['error_tax_id'] = 'Specify the INN!';
$_['error_vat'] = 'VAT number is invalid!';
$_['error_address_1'] = 'Address 1 must be between 3 and 128 characters!';
$_['error_address_2'] = 'Address 2 must be between 3 and 128 characters!';
$_['error_postcode'] = 'postcode must be between 2 and 10 characters!';
$_['error_country'] = 'select a country!';
$_['error_zone'] = 'choose a region / state!';
$_['error_city'] = 'Enter the city!';
$_['error_agree'] = 'Error: You must accept the "%s"!';
$_['error_address'] = 'Error: You must specify a address!';
$_['error_shipping'] = 'Error: you do Not specify a delivery method!';
$_['error_no_shipping'] = 'Error: No suitable method of delivery. Contact <a href="%s">us</a> to solve!';
$_['error_payment'] = 'Error: do Not specify a payment method!';
$_['error_no_payment'] = 'Error: No suitable method of payment. Contact <a href="%s">us</a> to solve!';
$_['error_coupon'] = 'Error: Coupon is invalid, expired or reached the limit of its use!';
$_['error_voucher'] = 'Error: the Gift certificate is invalid or the balance has already been used!';
$_['error_maximum'] = 'Error: The maximum number of points that can be applied is %s!';
$_['error_reward'] = 'Error: Enter the number of bonus points to use!';
$_['error_points'] = "Error: you don't Have %s reward points!";
$_['error_shipping'] = 'Specify the delivery!';
$_['error_min_amount'] = 'Minimum order amount: %s';
$_['error_validation'] = 'Error: Carefully check the values of the fields';
$_['error_upload_file'] = 'You can attach only an image, a text file, a word document or an excel document!А';

// buttons
$_['button_checkout'] = 'Confirm order';

$_['entry_edit_order'] = 'Edit order';
$_['text_shopping_cart'] = 'shopping Cart';
$_['text_watched'] = 'Viewed products';
$_['text_wishlist'] = 'featured products';
$_['text_orders'] = 'order History';
$_['text_favorite'] = 'Favorites';
$_['text_product'] = 'Product';
$_['text_price'] = 'Value';
$_['text_pick_yourself'] = 'Pick up yourself';
$_['text_order_delivery'] = 'delivered';
