<?php

// Heading
$_['heading_title'] = 'My Wish List';

// Text
$_['text_account'] = 'Account';
$_['text_instock'] = 'In Stock';
$_['text_wishlist'] = 'Wish List (%s)';
$_['text_login'] = 'You must <a href="%s">login</a> or <a href="%s">create an account</a> to save <a href="%s">%s</a> to your <a href="%s">wish list</a>!';
$_['text_success'] = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">wish list</a>!';
$_['text_exists'] = '<a href="%s">%s</a> already exist in Your <a href="%s">wish list</a>!';
$_['text_remove'] = 'Success: You have modified your wish list!';
$_['text_empty'] = 'Your watched list is empty.';
$_['text_quantity'] = 'Qty:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model'] = 'Product Code:';
$_['text_points'] = 'Reward Points:';
$_['text_price'] = 'Price:';
$_['text_tax'] = 'Ex Tax:';
$_['text_compare'] = 'compare';
$_['text_sort'] = 'Sort By:';
$_['text_default'] = 'Default';
$_['text_name_asc'] = 'Name (A - Z)';
$_['text_name_desc'] = 'Name (Z - A)';
$_['text_price_asc'] = 'Price (Low &gt; High)';
$_['text_price_desc'] = 'Price (High &gt; Low)';
$_['text_rating_asc'] = 'Rating (Lowest)';
$_['text_rating_desc'] = 'Rating (Highest)';
$_['text_model_asc'] = 'Model (A - Z)';
$_['text_model_desc'] = 'Model (Z - A)';
$_['text_limit'] = 'Show:';

// Button
$_['button_table'] = 'Table';
