<?php

// Heading
$_['heading_title'] = 'My Account';

// Text
$_['text_account'] = 'Account';
$_['text_my_account'] = 'My Account';
$_['text_my_orders'] = 'My Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit'] = 'Edit';
$_['text_password'] = 'Change your password';
$_['text_address'] = 'Modify your address book entries';
$_['text_wishlist'] = 'Modify your wish list';
$_['text_order'] = 'View your order history';
$_['text_download'] = 'Downloads';
$_['text_reward'] = 'Reward Points:';
$_['text_return'] = 'View your return requests';
$_['text_transaction'] = 'Your Transactions';
$_['text_newsletter'] = 'Subscribe to newsletter:';
$_['text_recurring'] = 'Recurring payments';
$_['text_transactions'] = 'Transactions';
$_['text_name'] = 'Name:';
$_['text_phone'] = 'Phone:';
$_['text_email'] = 'Email:';
$_['text_password'] = 'Password:';
$_['text_shipping'] = 'Shippping:';
