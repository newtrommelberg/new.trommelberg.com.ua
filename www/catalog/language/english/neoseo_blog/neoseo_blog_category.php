<?php
// Heading
$_['blog_heading']					  = 'Blog';
$_['heading_search']					= 'Blog search';

// Buttons
$_['button_continue']	      = 'Continue Reading';
$_['button_submit']			  = 'Submit';
$_['text_more']               = 'Details';
$_['text_autor']              = 'Author: ';
$_['show_more_before']        = 'view more';
$_['show_more_after']         = 'view less';

// Text
$_['text_comments']					  = ' Comments';
$_['text_comment']					  = ' Comment';
$_['text_tax']                = 'Ex Tax:';
$_['text_sort']               = 'Sort By:';
$_['text_default']            = 'Default';
$_['text_name_asc']           = 'Name (A - Z)';
$_['text_name_desc']          = 'Name (Z - A)';
$_['text_date_modified_asc']  = 'Date (old first)';
$_['text_date_modified_desc'] = 'Latest';
$_['text_limit']              = 'Show:';
$_['text_sub_categories']     = 'Subcategories:';
$_['text_search']             = 'Search';
$_['text_viewed']             = ' %s';

// Error
$_['text_category_error']			= 'Category not found!';
$_['text_author_error']				= 'Author not found!';
$_['text_article_error']			= 'Article not found!';
$_['text_empty']  					  = 'Articles not found!';
