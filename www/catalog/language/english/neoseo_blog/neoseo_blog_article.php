<?php
// Heading
$_['blog_heading']            = 'Blog';

// Buttons
$_['button_continue']         = 'Continue Reading';
$_['button_submit']           = 'Submit';
$_['button_cart']             = 'In cart';
// Text
$_['text_comments']           = ' Comments';
$_['text_comment']            = ' Comment';
$_['text_tax']                = 'Ex Tax:';
$_['text_sort']               = 'Sort By:';
$_['text_default']            = 'Default';
$_['text_name_asc']           = 'Name (A - Z)';
$_['text_name_desc']          = 'Name (Z - A)';
$_['text_date_modified_asc']  = 'Date (old first)';
$_['text_date_modified_desc'] = 'Latest';
$_['text_limit']              = 'Show:';
$_['text_sub_categories']     = 'Subcategories:';
$_['text_search']             = 'Search';
$_['text_related_articles']  = 'Related articles';
$_['text_related_products']  = 'Related products';
$_['text_viewed']             = "Viewed: %s";
$_['text_author']            = 'Author:';
// Error
$_['text_category_error']     = 'Category not found!';
$_['text_author_error']       = 'Author not found!';
$_['text_article_error']      = 'Article not found!';
$_['text_empty']              = 'Articles not found!';
