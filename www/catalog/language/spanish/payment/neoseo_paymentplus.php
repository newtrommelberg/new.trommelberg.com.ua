<?php
// Text
$_['text_title']       = 'Transferencia bancaria';
$_['text_description'] = 'Transferencia bancaria';
$_['text_instruction']			= 'Instrucciones de pago';
$_['text_payment']				= 'Su orden no será enviada hasta que la tienda reciba el pago.';