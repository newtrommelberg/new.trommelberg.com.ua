<?php
// Heading
$_['heading_title']   = 'Vista rápida del producto';

// Button
$_['button_continue'] = 'Seguir comprando';

// Text
$_['text_price'] = 'Precio';


// TimeFlicr
$_['text_days'] = 'Días';
$_['text_hours'] = 'Horas';
$_['text_minutes'] = 'Minutos';
$_['text_seconds'] = 'Segundos';
