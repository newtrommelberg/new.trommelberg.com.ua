<?php
// Heading
$_['heading_title']   = 'Orden rápida';
$_['text_quickorder']   = 'Su teléfono';

// Text
$_['text_no_phone'] = 'Ingrese su número de contacto';
$_['text_name'] = 'Tu nombre';
$_['text_phone'] = 'Número de contacto';
$_['text_price'] = 'Precio';

// Button
$_['button_quickorder'] = 'Orden rápida';
$_['button_continue'] = 'Seguir comprando';
$_['button_action'] = 'Orden';
