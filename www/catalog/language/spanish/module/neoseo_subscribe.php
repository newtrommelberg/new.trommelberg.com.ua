<?php
// Encabezado
$_['heading_title'] = 'Suscripción!';

// Texto
$_['text_title'] = 'Suscríbase ahora';
$_['text_email'] = 'Tu E-mail';
$_['text_success'] = 'Gracias por suscribirse a nuestro boletín!';
$_['text_empty'] = 'Ingresa una dirección de correo electrónico y vuelve a intentarlo';
$_['text_wrong'] = 'Ingrese el correo electrónico correcto y vuelva a intentarlo';

// Botón
$_['button_action'] = 'Enviar';

// error
$_['error_exist'] = 'Advertencia! No te has suscrito ';