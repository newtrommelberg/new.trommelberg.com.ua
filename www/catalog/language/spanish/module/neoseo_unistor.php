<?php
// Text
$_['text_social']           = 'Inicie sesión con su perfil social:';
$_['text_quickview']        = 'Vista rápida';
$_['prev_text']             = 'Anterior';
$_['next_text']             = 'Siguiente';
$_['text_read_more']        = 'Lee mas';
$_['text_read_less']        = 'Leer menos';
$_['text_instock']          = 'En stock';
$_['text_no_phone']         = 'Ingrese su teléfono';
$_['text_dimension']        = 'Dimensiones (L x An x Al):';
$_['text_watched']          = 'Productos vistos';
$_['text_compare']          = 'Comprar';
$_['text_watched_menu']     = 'Productos vistos';
$_['text_compare_menu']     = 'Comprar';
$_['text_all_categories']   = 'All categories';
$_['text_wishlist']         = 'Novedades';
$_['text_callback']         = 'Llamar de vuelta';
$_['text_price']            = 'Precio: ';
$_['text_subscribe_title']  = 'Learn about new promotions and latest products';
$_['text_subscribe_par']    = 'Y recibe noticias sobre promociones y ofertas especiales';
$_['text_payments_title']   = 'Aceptamos';
$_['text_tax']              = 'Ex Tax:';
$_['text_work_hours']       = 'Horas laborales:';
$_['text_information']      = 'Información';
$_['text_callback']         = 'Llamar de vuelta';
$_['text_social']           = 'Ingrese con';
$_['text_need_account']     = 'Usted no tiene cuenta?';
$_['text_or']               = '<span>o</span>';
$_['text_enter']            = 'Entrar en la tienda';
$_['text_forgotten']        = 'Lo olvidé';
$_['text_password']         = 'Contraseña';
$_['text_email']            = 'Email';
$_['text_track_order']      = 'Orden de pista';
$_['text_category']         = 'Catálogo';
$_['text_items']            = '<small>%s</small><b class="cb1">Carro</b><br><span class="csp2">%s</span><br><a>revisa</a>';
$_['text_menu_name']        = 'Catálogo';
$_['text_sku']        		= 'Sku:';
$_['text_weight']        	= 'Weight:';
$_['text_manufacturer']     = 'Manufacturer:';
$_['text_model']        	= 'Código de producto:';
$_['text_read_more']        = 'Leer más';
$_['text_read_less']        = 'Esconder';
$_['text_all_reviews']      = 'Show all reviews';

// Button
$_['button_update']         = 'Guardar';
$_['button_write']          = 'Escribir';
$_['button_subscribe']      = 'Enviar';
$_['button_table']          = 'Tabla';
$_['button_search']         = 'SEARCH';

// Entry
$_['entry_email']           = 'E-mail';

// TimeFlicr
$_['text_days'] = 'Días';
$_['text_hours'] = 'Horas';
$_['text_minutes'] = 'Minutos';
$_['text_seconds'] = 'Segundos';

//Menu
$_['main_menu_category_quantity'] = 'Total de categorias: ';
