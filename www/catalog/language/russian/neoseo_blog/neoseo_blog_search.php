<?php
// Heading
$_['heading_title']     = 'Поиск';

// Text
$_['text_search']   = 'Статьи отвечающие критериям поиска';
$_['text_keyword']  = 'Ключевые слова';
$_['text_category'] = 'Все категории';
$_['text_sub_categories'] = 'Подкатегории:';
$_['text_empty'] = 'Нет результатов';
$_['text_viewed']  = 'Просмотров: %s';

// Entry
$_['entry_search']  = 'Поиск';