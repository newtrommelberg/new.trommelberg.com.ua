<?php

// Text
$_['text_category_articles'] = 'Статьи по теме:';
$_['text_more_articles']     = 'Все статьи раздела'; 
$_['text_empty']             = 'Статьи не найдены!';
$_['text_related_articles']  = 'Связанные статьи:';
$_['text_related_products']  = 'Товары из статьи:';
$_['text_more']              = 'Подробнее';
$_['text_group_argo']        = 'Для участников Арго:';
$_['text_points']            = 'Бонусные Баллы:';
$_['text_reward']            = 'Бонусные баллы:';
$_['text_author']            = 'Автор:';
$_['text_tax']               = 'Без налога:';
$_['text_instock']           = 'В наличии';
$_['text_viewed']            = " %s";
$_['text_comments']          = ' ';
$_['text_comment']           = ' Комментарий';

//Button
$_['button_continue']         = 'Продолжить';
$_['button_submit']           = 'Отправить';
$_['button_cart']             = 'В корзину';

// Error
$_['text_category_error']     = 'Категория не найдена!';
$_['text_author_error']       = 'Автора не найден!';
$_['text_article_error']      = 'Статья не найдена';
$_['text_empty']              = 'Статьи не найдены!';