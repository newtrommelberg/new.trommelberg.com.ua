<?php
// Heading
$_['heading_title']   = 'Подписка оформлена!';

// Text
$_['text_title']   = 'Подпишись сейчас';
$_['text_email'] = 'Ваш E-mail';
$_['text_name'] = 'Введите свое имя';
$_['text_success'] = 'Спасибо что подписались на нашу рассылку!';
$_['text_empty'] = 'Укажите email и повторите попытку';
$_['text_wrong'] = 'Укажите правильный email и повторите попытку';

// Button
$_['button_action'] = '&rarr;';

// Error
$_['error_exist'] = 'Внимание! Подписка не оформлена';