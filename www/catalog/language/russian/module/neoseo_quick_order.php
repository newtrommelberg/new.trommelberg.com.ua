<?php

// Heading
$_['heading_title'] = 'Быстрый заказ';
$_['text_quick_order'] = 'Ваш телефон';

// Text
$_['text_name'] = 'Ваше имя';
$_['text_phone'] = 'Контактный телефон';
$_['text_price'] = 'Цена';
$_['text_no_phone'] = 'Укажите телефон';
$_['text_agree'] = 'Я прочитал и согласен с <a class="colorbox" href="%s" target="_blank">%s</a>';

// Button
$_['button_quick_order'] = 'Быстрый заказ';
$_['button_continue'] = 'Продолжить покупки';
$_['button_action'] = 'Заказать';
