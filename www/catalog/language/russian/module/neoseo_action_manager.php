<?php

// Heading
$_['actions_heading'] = 'Акции';

$_['action_status_list'][0] = 'Подарок';
$_['action_status_list'][1] = 'Скидка';
$_['action_status_list'][2] = '';

$_['action_status_data'][0] = 'Подарок';
$_['action_status_data'][1] = 'Скидка';
$_['action_status_data'][2] = '';

$_['read_more'] = 'Подробнее';
$_['till_finish'] = 'До окончания акции';
$_['action_finish'] = 'Акция закончилась';


$_['days_left'] = 'Дней';
$_['hours_left'] = 'Часов';
$_['minutes_left'] = 'Мин.';
$_['seconds_left'] = 'Сек.';

$_['text_action_products'] = 'Товары, участвующие в акции';
