<?php

// Heading
$_['heading_title'] = 'Спасибо за заявку!';

// Text
$_['text_name'] = "Ваше имя";
$_['text_email'] = 'E-mail';
$_['text_phone'] = 'Телефон';

// Button
$_['button_continue'] = 'Продолжить покупки';
$_['button_action'] = 'Оставить заявку';
$_['button_unsubscribe'] = 'Отказаться';
$_['button_subscribe'] = 'Сообщить';