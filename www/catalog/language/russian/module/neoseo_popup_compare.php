<?php

// Heading
$_['heading_title'] = 'Продукт добавлен в сравнение!';

// Column
$_['column_id'] = '#';
$_['column_name'] = 'Продукт';
$_['column_price'] = 'Цена';
$_['column_action'] = '';

// Button
$_['button_continue'] = 'Продолжить покупки';
$_['button_action'] = 'Перейти к сравнению';
$_['button_delete'] = 'Удалить из списка';
