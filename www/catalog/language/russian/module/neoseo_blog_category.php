<?php
// Heading
$_['blog_heading']            = 'Блог';
$_['heading_search']          = 'Поиска по блогу';

// Buttons
$_['button_continue']         = 'Читать';
$_['button_submit']           = 'Отправить';

// Text
$_['text_comments']           = ' Комментариев';
$_['text_comment']            = ' Комментарий';
$_['text_tax']                = 'Налог:';
$_['text_sort']               = 'Сортировка:';
$_['text_default']            = 'По умолчанию';
$_['text_name_asc']           = 'Название (А - Я)';
$_['text_name_desc']          = 'Название (Я - А)';
$_['text_date_modified_desc'] = 'Последние';
$_['text_limit']              = 'Показать:';
$_['text_sub_categories']     = 'Подкатегории:';
$_['text_search']             = 'Поиск';
	
// Error
$_['text_category_error']     = 'Категория не найдена!';
$_['text_author_error']       = 'Автор не найден!';
$_['text_article_error']      = 'Статья не найдена!';
$_['text_empty']              = 'Статьи не найдены!';