<?php

// Heading
$_['heading_title'] = 'Отзывы покупателей';

$_['text_average'] = 'Рейтинг:';
$_['text_stars'] = '%s из 5 !';
$_['text_no_rating'] = 'Без рейтинга';

// Text
$_['text_error'] = 'Нет ни одного отзыва!';
$_['text_empty'] = 'Нет ни одного отзыва!';
$_['text_pagination'] = 'Отзывы с {start} по {end} из {total} (всего страниц - {pages})';
$_['text_showall'] = 'Показать все отзывы';
$_['text_write'] = 'Написать отзыв';
$_['text_author'] = 'Автор: %s ';

// Text
$_['text_message'] = '<p>Ваш отзыв успешно отправлен!</p>';
$_['text_note'] = '<span style="color: #FF0000;">Важно:</span> HTML не переводиться!';
$_['text_average'] = 'Оценка:';
$_['text_stars'] = '%s из 5!';
$_['text_conditions'] = 'Здесь Вы можете оставить свой отзыв о работе магазина или задать вопрос.';
$_['text_add'] = 'Появились новые отзывы.';
$_['admin_answer'] = 'Администратор магазина';

// Entry Fields
$_['entry_name'] = 'Имя:';
$_['entry_yt'] = 'Ссылка на видеотзыв (YouTube):';
$_['entry_user_image'] = 'Загрузить картинку: ';
$_['entry_yt_desc'] = 'https://www.youtube.com/watch?v=jFWUHiJ0qtw';
$_['entry_email'] = 'E-Mail:';
$_['entry_title'] = 'Тема:';
$_['entry_enquiry'] = 'Сообщение:';
$_['entry_captcha'] = 'Введите код:';
$_['entry_rating'] = 'Рейтинг:';
$_['entry_good'] = 'Отлично';
$_['entry_bad'] = 'Плохо';




// Email
$_['email_subject'] = 'Запрос %s';

// Errors
$_['error_name'] = 'Имя должно быть от 3 до 32 символов!';
$_['error_title'] = 'Тема должна быть от 3 до 64 символов!';
$_['error_email'] = 'Неправильный E-Mail!';
$_['error_youtube'] = 'Не верный формат YouTube ссылки!';
$_['error_enquiry'] = 'Сообщение должно быть от 25 до 1000 символов!';
$_['error_captcha'] = 'Проверочный код не совпадает!';
?>