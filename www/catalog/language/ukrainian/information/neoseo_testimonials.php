<?php
// Heading
$_['heading_title'] = 'Відгуки покупців';

$_['text_average'] = 'Рейтинг:';
$_['text_stars'] = '%s з 5 !';
$_['text_no_rating'] = 'Без рейтингу';

// Text
$_['text_error'] = 'Немає жодного відкликання!';
$_['text_empty'] = 'Немає жодного відкликання!';
$_['text_pagination'] = 'Відгуки {start} {end} з {total} (всього сторінок - {pages})';
$_['text_showall'] = 'Показати всі відгуки';
$_['text_write'] = 'Написати відгук';
$_['text_author'] = 'Автор: %s ';

// Text
$_['text_message'] = '<p>Ваш відгук успішно відправлено!</p>';
$_['text_note'] = '<span style="color: #FF0000;">Важливо:</span> HTML не переводитися!';
$_['text_average'] = 'Оцінка';
$_['text_stars'] = '%s з 5!';
$_['text_conditions'] = 'Тут Ви можете залишити свій відгук про роботу магазину або задати запитання.';
$_['text_add'] = "З'явилися нові відгуки.";
$_['admin_answer'] = 'Адміністратор магазину';

// Entry Fields
$_['entry_name'] = "Ім'я";
$_['entry_yt'] = 'Посилання на видеотзыв (YouTube):';
$_['entry_user_image'] = 'Завантажити картинку: ';
$_['entry_yt_desc'] = 'https://www.youtube.com/watch?v=jFWUHiJ0qtw';
$_['entry_email'] = 'E-Mail:';
$_['entry_title'] = 'Тема:';
$_['entry_enquiry'] = 'Повідомлення';
$_['entry_captcha'] = 'Введіть пароль:';
$_['entry_rating'] = 'Рейтинг:';
$_['entry_good'] = 'Відмінно';
$_['entry_bad'] = 'Погано';




// Email
$_['email_subject'] = 'Запит на %s';

// Errors
$_['error_name'] = "Ім'я повинно містити від 3 до 32 символів!";
$_['error_title'] = 'Тема повинна бути від 3 до 64 символів!';
$_['error_email'] = 'Неправильний E-Mail!';
$_['error_youtube'] = 'Не вірний формат YouTube посилання!';
$_['error_enquiry'] = 'Повідомлення повинно бути від 25 до 1000 символів!';
$_['error_captcha'] = 'код перевірки не співпадає!';

?>