<?php

// Heading
$_['heading_title'] = 'Авторизація';

// Text
$_['text_enter_social'] = 'Увійти за допомогою соц.сетей';
$_['text_remember_me'] = 'Запам\'ятати мене';
$_['text_account'] = 'Особистий Кабінет';
$_['text_login'] = 'Авторизація';
$_['text_new_customer'] = 'Новий клієнт';
$_['text_register'] = 'Реєстрація';
$_['text_i_am_returning_customer'] = 'Вхід в особистий кабінет';
$_['text_forgotten'] = 'Забули пароль?';

// Entry
$_['entry_email'] = 'E-Mail';
$_['entry_password'] = 'Пароль';

// Error
$_['error_login'] = 'Неправильно заповнені поле E-Mail і/або пароль!';
$_['error_attempts'] = 'Увага: Ви перевищели ліміт спроб входу в систему. Будьласка, спробуйте через час.';
$_['error_approved'] = 'Необхідно підтвердити аккаунт перед авторизацією.';
