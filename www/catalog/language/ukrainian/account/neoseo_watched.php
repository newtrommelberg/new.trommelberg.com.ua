<?php

// Heading
$_['heading_title'] = 'Переглянуті товари';

// Text
$_['text_account'] = 'Особистий Кабінет';
$_['text_instock'] = 'В наявності';
$_['text_wishlist'] = 'Обрані';
$_['text_remove'] = 'Список переглянутих товарів успішно змінений!';
$_['text_empty'] = 'У вас нема переглянутих товарів';
$_['text_quantity'] = 'Кількість:';
$_['text_manufacturer'] = 'Виробник:';
$_['text_model'] = 'Модель:';
$_['text_points'] = 'Бонусні бали:';
$_['text_price'] = 'Ціна:';
$_['text_tax'] = 'Без ПДВ:';
$_['text_compare'] = 'в порівняння';
$_['text_sort'] = 'Сортування:';
$_['text_default'] = 'За умовчанням';
$_['text_name_asc'] = 'Назва (А-Я)';
$_['text_name_desc'] = 'Назва (Я-А)';
$_['text_price_asc'] = 'Ціна (низька &gt; висока)';
$_['text_price_desc'] = 'Ціна (висока &gt; низька) ';
$_['text_rating_asc'] = 'Рейтинг (починаючи з низького)';
$_['text_rating_desc'] = 'Рейтинг (починаючи з високого)';
$_['text_model_asc'] = 'Модель (А - Я)';
$_['text_model_desc'] = 'Модель (Я - А)';
$_['text_limit'] = 'Показати:';

// Button
$_['button_table'] = 'Таблиця';
