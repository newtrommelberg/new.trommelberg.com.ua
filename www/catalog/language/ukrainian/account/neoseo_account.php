<?php

// Heading
$_['heading_title'] = 'Особистий Кабінет';

// Text
$_['text_account'] = 'Особистий Кабінет';
$_['text_my_account'] = 'Мій обліковий запис';
$_['text_my_orders'] = 'Мої замовлення';
$_['text_my_newsletter'] = 'Підписка';
$_['text_edit'] = 'Змінити контактну інформацію';
$_['text_password'] = 'Змінити свій пароль';
$_['text_address'] = 'Змінити мої адреси';
$_['text_wishlist'] = 'Обрані товари';
$_['text_order'] = 'Історія замовлень';
$_['text_download'] = 'Файли для скачування';
$_['text_reward'] = 'Бонусні балиЖ';
$_['text_return'] = 'Запити на повернення';
$_['text_transaction'] = 'Історія транзакцій';
$_['text_newsletter'] = 'Підписка на новини:';
$_['text_recurring'] = 'Періодичні платежі';
$_['text_transactions'] = 'Транзакції';
$_['text_name'] = 'Ім’я:';
$_['text_phone'] = 'Телефон:';
$_['text_email'] = 'Email:';
$_['text_password'] = 'Пароль:';
$_['text_shipping'] = 'Адреса доставки:';


