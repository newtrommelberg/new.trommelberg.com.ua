<?php

// Heading
$_['heading_title'] = 'Оформлення замовлення';
$_['heading_title_login'] = 'Авторизація';


// Text
$_['text_checkout_option'] = 'Авторизація';
$_['text_checkout_account'] = 'Покупець';
$_['text_checkout_payment_address'] = 'Деталі платежів';
$_['text_checkout_shipping_method'] = 'Доставка і оплата';
$_['text_checkout_payment_method'] = 'Оплата';
$_['text_checkout_confirm'] = 'Підтвердити замовлення';
$_['text_modify'] = 'Modify &raquo;';
$_['text_new_customer'] = 'Новий покупець';
$_['text_returning_customer'] = 'Постійний покупець';
$_['text_checkout'] = 'Опції оформленняя замовлення:';
$_['text_i_am_returning_customer'] = 'Постійний покупець';
$_['text_register'] = 'Зареєструватися';
$_['text_guest'] = 'Замовити без реєстрації';
$_['text_register_account'] = 'Створивши обліковий запис, ви зможете робити покупки швидше, бути в курсі стану замовлень і відслідковувати замовлення, які ви робили раніше.';
$_['text_forgotten'] = 'Забули пароль';
$_['text_your_details'] = 'Ваші особисті дані';
$_['text_your_address'] = 'Ваша адреса';
$_['text_your_password'] = 'Ваш пароль';
$_['text_agree'] = 'Я прочитав і згоден з <a class="colorbox" href="%s">%s</a>';
$_['text_shipping_method'] = 'Спосіб доставки:';
$_['text_payment_method'] = 'Спосіб оплати:';
$_['text_comments'] = 'Примітка до замовлення';
$_['text_create_account'] = 'Зареєструватися';
$_['text_please_wait'] = 'Очікуйте...';
$_['text_coupon'] = 'Купон для знижки був вдало використаний!';
$_['text_voucher'] = 'Сертифікат-подарунок був вдало використаний!';
$_['text_reward'] = 'Бонусні бали були вдало застосовані!';
$_['text_use_coupon'] = 'Використати';
$_['text_use_voucher'] = 'Використати';
$_['text_use_reward'] = 'Використати';
$_['text_cart'] = 'Товари у кошику';
$_['text_image'] = 'Зображення';
$_['text_name'] = 'Продукт';
$_['text_quantity'] = 'Кількість';
$_['text_price'] = 'Ціна';
$_['text_total'] = 'Разом';
$_['text_action'] = 'Дії';
$_['text_items'] = '%s';
$_['text_shipping'] = 'Доставка';
$_['text_country'] = 'Країна:';
$_['text_region'] = 'Регіон:';
$_['text_city'] = 'Місто:';
$_['text_payment'] = 'Оплата:';
$_['text_login'] = 'Я постійний покупець';
$_['text_сontact_details'] = 'Контактні дані';
$_['text_delivery_payment_details'] = 'Спосіб доставки та оплати';
$_['text_edit'] = 'Редагувати';
$_['text_not_have_account'] = 'Я новий покупець';
$_['text_next'] = 'Далі';
$_['text_unit'] = 'шт x';
$_['text_files_upload'] = 'Вибрати файли';
$_['text_contact_info'] = 'Контактні дані';

// Column
$_['column_name'] = 'Продукт';
$_['column_model'] = 'Модель';
$_['column_quantity'] = 'Кількість';
$_['column_price'] = 'Ціна';
$_['column_total'] = 'Разом';

// Entry
$_['entry_account'] = 'Обліковий запис:';
$_['entry_coupon'] = 'Використати купон';
$_['entry_voucher'] = 'Використати сертифікат';
$_['entry_reward'] = 'Використати бонусні бали';
$_['entry_email'] = 'Електронна адреса ( email ):';
$_['entry_password'] = 'Пароль:';

// Error
$_['error_required'] = 'Поле обов\'язкове для заповнення';
$_['error_warning'] = 'There was a problem while trying to process your order! If the problem persists please try selecting a different payment method or you can contact the store owner by <a href="%s">clicking here</a>.';
$_['error_login'] = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_approved'] = 'Увага: Ваш обліковий запис має бути затверджений раніше ніж ви зможете авторизуватись.';
$_['error_exists'] = 'Увага: Електронна адреса вже використовується!';
$_['error_firstname'] = 'Ваше ім\'я має бути від 1 до 32 символів!';
$_['error_lastname'] = 'Ваше прізвище має бути від 1 до 32 символів!';
$_['error_email'] = 'Електронна адреса введена з помилками!';
$_['error_telephone'] = 'Телефон має бути від 3 до 32 символів!';
$_['error_fax'] = 'Факс має бути від 3 до 32 символів!';
$_['error_company'] = 'Назва компанії має бути від 3 до 32 символів!';
$_['error_password'] = 'Пароль має бути від 3 до 20 символів!';
$_['error_confirm'] = 'Підтвердження пароля не співпадає з паролем!';
$_['error_company_id'] = 'Вкажіть ОКПО Компанії!';
$_['error_tax_id'] = 'Вкажіть ІНН!';
$_['error_vat'] = 'VAT number is invalid!';
$_['error_address_1'] = 'Адреса 1 має бути від 3 до 128 символів!';
$_['error_address_2'] = 'Адреса 2 має бути від 3 до 128 символів!';
$_['error_postcode'] = 'Поштовий індекс має бути від от 2 до 10 символів!';
$_['error_country'] = 'Вкажіть країну!';
$_['error_zone'] = 'Вкажіть регіон / область!';
$_['error_city'] = 'Вкажіть місто!';
$_['error_agree'] = 'Помилка: Ви маєте прийняти "%s"!';
$_['error_address'] = 'Помилка: Ви маєте вказати адресу!';
$_['error_shipping'] = 'Помилка: Не вказаний метод доставки!';
$_['error_no_shipping'] = 'Помилка: Немає підходящого методу доставки. Зв\'яжіться з <a href="%s">нами</a> для вирішення проблеми!';
$_['error_payment'] = 'Помилка: Не вказаний метод оплати!';
$_['error_no_payment'] = 'Помилка: Немає підходящого методу оплати. Зв\'яжіться з <a href="%s">нами</a> для вирішення проблеми!';
$_['error_coupon'] = 'Помилка: Купон не вірний, прострочений, або досягнений ліміт його використання!';
$_['error_voucher'] = 'Помилка: Сертифікат-подарунок не вірний, або його баланс вже був використаний!';
$_['error_maximum'] = 'Помилка: Ви можете використувати не більше %s бонусних балів!';
$_['error_reward'] = 'Помилка: Введіть число бонусних балів для використання!';
$_['error_points'] = 'Помилка: У вас немає %s бонусних балів!';
$_['error_shipping'] = 'Вкажіть доставку!';
$_['error_min_amount'] = 'Мінімальна сума замовлення: %s';
$_['error_validation'] = 'Помилка: Уважно перевірте значення полів';
$_['error_upload_file'] = 'Вы можете прикрепить только изображение, текстовый файл, word-документ либо excel-документ!';

// buttons
$_['button_checkout'] = 'Підтвердити замовлення';

$_['entry_edit_order'] = 'Редагувати замовлення';
$_['text_shopping_cart'] = 'Кошик покупок';
$_['text_watched'] = 'Переглянуті товари';
$_['text_wishlist'] = 'Обрані товари';
$_['text_orders'] = 'Исторія замовлень';
$_['text_favorite'] = 'Улюблені товари';
$_['text_product'] = 'Товар';
$_['text_price'] = 'Вартість';
$_['text_pick_yourself'] = 'Забрати самостійно';
$_['text_order_delivery'] = 'Замовити доставку';
