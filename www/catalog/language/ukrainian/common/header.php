<?php

// Text
$_['text_home']          = 'Головна';
$_['text_wishlist']      = 'Обрані (%s)';
$_['text_shopping_cart'] = 'Кошик';
$_['text_category']      = 'Категорії';
$_['text_account']       = 'Особистий Кабінет';
$_['text_register']      = 'Реєстрація';
$_['text_login']         = 'Авторизація';
$_['text_order']         = 'Історія замовлень';
$_['text_transaction']   = 'Транзакції';
$_['text_download']      = 'Завантаження';
$_['text_logout']        = 'Вихід';
$_['text_checkout']      = 'Оформлення замовлення';
$_['text_search']        = 'Пошук';
$_['text_all']           = 'Дивитися Усі- ';
$_['text_menu']          = 'Меню';
$_['text_all_categories']= 'Усі категорії';
$_['text_hide']          = 'Приховати';
$_['text_in_youtube']    = 'Ми у Youtube';