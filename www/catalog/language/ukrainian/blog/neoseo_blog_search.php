<?php
// Heading
$_['heading_title'] = 'Пошук по блогу';

// Text
$_['text_search']   = 'Статті, які відповідають критеріям пошуку';
$_['text_keyword']  = 'Ключові слова';
$_['text_category'] = 'Всі категорії';
$_['text_empty'] = 'Нема результатів';
$_['text_viewed'] = "Переглядів: %s"; 
$_['text_more'] = 'Детальнiше';
$_['text_author'] = 'Автор: ';
 
// Entry
$_['entry_search']  = 'Пошук';