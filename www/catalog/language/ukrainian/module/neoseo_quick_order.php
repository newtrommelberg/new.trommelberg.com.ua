<?php

// Heading
$_['heading_title'] = 'Замовлення в 1 клiк';
$_['text_quick_order'] = 'Ваш телефон';

// Text
$_['text_name'] = 'Ваше ім\'я';
$_['text_phone'] = 'Контактний телефон';
$_['text_price'] = 'Ціна';
$_['text_no_phone'] = 'Вкажіть номер телефону';
$_['text_agree'] = 'Я прочитав і згоден з <a class="colorbox" href="%s" target="_blank">%s</a>';

// Button
$_['button_quick_order'] = 'Купити в 1 клiк';
$_['button_continue'] = 'Продовжити покупки';
$_['button_action'] = 'Заказати';
