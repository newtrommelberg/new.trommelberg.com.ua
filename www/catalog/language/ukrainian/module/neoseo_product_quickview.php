<?php

// Heading
$_['heading_title'] = 'Швидкий перегляд продукту';

// Button
$_['button_continue'] = 'Продовжити покупки';

// Text
$_['text_price'] = 'Цiна';

// TimeFlicr
$_['text_days'] = 'Днів';
$_['text_hours'] = 'Годин';
$_['text_minutes'] = 'Хвилин';
$_['text_seconds'] = 'Секунд';

