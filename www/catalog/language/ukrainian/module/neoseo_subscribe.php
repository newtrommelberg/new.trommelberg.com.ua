<?php
// Heading
$_['heading_title']   = 'Підписку оформлено!';

// Text
$_['text_title']   = 'Підпишіся зараз!';
$_['text_email'] = 'Ваш E-mail';
$_['text_name'] = 'Введіть своє ім’я';
$_['text_success'] = 'Дякуємо, що підписалися на нашу розсилку!';
$_['text_empty'] = 'Вкажіть email та спробуйте ще раз';
$_['text_wrong'] = 'Вкажіть правильний email і спробуйте ще раз';

// Button
$_['button_action'] = 'Відправити';

// Error
$_['error_exist'] = 'Увага! Підписку не оформлено';