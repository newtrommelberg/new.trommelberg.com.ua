<?php
$_['text_characteristics_title']              = 'Характеристики';
$_['text_product_action']                     = 'Акція! Супер ціни на акційний товар від нашого магазину!';
$_['text_follow_price']                       = 'Стежити за ціною';
$_['text_all_characteristics']                = 'Дивитися всі характеристики';
$_['text_see_all']                            = 'Дивитись все';
$_['text_collapse_all']                       = 'Згорнути';
$_['text_more']                               = 'Читати повнiстю';
$_['text_comment_answer']                     = 'Вiдповiсти';
$_['text_reviews_quantity']                   = 'Відгуки';
$_['text_give_feedback']                      = 'Залишити відгук';
$_['text_see_all_reviews']                    = 'Дивитися всі відгуки';
$_['text_shipping']                           = 'Доставка';
$_['text_payment']                            = 'Оплата';
$_['text_guarantee']                          = 'Гарантiя';

$_['text_social']           = 'Авторизуватися за допомогою соцiальноi мережi:';
$_['text_quickview']        = 'Швидкий перегляд';
$_['prev_text']             = 'Попередній';
$_['next_text']             = 'Наступний';
$_['text_read_more']        = 'Читати далі';
$_['text_read_less']        = 'Скрити';
$_['text_instock']          = 'У наявності';
$_['text_no_phone']         = 'Вкажіть номер телефону';
$_['text_dimension']        = 'Розміри (Д х Ш х В):';
$_['text_watched']          = 'Переглянуті товари';
$_['text_compare']          = 'Порівняння';
$_['text_wishlist']         = 'Обрані';
$_['text_compare_menu']     = 'Порівняння товарів';
$_['text_wishlist_menu']    = 'Вибрані товари';
$_['text_callback_menu']    = 'Передзвоніть мені';
$_['text_all_categories']   = 'Всі категорії';
$_['text_callback']         = 'Передзвоніть мені';
$_['text_price']            = 'Ціна: ';
$_['text_subscribe_title']  = 'Підпишись зараз';
$_['text_subscribe_par']    = 'Та отримуйте новини про акції та спеціальні пропозиції';
$_['text_payments_title']   = 'Ми приймаємо';
$_['text_tax']              = 'Без ПДВ:';
$_['text_work_hours']       = 'Ми працюємо:';
$_['text_information']      = 'Корисна інформація';
$_['text_callback']         = 'Передзвоніть мені';
$_['text_social']           = 'Зайдіть за допомогою соцмереж';
$_['text_need_account']     = 'У вас ще немає акаунту?';
$_['text_or']               = '<span>або</span>';
$_['text_enter']            = 'Вхід до магазину';
$_['text_forgotten']        = 'Я забув(ла)';
$_['text_password']         = 'Пароль';
$_['text_email']            = 'Электронна пошта';
$_['text_track_order']      = 'Відслідкувати замовлення';
$_['text_category']         = 'Каталог';
$_['text_items']            = '<div class="cart__total-products"><div class="cart__total-items">%s</div>товар (iв)</div><br><div class="cart__total-cost">%s</div>';
$_['text_menu_name']        = 'Каталог товарів';
$_['text_sku']        		= 'Артикул:';
$_['text_weight']        	= 'Вага:';
$_['text_manufacturer']     = 'Виробник:';
$_['text_model']        	= 'Код товару:';
$_['text_read_more']        = 'Читати повнiстю';
$_['text_read_less']        = 'Сховати';
$_['text_all_reviews']      = 'Останні відгуки';
$_['text_name_download']    = 'Назва';
$_['text_size_download']    = 'Розмір';
$_['text_date_added_download']    = 'Дата додавання';
$_['text_number_download']    = 'Номер';

$_['button_update']         = 'Зберегти';
$_['button_write']          = 'Написати';
$_['button_subscribe']      = 'Відправити';
$_['button_table']          = 'Таблиця';
$_['button_search']         = 'ПОШУК';

$_['entry_email']           = 'E-mail';

// TimeFlicr
$_['text_days'] = 'Днів';
$_['text_hours'] = 'Годин';
$_['text_minutes'] = 'Хвилин';
$_['text_seconds'] = 'Секунд';

//Menu
$_['main_menu_category_quantity'] = 'Всього категорій: ';

