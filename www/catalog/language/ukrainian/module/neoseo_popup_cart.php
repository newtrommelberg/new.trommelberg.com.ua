<?php

// Heading
$_['text_popup_cart_title'] = 'Продукт доданий до корзини!';
$_['text_subtotal'] = "Всього";

// Column
$_['column_id'] = '#';
$_['column_name'] = 'Продукт';
$_['column_quantity'] = 'Кількість';
$_['column_price'] = 'Ціна';
$_['column_action'] = '';

// Button
$_['button_continue'] = 'Продовжити покупки';
$_['button_checkout'] = 'Оформити замовлення';
$_['button_plus'] = 'Збільшити кількість товару';
$_['button_minus'] = 'Зменшити кількість товару';
$_['button_delete'] = 'Прибрати товар з корзини';

$_['message_cart_single'] = 'Товар успішно додано до корзини! Ви можете продовжити покупки або перейти до оформлення замовлення';
?>