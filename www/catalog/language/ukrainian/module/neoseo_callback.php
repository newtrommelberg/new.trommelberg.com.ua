<?php

// Heading
$_['heading_title'] = 'Замовити зворотній дзвінок';

// Text
$_['text_prompt'] = 'Вкажіть свої дані';
$_['text_name'] = 'Ваше ім\'я';
$_['text_phone'] = 'Контактний телефон';
$_['text_email'] = 'Email';
$_['text_message'] = 'Що Вас цікавить';
$_['text_title_finish'] = 'Дякуємо!';
$_['text_time1'] = 'Зручний час для дзвінка';
$_['text_time2'] = 'Передзвонюємо по буднях з 08:30 до 18:00. ';
$_['text_time_from'] = 'з';
$_['text_time_to'] = 'до';

// Button
$_['button_continue'] = 'Продовжити покупки';
$_['button_action'] = 'Очікую дзвінок';
