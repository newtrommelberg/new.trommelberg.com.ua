<?php
// Label
$_['label_name'] = 'за іменем';
$_['label_model'] = 'за модулем';
$_['label_sku'] = 'за артикулом';

// Text
$_['text_filter'] = 'Фільтрация по:';
$_['text_search_page'] = 'Сторінка пошуку:';
$_['text_look_results'] = 'Дивитись всі результати';