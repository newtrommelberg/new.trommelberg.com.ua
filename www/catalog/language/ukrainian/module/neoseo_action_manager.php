<?php

// Heading
$_['actions_heading'] = 'Акції';

$_['action_status_list'][0] = 'Подарунок';
$_['action_status_list'][1] = 'Скидка';
$_['action_status_list'][2] = '';

$_['action_status_data'][0] = 'Подарунок';
$_['action_status_data'][1] = 'Скидка';
$_['action_status_data'][2] = '';

$_['read_more'] = 'Детальніше';
$_['action_finish'] = 'Акція закінчилася';
$_['till_finish'] = 'До закінчення акції';


$_['days_left'] = 'Днів';
$_['hours_left'] = 'Годин';
$_['minutes_left'] = 'Хв.';
$_['seconds_left'] = 'Сек.';

$_['text_action_products'] = 'Товари, які беруть участь в акції';