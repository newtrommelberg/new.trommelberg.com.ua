<?php
// Heading
$_['blog_heading']            = 'Блог';

// Buttons
$_['button_continue']         = 'Читати';
$_['button_submit']           = 'Відправити';
$_['button_cart']             = 'В кошик'; 

// Text
$_['text_comments']           = ' Коментарів';
$_['text_comment']            = ' Коментар';
$_['text_tax']                = 'Податок:';
$_['text_sort']               = 'Сортування:';
$_['text_default']            = 'За замовчуванням';
$_['text_name_asc']           = 'Назва (А - Я)';
$_['text_name_desc']          = 'Назва (Я - А)';
$_['text_date_modified_desc'] = 'Останні';
$_['text_limit']              = 'Показати:';
$_['text_sub_categories']     = 'Підкатегорії:';
$_['text_search']             = 'Пошук';
$_['text_related_articles']   = 'Пов\'язані статті:';
$_['text_related_products']   = 'Товари зі статті:';
$_['text_viewed']             = "Переглядів: %s";
$_['text_author']            = 'Автор:';

// Error
$_['text_category_error']     = 'Категорія не знайдена!';
$_['text_author_error']       = 'Автор не знайдений!';
$_['text_article_error']      = 'Стаття не знайдена!';
$_['text_empty']              = 'Статті не знайдені!';