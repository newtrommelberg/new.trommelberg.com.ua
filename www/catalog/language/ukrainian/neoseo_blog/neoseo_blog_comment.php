<?php
// Entry
$_['entry_name']              = 'Ваше ім\'я:';
$_['entry_captcha']           = 'Введіть код в поле нижче:';
$_['entry_comment']           = 'Ваш коментар:';

$_['text_author_information'] = '(Автор)';
$_['text_posted_by']          = 'Додав';
$_['text_on']                 = 'про';
$_['text_updated']            = 'Оновлено';
$_['text_view_comment']       = 'Переглянути коментарі';
$_['text_write']              = 'Написати коментар';
$_['text_cancel_reply']       = 'відмінити відповідь';
$_['text_note']               = '<span class="text-danger">Увага:</span> HTML розмітка не підтримується!!';
$_['text_comments']           = 'Коментарі';
$_['text_comment']            = 'коментар';
$_['text_success']            = 'Дякуємо за Ваш коментар!';
$_['text_success_approval']   = 'Дякуємо за Ваш коментар. Він буде опублікований після перевірки!';
$_['text_wait']               = 'Почекайте';
$_['text_reply_comment']      = 'Відповісти';
$_['text_said']               = 'говорить:';
$_['text_authors']            = 'Автори';
$_['text_no_comments']        = 'Ще ніхто нічого не написав, ви можете стати першими!';
$_['entry_rating']            = 'Рейтинг';
$_['entry_good']              = 'Добре';
$_['entry_bad']               = 'Погано';

// Error
$_['error_name']              = 'Ім\'я автора повинно бути від 3 до 25 символів!';
$_['error_text']              = 'Текст коментаря повинен бути від 3 до 1000 символів!';
$_['error_captcha']           = 'Код перевірки не відповідає картинці!';